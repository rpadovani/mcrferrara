<?php
require_once('php/generatore.php');

$gen = new generatore("Società scientifiche");
$gen->chiudi_head();
$gen->crea_body();
?>
    <div class="container">
        <h2>Nel presente</h2>
        <p>Attualmente Mcr Ferrara è segreteria organizzativa permanente di 3 società: <br/>
            <a href="alleanza_flebolinfologica.php"><img src="img/segreterie/alleanza_flebolinfologica.png"
                                                         alt="Logo Alleanza Flebolinfologica"
                                                         class="rounded mx-auto d-block"/></a><br/>
            <a href="sedi.php"><img src="img/segreterie/sedi.jpg" alt="Logo SEDI"
                                    class="rounded mx-auto d-block"/></a><br/>
            <a href="smc.php"><img src="img/segreterie/smc.jpg" alt="Logo S.M.C" class="rounded mx-auto d-block"/></a>
        </p>
        <h2>Nel passato</h2>
        <p>Mcr Ferrara nel passato è stata segreteria organizzativa anche di: <br/>
            <img src="img/segreterie/aaroi.jpg" alt="Logo arraoi" class="rounded mx-auto d-block"/> <br/>
            <img src="img/segreterie/gastrocare_mini.jpg" alt="Logo gastrocare" class="rounded mx-auto d-block"/><br/>
            <img src="img/segreterie/sifl.jpg" alt="Logo sifl" class="rounded mx-auto d-block"/><br/>
        </p>
    </div>
<?php
$gen->chiudi_pagina();

