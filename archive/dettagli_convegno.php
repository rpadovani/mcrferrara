<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*Mostra i dettagli di un convegno passato per get*/

//Richiamiamo i file necessari
require_once('php/generatore.php');
require_once('php/convegni.php');

//Recuperiamo l'id del convegno
if (isset($_GET['id']))
    $id_convegno = $_GET['id'];

//Inizializziamo la classe che gestice i convegni
$convegno = new gestisci_convegno();

//Se non è stato selezionato nessun convegno scriviamo che non ci sono convegni
if (!isset($id_convegno)) {
    $scritta = "Nessun convegno selezionato";
    $titolo ="Nessun convegno selezionato";
}

//Altrimenti creiamo la query da passare alla funzione che elenca i convegni
else {
    //Creiamo la query di base
    $query = "SELECT * FROM convegni WHERE ";
    
    //Facciamo il controllo sulla validità del parametro passato
    if (is_numeric($id_convegno))
        $query = $query . "id_convegno = $id_convegno";
    //Altrimenti solleviamo un'eccezione
    else
        throw new errore_query();
    
    $titolo = "Mcrferrara - " . $convegno->titolo($query);
}
//Creiamo la pagina
$gen = new generatore($titolo);

//Prima di chiudere la testa ci piazziamo il codice necessario per il pulsante +1 di G+
echo <<<GOOGLEPLUS
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
        {lang: 'it'}
    </script>
GOOGLEPLUS;

$gen->chiudi_head();
$gen->crea_body("convegni");
//Inseriamo il codice per il pulsante raccomanda di Fb
echo <<<FACEBOOK
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
FACEBOOK;

//Inseriamo il sottomenù dei convegni
$gen->crea_convegni();
$gen->inserisci_contenuto();
if (isset($id_convegno))
    $convegno->dettagli_convegno($query);
?>

<?php
$gen->chiudi_pagina();
?>
