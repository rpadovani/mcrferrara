<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011-2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5, HTML 4
Tabulazioni: 4 spazi
Nomi separati da underscore
*/ 
//Richiamiamo il file che genera la pagina
require_once('php/generatore.php');

//Impostiamo il titolo della pagina
$gen = new generatore("Società scientifiche");

//Inseriamo il CSS aggiuntivo per le segreterie
echo '<link rel="stylesheet" type="text/css" href="css/segreterie.css" />';

//Chiudiamo il tag <head>
$gen->chiudi_head();

//Inseriamo il menù superiore selezionato su segreterie scientifiche
$gen->crea_body('società');

//Inseriamo il sottomenù senza nessuna voce selezionata
$gen->crea_societa();

//Apriamo il body
$gen->inserisci_contenuto();
?>
<h2>Nel presente</h2>
<p>Attualmente Mcr Ferrara è segreteria organizzativa permanente di 2 società: <br/>
	<a href="sedi.php"><img src="img/segreterie/sedi.jpg" alt="sedi" class="img_sedi" /></a><br/>
	<a href="smc.php"><img src="img/segreterie/smc.jpg" alt="smc" class="img_smc" /></a>
</p>
<h2>Nel passato</h2>
<p>Mcr Ferrara nel passato è stata segreteria organizzativa anche di: <br/>
	<img src="img/segreterie/aaroi.jpg" alt="aaroi" class="img_aaroi" /> <br/>
	<img src="img/segreterie/gastrocare_mini.jpg" alt="gastrocare" class="img_gastrocare" /><br/>
	<img src="img/segreterie/sifl.jpg" alt="sifl" class="img_sifl" /><br/>
</p>
<?php
$gen->chiudi_pagina();
?>
