<?php 
require_once('php/generatore.php');

$gen = new generatore("Gastrocare");
$gen->chiudi_head();
$gen->crea_body('società');
$gen->crea_societa('gastrocare');
$gen->inserisci_contenuto();
?>
<h2>Informazioni</h2>
<p>
	<img src="img/segreterie/gastrocare.jpg" alt="gastrocare" class="img_gastrocare_pagina" /><br/>
	Dal Novembre 2005, una rappresentanza di professionisti, Infermieri e Medici, che operano in campo Gastroenterologico, ha iniziato a lavorare alla costituzione di un gruppo di lavoro multiprofessionale, aperto a tutti quanti avessero voluto impegnarsi, denominandolo Gastrocare, per sottolineare l'iniziale apparentamento organizzativo con l'Associazione dei Gastroenterologi ed endoscopisti ospedalieri, ma anche per non far nascere una nuova società scientifica.
</p> 
<h2>Contatti</h2>
<table class="tabella_segreterie"> 
	<tr><td class="fisso"><b>Sede: </b></td><td class="descr">Via Cittadella 31, 44121 Ferrara<br/></td></tr> 
	<tr><td class="fisso"><b>Sito internet: </b></td><td class="descr"><a href="http://www.gastrocare.org">www.gastrocare.org</a><br/></td></tr> 
	<tr><td class="fisso"><b>Indirizzo email: </b></td><td class="descr"><a href="mailto:segreteria@gastrocare.org">segreteria@gastrocare.org</a><br/></td></tr> 
</table>
<?php
$gen->chiudi_pagina();
?>