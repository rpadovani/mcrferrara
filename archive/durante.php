<?php
require_once('php/generatore.php');

$gen = new generatore("Servizi congressuali");
$gen->chiudi_head();
$gen->crea_body("servizi");
$gen->crea_servizi("durante");
$gen->inserisci_contenuto();
?>
<h2>Servizi congressuali</h2>
Perché l'evento si svolga al meglio, Mcr Ferrara ha studiato appositi servizi da offrire, fra cui:
<ul class="servizi">
    <li>&rArr; Allestimento sale congressuali presso la sede del convegno;</li>
    <li>&rArr; Coordinamento di tutte le fasi del convegno;</li>
    <li>&rArr; Servizio di segreteria generale presso la sede del convegno;</li>
    <li>&rArr; Distribuzione del materiale congressuale;</li>
    <li>&rArr; Distribuzione materiale illustrativo sulla città di Ferrara;</li>
</ul>
<?php
$gen->chiudi_pagina();
?>
