<?php
require_once('php/generatore.php');

$gen = new generatore("Mcr Ferrara");
?>
<!--Inseriamo i file aggiuntivi per lo slider-->
<link rel="stylesheet" href="slider/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="slider/nivo-slider.css" type="text/css" media="screen" />
<?php
$gen->chiudi_head();
$gen->crea_body("home");
$gen->inserisci_contenuto(); ?>

<div id="wrapper">
    

        <div class="slider-wrapper theme-default">
            <div class="ribbon"></div>
            <div id="slider" class="nivoSlider">
                <img src="img/slider/1.jpg" alt="" />
                <img src="img/slider/2.jpg" alt="" />
                <img src="img/slider/3.jpg" alt="" />
            </div> 
        </div>

    </div>

<div id="isole_home_page">    
<div class="contenitore_piccolo">
    <div class="titolo_contenitore_piccolo">Contatti</div>

    <div class="voce_contenitore_piccolo">
        <div class="titoletto_contenitore_piccolo">Numero di telefono</div>
        <div class="dettagli_contenitore_piccolo">+39 0532 242418</div>
    </div>
    <div class="voce_contenitore_piccolo">
        <div class="titoletto_contenitore_piccolo">Indirizzo email</div>
        <div class="dettagli_contenitore_piccolo">info@mcrferrara.org</div>
    </div>
    <div class="voce_contenitore_piccolo">
        <div class="titoletto_contenitore_piccolo">Numero di fax</div>
        <div class="dettagli_contenitore_piccolo">+39 0532 213560 </div>
    </div>
    <div class="voce_contenitore_piccolo">
        <div class="titoletto_contenitore_piccolo">Sede</div>
        <div class="dettagli_contenitore_piccolo">Viale Corso Isonzo 53, 44121 Ferrara</div>
    </div>

    <div class="vedi_altro_contenitore_piccolo"><a href="contatti.php">Vedi tutto &#8594;</a></div>
</div>

<div class="contenitore_piccolo">
    <div class="titolo_contenitore_piccolo">Prossimi convegni</div>
    
    <?php 
        //Richiamiamo il file che gestisce i convegni
        require_once('php/convegni.php');
        
        $convegni = new gestisci_convegno();
        
        $convegni->prossimi_convegni();
    ?>

    <div class="vedi_altro_contenitore_piccolo"><a href="convegni.php?anno=<?php echo date('Y'); ?>">Vedi tutto &#8594;</a></div>
</div>

<div class="contenitore_piccolo">
    <div class="titolo_contenitore_piccolo">Ultime relazioni disponibili</div>
    <?php 
        //Richiamiamo il file che gestisce le relazioni
        require_once('php/relazioni.php');
        
        $relazioni = new gestisci_relazioni();
        
        $relazioni->ultime_relazioni();
    ?>
    <div class="vedi_altro_contenitore_piccolo"><a href="relazioni.php">Vedi tutto &#8594;</a></div>
</div>

<div class="chiusura"></div>
</div>



    <script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
            directionNav:false, // Next e Prev navigation
            controlNav: false
        });
    });
    </script>
<?php
$gen->chiudi_pagina();
?>
