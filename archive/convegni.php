<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*File che visualizza tutti i convegni, prendendo le opzioni passate per url. Se non ce ne sono, presenta la lista degli anni e il modulo di ricerca*/

//Richiamiamo i file necessari
require_once('php/generatore.php');
require_once('php/convegni.php');

//Recuperiamo eventuali variabili passate per get
if (isset($_GET['anno']))
    $anno_convegni = $_GET['anno'];

//Creiamo la pagina
$gen = new generatore("Convegni");
$gen->chiudi_head();
$gen->crea_body("convegni");
//Se siamo sulla pagina di un anno dei convegni evidenziamo il link corrispondente
if (isset($anno_convegni)){
    if ($anno_convegni == date('Y'))
        $gen->crea_convegni($anno_convegni);
    else
        $gen->crea_convegni();
}
else
    $gen->crea_convegni('convegni');
$gen->inserisci_contenuto();

//Inizializziamo la classe che gestice i convegni
$convegno = new gestisci_convegno();

//Se non sono ci sono variabili creiamo la selezione dei convegni
if (!isset($_GET['anno'])) {
    echo "<h2>Seleziona per anno</h2>";
    
    //Creiamo la lista degli anni
    $convegno->crea_lista_anni();
    
    //Creiamo il form di ricerca
    /*echo "<h2>Fai una ricerca</h2>";
    $convegno->crea_form_ricerca();*/
    echo "<div class='chiusura'></div>";
}
//Se c'è qualche variabile per get componiamo la query
else {
    //Creiamo la query di base
    $query = "SELECT * FROM convegni WHERE ";
    
    //Controlliamo quali variabili esistono ed aggiungiamole alla query
    if (isset($anno_convegni)){
        //Controlliamo che sia una stringa valida per evitare SQL injection: deve essere un numero!
        if (is_numeric($anno_convegni))
            $query = $query . "anno = $anno_convegni ";
        else
            throw new errore_query();
    }
    
    //Aggiungiamo l'ordine con cui mostrare i convegni
    $query = $query . "ORDER BY anno, mese, giorno";
    
    //Elenchiamo i convegni
    $convegno->crea_lista_convegni($query);
}
?>

<?php
$gen->chiudi_pagina();
?>
