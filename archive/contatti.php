<?php
require_once('php/generatore.php');

$gen = new generatore("Contatti");
?>
<!--G+ badge-->
<link href="https://plus.google.com/103965620738224272340" rel="publisher" /><script type="text/javascript">
window.___gcfg = {lang: 'it'};
(function() 
{var po = document.createElement("script");
po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(po, s);
})();</script>
<?php
$gen->chiudi_head();
$gen->crea_body("chi_siamo");
$gen->crea_chi_siamo("contatti");
$gen->inserisci_contenuto();
?>
<!--Javascript SDK per Facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<h2>Contatti</h2>
<table class="tabella_segreterie"> 
	<tr><td class="fisso"><b>Sede: </b></td><td class="descr">Via Cittadella 31, 44121 Ferrara<br/></td></tr> 
	<tr><td class="fisso"><b>Email: </b></td><td class="descr"><a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a><br/></td></tr> 
</table>
<h2>Social network</h2>
Mcr Ferrara è presente anche su <a href="https://plus.google.com/">Google+</a> e <a href="http://www.facebook.com/mcrferrara">Facebook</a>. Seguici per essere sempre informato sulle ultime attività!<br/><br/>
<div class="badge_fb">
    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Mcrferrara/158830014158119" data-width="400" data-show-faces="true" data-stream="false" data-header="false"></div>
</div>
<div class="badge_google">
    <g:plus href="https://plus.google.com/103965620738224272340" size="badge"></g:plus>
</div>
<div class="chiusura"></div>
<?php
$gen->chiudi_pagina();
?>
