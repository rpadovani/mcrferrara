<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*File che crea le schede di iscrizioni*/
//Richiamiamo i file necessari
require_once('php/generatore.php');

//Creiamo la pagina
$gen = new generatore("Conferma iscrizione online");
$gen->chiudi_head();
$gen->crea_body("convegni");
$gen->crea_convegni("iscrizioni");  

$gen->inserisci_contenuto();

?>
<h2>L'iscrizione è avvenuta con successo</h2>
I suoi dati sono stati inviati a M.C.R. Ferrara. <br/>
Per qualsiasi dubbio non esiti a contattarci all'indirizzo email <a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a><br/>

<h2>Social network</h2>
Mcr Ferrara è presente anche su <a href="https://plus.google.com/">Google+</a> e <a href="http://www.facebook.com/mcrferrara">Facebook</a>. Seguici per essere sempre informato sulle ultime attività!


<?php
$gen->chiudi_pagina();
?>
