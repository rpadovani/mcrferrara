<?php
require_once('php/generatore.php');
require_once('php/relazioni.php');

$gen = new generatore("Relazioni dei convegni");
$gen->chiudi_head();
$gen->crea_body("convegni");
$gen->crea_convegni('relazioni');
$gen->inserisci_contenuto();

$relazioni = new gestisci_relazioni();
$relazioni->elenca_relazioni();
?>

<?php
$gen->chiudi_pagina();
?>
