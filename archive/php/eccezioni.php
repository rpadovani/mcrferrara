<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

//Richiamiamo il file di configurazione che ha al suo interno l'indirizzo email dell'amministratore
require_once("configurazione.php");

/*Il file crea la classe Exception predefinita e la estende la classe Exception per controllare i possibili errori generati.
Funzioni presenti:
- eccezione_generale($eccezione): funzione predefinita per le eccezioni, che viene usata per tutte le eccezioni non gestite, reindirizza l'utente ad una pagina predisposta e invia una mail all'amministratore di sistema
Classi presenti:
- errore_database: estende Exception per gestire eventuali connessioni non riuscite con il database
*/
function eccezione_generale($eccezione) {
    //Registriamo l'errore ed inviamo una mail all'amministratore di sistema
    $classe_eccezione = get_class($eccezione);
    $file_eccezione = $eccezione->getFile();
    $linea_eccezione = $eccezione->getLine();
    $messaggio_eccezione = $eccezione->getMessage();
    
    $headers_email = "From: ". EMAIL_NO_REPLY . "\r\n" . "Reply-To: ". EMAIL_NO_REPLY . "\r\n" . 'X-Mailer: PHP/' . phpversion(); 
    $destinatario_email = EMAIL_AMMINISTRATORE;
    $oggetto_email = "Eccezione generale occorsa sul sito www.mcrferrara.org";
    $testo_email = "Questo è un messaggio generato automaticamente. È stata sollevata un'eccezione non gestita sul sito www.mcrferrara.org.
    Eccezione: $classe_eccezione (sul $file_eccezione alla linea $linea_eccezione):
    $messaggio_eccezione
    1001 => Connessione fallita al database
    1002 => Query non valida, tentativo di sql injection";
    
    mail($destinatario_email, $oggetto_email, $testo_email, $headers_email);
       
    
    //L'utente viene reindirizzato alla pagina di errore generica
    header("Location: eccezione_generale.php?errore=$messaggio_eccezione");
}

//Impostiamo la funzione eccezione_generale come gestore predefinita delle eccezioni
set_exception_handler("eccezione_generale");

//Classe per un eventuale errore nella connessione con il database. 
class errore_database extends Exception {
  public function __construct($numero_errore) {
    parent::__construct("1001");
  }
}

//Classe per un eventuale errore nella creazione di una query di interrogazione al database. Necessaria per evitare sql_injection 
class errore_query extends Exception {
  public function __construct() {
    parent::__construct("1002");
  }
}

?>
