<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*File che si occupa di gestire i convegni, le liste e la connessione con il database
Contiene la classe gestisci_convegno, che ha al suo interno le funzioni
- crea_lista_anni(): crea nella pagina "convegni.php" la lista degli anni in cui sono presenti convegni 
- crea_form_ricerca(): crea il form di ricerca per i convegni
- crea_connessione(): è la funzione che crea una connessione al database e nel caso si connetta senza errori restituisce come oggetto la connessione
- crea_lista_convegni($query): in base alla query ricevuta elenca i convegni selezionati; le query sono già state filtrate. Se non ci sono risultati scrive che non sono stati trovati risultati (appunto)
- dettagli_convegno($query): seleziona un singolo convegno in base all'id passato per argomento
- prossimi_convegni(): stampa i prossimi 4 convegni in home page
*/

//Richiamiamo il file di configurazione che contiene le password di connessione al database, e il file di gestione delle eccezioni
require_once('configurazione.php');
require_once('eccezioni.php');

class gestisci_convegno {
    function crea_lista_anni() {
        //I convegni iniziano nel 2003 e finiscono nell'anno attuale se non sono presenti quelli dell'anno successivo
        $anno_inizio = 2003;
        $anno_fine = date('Y'); //L'anno di fine è quello attuale
        
        //Controlliamo se ci sono convegni di anni successivi
        $connessione = $this->crea_connessione();
        $query = "SELECT * FROM convegni WHERE anno > $anno_fine";
        $esiste_anno = @$connessione->query($query);
        
        //Se la query non è vuota aumenta di uno l'ultimo anno (è impossibile che ci siano convegni di 2 anni dopo)
        if (($esiste_anno->num_rows) !== 0)
            $anno_fine++ ;
        
        //Creaiamo un while che scriva tutti gli anni presenti, dall'ultimo al primo
        $anno_attuale = $anno_fine;
        while ($anno_attuale >= $anno_inizio){
            echo "<div class='anno_convegno'><a href='convegni.php?anno=$anno_attuale'>$anno_attuale &#8594</a></div>";
            $anno_attuale--;
        }
        
        //Chiudiamo la connessione visto che non è più utilizzata
        $connessione->close();
    }

    function crea_form_ricerca() {
        //Iniziamo creando il form
        echo "<fieldset>";
        echo "<form name='ricerca_convegni' action='ricerca.php'>";
        
            //Inseriamo l'anno di inizio
            echo "<label for='anno_inizio'>Anno di inizio: </label>";
            echo "<input id='anno_inizio' class='anno' type='text'/><br/>";
            
            //Inseriamo l'anno di fine
            echo "<label for='anno_fine'>Anno di fine: </label>";
            echo "<input id='anno_fine' class='anno' type='text'/><br/>";
            
            //Inseriamo la selezione del mese
            echo "<label for='mese_ricerca'>Mese: </label>";
            echo "<select id='mese_ricerca' class='generico' type='text'/><br/>";
                //Inseriamo tutti i mesi
                echo "<option value='0'>[Seleziona]</option>";
                echo "<option value='1'>Gennaio</option>";
                echo "<option value='2'>Febbraio</option>";
                echo "<option value='3'>Marzo</option>";
                echo "<option value='4'>Aprile</option>";
                echo "<option value='5'>Maggio</option>";
                echo "<option value='6'>Giugno</option>";
                echo "<option value='7'>Luglio</option>";
                echo "<option value='8'>Agosto</option>";
                echo "<option value='9'>Settembre</option>";
                echo "<option value='10'>Ottobre</option>";
                echo "<option value='11'>Novembre</option>";
                echo "<option value='12'>Dicembre</option>";
            echo "</select><br/>";
            
            //Inseriamo la selezione della società
            echo "<label for='società_ricerca'>Società: </label>";
            echo "<select id='società_ricerca' class='generico' type='text'/><br/>";
                //Inseriamo tutti i mesi
                echo "<option value='0'>[Seleziona]</option>";
                echo "<option value='1'>Smc</option>";
            echo "</select><br/>";
            
            //Inseriamo il bottone della ricerca
            echo "<input type='submit' value='Inizia ricerca' class='bottone_ricerca' />";
        
        //Chiudiamo il form
        echo "</form>";
        echo "</fieldset>";
    }

    private function crea_connessione() {
        //Connettiamoci al server
        $conn = new mysqli(SERVER, NOME, PASSWORD, DATABASE);
            
        //Se c'è un errore solleva un'eccezione
        if (mysqli_connect_errno() !== 0)
            throw new errore_database(mysqli_connect_error());
        
        //Se non sono stati sollevati errori ritorna la connessione al database
        return $conn;  
    }
    
    function crea_lista_convegni($query) {
        //Connettiamoci al server attraverso la funzione crea_connessione()
        $connessione = $this->crea_connessione();
        
        //Impostiamo l'utf-8 come set di connessione
        $connessione->query("SET NAMES 'utf8'");
        
        //Eseguiamo la query, su cui sono già stati fatti i controlli
        $convegni = @$connessione->query($query);
        
        //Se la query ritorna vuota scriviamo che non ci sono convegni da visualizzare
        if (($convegni->num_rows) === 0) {
            echo "<h2>Nessun risultato</h2>";
            echo "Non sono disponibili convegni che corrispondano ai parametri indicati";
        }
        else {
            //Variabile che usiamo per scrivere l'anno nei titoletti
            $anno_attuale = 0;
            
            //Scriviamo i convegni uno alla volta
            while (($convegno = @$convegni->fetch_assoc()) !== NULL) {

                
                //Se è la prima volta che viene selezionato quell'anno viene scritto il titoletto con l'anno
                if ($anno_attuale !== $convegno['anno']) {
                    $anno_attuale = $convegno['anno'];
                    echo "<h2>$anno_attuale</h2>";
                }
                
                //Inseriamo la data
                echo "<div class='singolo_convegno'>";
                    echo "<div class='data_singolo_convegno'>";
                        echo "<div class='giorno_singolo_convegno'>";
                            if ($convegno['giorno_fine'] === NULL)    
                                echo $convegno['giorno'];
                            else
                                echo $convegno['giorno'].'-'.$convegno['giorno_fine'];
                        echo "</div>";    
                        echo "<div class='mese_singolo_convegno'>";    
                            $this->converti_mese($convegno['mese']);
                        echo "</div>";
                        echo "<div class='anno_singolo_convegno'>";
                            echo $convegno['anno'];
                        echo "</div>";
                    echo "</div>";
                    
                    //Controlliamo che il titolo non sia troppo lungo; se lo è lo tagliamo
                    if ((strlen($convegno['titolo'])) > 79) 
                        $convegno['titolo'] = substr($convegno['titolo'], 0, 80) . '...';
                        
                    //Inseriamo il titolo
                    echo "<div class='titolo_singolo_convegno'>";
                        echo $convegno['titolo'];
                        //Inseriamo il luogo
                        echo "<div class='luogo_singolo_convegno'>";
                            echo $convegno['luogo'];
                        echo "</div>";
                    echo "</div>";
                    
                    //Eventualmente inseriamo le icone sotto
                    if ((($convegno['società'])!==NULL) or (($convegno['download'])!==NULL) or (($convegno['relazioni'])!==NULL) or (($convegno['iscrizioni'])!==NULL)) {
                        echo "<div class='icone_singolo_convegno'>";
                        
                        //Inseriamo l'icona della società
                        if (($convegno['società'])!=='') {
                            switch ($convegno['società']){
                                case "smc":
                                    echo "<a href='smc.php' title='Organizzato dalla Smc'><img src='img/segreterie/smc.jpg' alt='Convegno Smc' class='immagine_icona_singolo_convegno' /></a>";
                                    break;
                            }         
                        }
                        
                        //Inseriamo l'icona del progamma in pdf
                        if (($convegno['download'])!=='' and ($convegno['download']!== NULL)) 
                            echo "<a href=".$convegno['download']." title='Scarica il programma del convegno'><img src='img/pdf.jpg' alt='Programma' class='immagine_icona_singolo_convegno' /></a>";
                            
                        //Inseriamo l'icona delle relazioni 
                     if (($convegno['relazioni'])!=='' and ($convegno['relazioni']!== NULL)) 
                         echo "<a href='dettagli_relazione.php?id=".$convegno['id_convegno']."' title='Scarica le relazioni del convegno'><img src='img/relazioni.jpg' alt='Relazioni' class='immagine_icona_singolo_convegno' /></a>";
                         
                    //Inseriamo l'icona delle iscrizioni
                    if ((isset($convegno['iscrizioni'])) and ($convegno['iscrizioni'])!=='' and ($convegno['iscrizioni']!== NULL)) {
                        //Controlliamo che il convegno non si sia già tenuto
                        if (($convegno['anno'] > date('Y')) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] > date('n'))) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] == date('n')) and ($convegno['giorno'] >= date('j'))) )
                            echo "<a href='iscrizioni.php?convegno=".$convegno['id_convegno']."' title='Iscriviti online al convegno'><img src='img/iscrizioni.gif' alt='Iscrizioni online' class='immagine_icona_singolo_convegno' /></a>";
                        }
                    
                    //Inseriamo l'icona per scaricare la scheda di iscrizione
                    if ((isset($convegno['scheda_iscrizione'])) and ($convegno['scheda_iscrizione'])!=='' and ($convegno['scheda_iscrizione']!== NULL)) {
                        //Controlliamo che il convegno non si sia già tenuto
                        if (($convegno['anno'] > date('Y')) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] > date('n'))) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] == date('n')) and ($convegno['giorno'] >= date('j'))) )
                            echo "<a href='download/iscrizioni/".$convegno['scheda_iscrizione']."' title='Scarica la scheda di iscrizione'><img src='img/download_iscrizioni.jpg' alt='Scheda di iscrizione' class='immagine_icona_singolo_convegno' /></a>";
                        }    
                        //Chiusura icone del singolo convegno
                        echo "</div>";
                    }
                    
                //Inseriamo il link "Leggi tutto" che porta alla pagina dettagli_convegno
                echo "<div class='leggi_tutto_singolo_convegno'><a href='dettagli_convegno.php?id=".$convegno['id_convegno']."'>Leggi tutto&#8594</a></div>";
                
                //Chiusura singolo convegno
                echo "</div>";  
            }  
        }
        //Chiudiamo la connessione
        $connessione->close();
    }
    
    function dettagli_convegno($query){
        //Connettiamoci al server attraverso la funzione crea_connessione()
        $connessione = $this->crea_connessione();
        
        //Impostiamo l'utf-8 come set di connessione
        $connessione->query("SET NAMES 'utf8'");
        
        //Eseguiamo la query, su cui sono già stati fatti i controlli
        $risultato = @$connessione->query($query);
        
        //Se la query ritorna vuota scriviamo che non ci sono convegni da visualizzare
        if (($risultato->num_rows) === 0) {
            echo "<h2>Nessun risultato</h2>";
            echo "Non sono disponibili convegni che corrispondano ai parametri indicati";
        }
        else {
            //Estraiamo i dati dalla query
            $convegno = $risultato->fetch_assoc(); 
            
            //Scriviamo il titoletto
            echo "<h2>Presentazione del convegno </h2>";
            
            //Scriviamo il titolo del convegno
            echo "<div class='titolo_dettaglio_convegno'>";
                echo "<b>".$convegno['titolo']."</b>";
            echo "</div>";
            
            //Se è disponibile inseriamo la descrizione del convegno
            if (isset($convegno['descrizione'])) {
                echo "<div class='descrizione_dettaglio_convegno'>";
                    echo $convegno['descrizione'];
                echo "</div>";
            }
            
            echo "<h2>Luogo e data</h2>";
            //Scriviamo il luogo del convegno
            echo "<div class='luogo_dettaglio_convegno'>";
                echo $convegno['luogo']; 
            echo "<br/></div><br/>";
            
            //Guardiamo se il convegno è già avvenuto o deve ancora avvenire
            if ($convegno['anno'] < date ('Y'))
                echo "<div class='data_dettaglio_convegno'>Il convegno si è tenuto ";
            elseif ($convegno['anno'] > date('Y'))
                echo "<div class='data_dettaglio_convegno'>Il convegno si terrà ";
            //Se l'anno è uguale controlliamo il mese
            elseif ($convegno['mese'] < date('n'))
                echo "<div class='data_dettaglio_convegno'>Il convegno si è tenuto ";
            elseif ($convegno['mese'] > date('n'))
                echo "<div class='data_dettaglio_convegno'>Il convegno si terrà ";
            //Se il mese è uguale controlliamo il giorno
            elseif ($convegno['giorno'] <= date('j'))
                echo "<div class='data_dettaglio_convegno'>Il convegno si è tenuto ";
            elseif ($convegno['giorno'] >= date('j'))
                echo "<div class='data_dettaglio_convegno'>Il convegno si terrà ";
            
            //A questo punto scriviamo la data. Se è il primo del mese scriviamo primo
            if ($convegno['giorno_fine'] === NULL) {
                if ($convegno['giorno'] == '1')
                    echo "il primo ";
                else
                    echo "il ". $convegno['giorno'] . ' ';
            }
            else
                echo "dal ". $convegno['giorno']. ' al ' . $convegno['giorno_fine'] . ' ';
            
            //Sostituiamo il numero del mese con il mese in lettere
            $this->converti_mese($convegno['mese']);
            
            //Scriviamo l'anno del convegno
            echo ' '. $convegno['anno'] . '</div>';
            
            //Eventuali servizi aggiuntivi prima dei pulsanti per la condivisione
            echo "<h2>Ulteriori informazioni</h2>";
            echo "<table class='tabella_segreterie'>";
            
                //Società organizzatrice
                echo "<tr><td class='fisso'><b>Società:</b> </td><td>";
                if ((isset($convegno['società'])) and ($convegno['società'] != '')) {
                    switch ($convegno['società']) {
                        case "smc":
                            echo "<a href='smc.php' title='Organizzato dalla Smc'><img src='img/segreterie/smc.jpg' alt='Convegno Smc' /></a>";
                            break;
                        
                        default:
                            echo "Nessuna";
                            break;
                    }
                }
                else
                    echo "Nessuna";
                echo "</td></tr>";
                
                //Programma da scaricare
                echo "<tr><td class='fisso'><b>Programma:</b> </td><td>";
                if ((isset($convegno['download'])) and ($convegno['download'])!=='' and ($convegno['download']!== NULL)) 
                    echo "<a href=".$convegno['download']." title='Scarica il programma del convegno'><img src='img/pdf.jpg' alt='Programma' /></a>";
                else
                    echo "Non è disponibile un programma da scaricare. Per ulteriori informazioni sul convegno ci <a href='mailto:info@mcrferrara.org'>contatti</a>";
                echo "</td></tr>";
                
                //Relazioni
                echo "<tr><td class='fisso'><b>Relazioni:</b> </td><td>";
                if ((isset($convegno['relazioni'])) and ($convegno['relazioni'])!=='' and ($convegno['relazioni']!== NULL)) 
                    echo "<a href=dettagli_relazione.php?id=".$convegno['id_convegno']." title='Guarda le relazioni pubblicate'><img src='img/relazioni.jpg' alt='Relazioni' class='img_relazioni' /></a>";
                else
                    echo "Non sono state pubblicate relazioni dell'evento.";
                echo "</td></tr>";
                
                //Accreditamento ECM
                if ((isset($convegno['accreditamento'])) and ($convegno['accreditamento'])!=='' and ($convegno['accreditamento']!== NULL))
                    echo "<tr><td class='fisso'><b>Accreditato per:</b> </td><td>".$convegno['accreditamento']."</td></tr>";
                
                //Iscrizioni online
                echo "<tr><td class='fisso'><b>Iscrizioni online:</b> </td><td>";
                if ((isset($convegno['iscrizioni'])) and ($convegno['iscrizioni'])!=='' and ($convegno['iscrizioni']!== NULL)) {
                    //Controlliamo che il convegno non si sia già tenuto
                    if (($convegno['anno'] > date('Y')) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] > date('n'))) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] == date('n')) and ($convegno['giorno'] >= date('j'))) )
                        echo "<a href='iscrizioni.php?convegno=".$convegno['id_convegno']."'>Disponibili</td></tr>";
                    else
                        echo "Convegno già avvenuto";
                }
                else {
                    if ($convegno['società'] == "smc" && (($convegno['anno'] > date('Y')) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] > date('n'))) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] == date('n')) and ($convegno['giorno'] >= date('j'))))) {
                        echo "Le iscrizioni agli incontri della SMC sono diverse a seconda della professione:<br/>
<u>Infermieri e tecnici</u> dipendenti dell'AOU FE devono iscriversi tramite il proprio coordinatore, per quelli non dipendenti si sa solo la mattina del convegno se ci sono posti disponibili per i crediti<br/>
<u>Medici:</u>  i crediti sono riservati agli iscritti alla SMC (€36 annui) o a chi si iscrive alla giornata (€25,00)<br/>
In ogni caso <b>le iscrizioni avvengono la mattina stessa</b> e la partecipazione, con attestato, è libera";
                    }
                    else if (($convegno['anno'] > date('Y')) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] > date('n'))) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] == date('n')) and ($convegno['giorno'] >= date('j'))))
                        echo "Non disponibili";
                    else
                        echo "Convegno già avvenuto";
                }
                    
                    
                //Inseriamo l'icona per scaricare la scheda di iscrizione
                if ((isset($convegno['scheda_iscrizione'])) and ($convegno['scheda_iscrizione'])!=='' and ($convegno['scheda_iscrizione']!== NULL)) {
                        //Controlliamo che il convegno non si sia già tenuto
                        if (($convegno['anno'] > date('Y')) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] > date('n'))) or (($convegno['anno'] == date('Y')) and ($convegno['mese'] == date('n')) and ($convegno['giorno'] >= date('j'))) )
                            echo "<tr><td class='fisso'><b>Scheda di iscrizione:</b> </td><td><a href='download/iscrizioni/".$convegno['scheda_iscrizione']."' title='Scarica la scheda di iscrizione'><img src='img/download_iscrizioni.jpg' alt='Scheda di iscrizione' class='img_relazioni' /></a></td></tr>";
                        } 
                    
            echo "</table>";
            
            //Condivisione social
            echo "<h2>Social buttons</h2>";
                 echo "<div class='bottoni_sociali'>";
                
                //Pulsante +1
                echo "<g:plusone></g:plusone>";
            echo "</div>";
            echo "<div class='bottoni_sociali'>";    
                //Pulsante twitter
                echo <<<TWITTER
                <a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-lang="it">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
TWITTER;
            echo "</div>";
            echo "<div class='bottoni_sociali'>";    
                //Pulsante Fb
                echo <<<FACEBOOK
                <fb:like send="false" layout="button_count" width="86" show_faces="true" action="recommend"></fb:like>
FACEBOOK;
            echo "</div>";
            
            
            //Div per impedire che i bottoni strasbordino
            echo "<div class='chiusura'></div>";
            //Chiudiamo la connessione
        $connessione->close();
        } 
    }
    
    function prossimi_convegni() {
        //Connettiamoci al server attraverso la funzione crea_connessione()
        $connessione = $this->crea_connessione();
        
        //Impostiamo l'utf-8 come set di connessione
        $connessione->query("SET NAMES 'utf8'");
        
        //La query deve identificare i prossimi 4 convegni
        $query = "SELECT id_convegno, titolo, giorno, giorno_fine, mese, anno, luogo FROM convegni WHERE (anno = ".date('Y')." AND mese = ".date('n'). " AND giorno >= ".date('j').") OR (anno = ".date('Y')." AND mese > ".date('n'). ") OR (anno > ".date('Y').") ORDER BY anno ASC, mese ASC, giorno ASC LIMIT 0, 4";
        
        //Eseguiamo la query
        $risultato = $connessione->query($query); 
        
        //Scriviamo i convegni uno alla volta
        while (($convegno = $risultato->fetch_assoc()) !== NULL) {
            echo '<div class="voce_contenitore_piccolo">
        <div class="titoletto_contenitore_piccolo">';
        //Controlliamo che il titolo non sia troppo lungo; se lo è lo tagliamo
        if ((strlen($convegno['titolo'])) > 35) 
            $convegno['titolo'] = substr($convegno['titolo'], 0, 33) . '...';
         echo '<a href="dettagli_convegno.php?id='.$convegno['id_convegno'].'">'.$convegno['titolo'].'</a></div>
        <div class="dettagli_contenitore_piccolo">';
        if ($convegno['giorno_fine']=== NULL)
            echo $convegno['giorno'] . ' ';
        else
            echo $convegno['giorno'].'-'.$convegno['giorno_fine']. ' ';
        $this->converti_mese($convegno['mese']);
        echo ' '. $convegno['anno'].'</div>
    </div>';
    
        }
        //Chiudiamo la connessione
        $connessione->close();
    }
    
    //Funzione che converte il mese da numero a lettere
    function converti_mese($mese) {
        switch($mese){
                case 1:
                    echo "gennaio";
                    break;
                    
                case 2:
                    echo "febbraio";
                    break;
                    
                case 3:
                        echo "marzo";
                    break;
                    
                case 4:
                    echo "aprile";
                    break;
                    
                case 5:
                    echo "maggio";
                    break;
                
                case 6:
                    echo "giugno";
                    break;
                
                case 7:
                    echo "luglio";
                    break;
                
                case 8:
                    echo "agosto";
                    break;
                
                case 9:
                    echo "settembre";
                    break;
                
                case 10:
                    echo "ottobre";
                    break;
                
                case 11:
                    echo "novembre";
                    break;
                
                case 12:
                    echo "dicembre";
                    break;
            }
    }
    
     function titolo($query){
        //Connettiamoci al server attraverso la funzione crea_connessione()
        $connessione = $this->crea_connessione();
        
        //Impostiamo l'utf-8 come set di connessione
        $connessione->query("SET NAMES 'utf8'");
        
        //Eseguiamo la query, su cui sono già stati fatti i controlli
        $risultato = @$connessione->query($query);
        
        //Se la query ritorna vuota scriviamo che non ci sono convegni da visualizzare
        if (($risultato->num_rows) === 0) {
            echo "Nessun convegno selezionato";
        }
        else {
            //Estraiamo i dati dalla query
            $convegno = $risultato->fetch_assoc(); 
            return $convegno['titolo'];
        }
    }
}
?>
