<?php
/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/
/*
$elenco_discipline = array(
    "Allergologia ed immunologia clinica",
    "Anatomia patologica",
    "Anestesia e rianimazione",
    "Angiologia",
    "Biochimica clinica",
    "Cardiochirurgia",
    "Cardiologia",
    "Chirurgia generale",
    "Chirurgia maxillo-facciale",
    "Chirurgia pediatrica",
    "Chirurgia plastica e ricostruttiva",
    "Chirurgia toracica",
    "Chirurgia vascolare",
    "Continuità assistenziale",
    "Dermatologia e venereologia",
    "Ematologia",
    "Endocrinologia",
    "Farmacologia e tossicologia clinica",
    "Gastroenterologia",
    "Genetica medica",
    "Geriatria",
    "Ginecologia e ostetricia",
    "Igiene degli alimenti e della nutrizione ",
    "Igiene, epidemiologia e sanità pubblica ",
    "Laboratorio di genetica medica",
    "Malattie dell'apparato respiratorio",
    "Malattie infettive",
    "Malattie metaboliche e diabetologia",
    "Medicina aeronautica e spaziale",
    "Medicina del lavoro",
    "Medicina dello sport",
    "Medicina e chirurgia di accettazione e di urgenza",
    "Medicina fisica e riabilitazione",
    "Medicina generale (Medici di famiglia)",
    "Medicina interna",
    "Medicina legale",
    "Medicina nucleare",
    "Medicina termale",
    "Medicina trasfusionale",
    "Medicine alternative",
    "Microbiologia e virologia",
    "Nefrologia",
    "Neonatologia",
    "Neurochirurgia",
    "Neurofisioèpatologia",
    "Neurologia",
    "Neuropsichiatria infantile",
    "Neuroradiologia",
    "Oftalmologia",
    "Oncologia",
    "Ortopedia e traumatologia",
    "Otorinolaringoiatria",
    "Patologia clinica (laboratorio di analisi chimico-cliniche e microbiologia)",
    "Pediatria (Pediatri di libera scelta)",
    "Pediatria" ,
    "Psichiatria",
    "Radiodiagnostica",
    "Radioterapia",
    "Reumatologia",
    "Scienza dell'alimentazione e dietetica",
    "Urologia"
    );
function controllo_testo($testo) {

}*/

require_once('convegni.php');

//Controlliamo che tutti i campi obbligatori siano stati compilati, e controlliamo che non contengano codice maligno (in realtà essendo una mail non c'è bisogno di controlli particolari...')! Se ne contengono vengono reindirizzati alla pagina di iscrizione. Poichè c'è già il controllo javascript non è necessario salvare i dati, perché chi lo tiene disattivato non si merita di tenere i dati :P
if (!isset($_POST['convegno']) || !is_numeric($_POST['convegno']) || $_POST['convegno'] <= 0) {
    header("Location:../iscrizioni.php");
}

$event_manager = new gestisci_convegno();
$event_id = $_POST['convegno'];
$query = "SELECT * FROM convegni WHERE id_convegno =  " . $event_id;
$title = $event_manager->titolo($query);

if (isset($_POST['nome']))
    $nome = $_POST['nome'];
else
    header("Location:../iscrizioni.php");

if (isset($_POST['cognome']))
    $cognome = $_POST['cognome'];
else
    header("Location:../iscrizioni.php");

if (isset($_POST['luogo_nascita']))
    $luogo_nascita = $_POST['luogo_nascita'];

if (isset($_POST['data_nascita']))
    $data_nascita = $_POST['data_nascita'];

if (isset($_POST['indirizzo']))
    $indirizzo = $_POST['indirizzo'];
else
    header("Location:../iscrizioni.php");

if (isset($_POST['città']))
    $citta = $_POST['città'];
else
    header("Location:../iscrizioni.php");

if (isset($_POST['provincia']))
    $provincia = $_POST['provincia'];

if (isset($_POST['cap']))
    $cap = $_POST['cap'];

if (isset($_POST['email']))
    $email = $_POST['email'];
else
    header("Location:../iscrizioni.php");

if (isset($_POST['telefono']))
    $telefono = $_POST['telefono'];

if (isset($_POST['cellulare']))
    $cellulare = $_POST['cellulare'];

if (isset($_POST['ente']))
    $ente = $_POST['ente'];
else
    header("Location:../iscrizioni.php");

if (isset($_POST['azienda'])) {$azienda = $_POST['azienda'];}
if (isset($_POST['ruolo'])) {$ruolo = $_POST['ruolo'];}
if (isset($_POST['ruolo3'])) {$ruolo = $_POST['ruolo3'];}
if (isset($_POST['qualifica'])) {$qualifica = $_POST['qualifica'];}
if (isset($_POST['ivanome'])) {$ivanome = $_POST['ivanome'];}
if (isset($_POST['ivaindirizzo'])) {$ivaindirizzo = $_POST['ivaindirizzo'];}
if (isset($_POST['ivacitta'])) {$ivacitta = $_POST['ivacitta'];}
if (isset($_POST['piva'])) {$piva = $_POST['piva'];}
if (!isset($_POST['termini_2'])) {header("Location:../iscrizioni.php");}

//Email
$messaggio = "
Convegno: ". $title ."
Cognome: " . $cognome ."
Nome: ". $nome ."
Luogo di nascita: ". $luogo_nascita ."
Data di nascita: ". $data_nascita ."

Indirizzo: ". $indirizzo."
Città: ". $citta."
Provincia: ". $provincia."
CAP: ". $cap."

E-mail: ". $email ."
Telefono: ". $telefono ."
Cellulare: ". $cellulare ."

Ente: ".$ente. "
Ruolo: " . $ruolo . "
Azienda: " . $azienda;

$messaggio = $messaggio . "

Con l'invio della presente autorizzo MCR a inserire i miei dati nell'elenco dei suoi clienti per l'invio di materiale informativo congressuale.
In ogni momento a norma dell'art. 13 L.675/96 potrò avere accesso ai miei dati, chiederne la modifica o la cancellazione oppure oppormi al loro utilizzo, scrivendo a MCR, Via Cittadella 31, 44121 Ferrara. ";

$headers = "From: ".$email . "\r\n" .
    "Reply-To: ". $email . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$destinatario="info@mcrferrara.org";
$oggetto = "Iscrizione online al convegno " . $title;
mail($destinatario, $oggetto, $messaggio, $headers);

header("Location:../conferma_iscrizione.php");
