<?php
/*
Project: Mcr Ferrara - Sito ufficiale
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*
File che genera la pagina principale
Funzioni facenti parte della classe generatore:
- __construct($titolo): crea l'head della pagina, assegnandoli il titolo passato per argomento quando si crea un'istanza della classe
- chiudi_head(): chiude l'head della pagina, inserendo lo script di GAnalytics
- crea_body(): la funzione crea il corpo della pagina e inserisce l'header
- crea_società(): la funzione crea il sottomenù riferito alle società
- chiudi_pagina(): la funzione inserisce il footer e chiude la pagina
- crea_convegni(): la funzione crea il sottomenù dei convegni

*/

class generatore {
    function __construct($titolo) {
        echo <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="it-IT" xml:lang="it-IT" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>$titolo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/gif" href="img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/principale.css" />
HTML;
    }

    function chiudi_head(){
        echo <<<HTML
    <!--GAnalytics-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-10243514-1']);
        _gaq.push(['_gat._anonymizeIp']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
HTML;
    }

    function crea_body($menù){
        echo <<<HTML
<body>
    <div id="contenitore_generale">
        <div id="testata">
HTML;

            //Link che porta all'home
            if ($menù == "home")
                echo '<a href="index.php" class="link_sup_sel_first">Home</a>';
            else
                echo '<a href="index.php" class="link_sup_first">Home</a>';

            //Link del chi siamo
            if ($menù == "chi_siamo")
                echo '<a href="chi_siamo.php" class="link_sup_sel">Chi siamo</a>';
            else
                echo '<a href="chi_siamo.php" class="link_sup">Chi siamo</a>';

            //Link dei servizi
            if ($menù == "servizi")
                echo '<a href="servizi.php" class="link_sup_sel">Servizi</a>';
            else
                echo '<a href="servizi.php" class="link_sup">Servizi</a>';

            //Link dei convegni
            if ($menù == "convegni")
                echo '<a href="convegni.php" class="link_sup_sel">Convegni</a>';
            else
                echo '<a href="convegni.php" class="link_sup">Convegni</a>';

            //Link delle società scientifiche
            if ($menù == "società")
                echo '<a href="societa.php" class="link_sup_sel" >Segreterie scientifiche</a>';
            else
                echo '<a href="societa.php" class="link_sup">Segreterie scientifiche</a>';

            //Inseriamo il logo della società
            echo <<<HTML
                <div class="logo_testata">
                   Mcr Ferrara <img src="img/logo_menu.jpg" height="33px" width="54px" alt="Logo Mcr Ferrara" />
                </div>
        </div>
HTML;
    }

    function crea_chi_siamo($pagina=NULL){
        echo '<div id="sottomenù">';
        if ($pagina == "chi_siamo") {
        	echo '
            <div class="link_sub_active">
                <a href="chi_siamo.php">Chi siamo</a>
            </div>
            <div class="link_sub">
                <a href="contatti.php">Contatti</a>
            </div>';
        }
        elseif ($pagina == "contatti") {
        	echo '
            <div class="link_sub">
                <a href="chi_siamo.php">Chi siamo</a>
            </div>
            <div class="link_sub_active">
                <a href="contatti.php">Contatti</a>
            </div>';
        }
    	else {
        	echo '
            <div class="link_sub">
                <a href="chi_siamo.php">Chi siamo</a>
            </div>
            <div class="link_sub">
                <a href="contatti.php">Contatti</a>
            </div>';
        }
        echo '</div>';
    }

    function crea_societa($pagina=NULL){
        echo '<div id="sottomenù">';
        if ($pagina == "sedi") {
        	echo '
            <div class="link_sub_active">
                <a href="sedi.php">Sedi</a>
            </div>
            <div class="link_sub">
                <a href="smc.php">Smc</a>
            </div>';
        }
    	elseif ($pagina == "sifl") {
        	echo '
            <div class="link_sub">
                <a href="sedi.php">Sedi</a>
            </div>
            <div class="link_sub_active">
                <a href="sifl.php">Sifl</a>
            </div>
            <div class="link_sub">
                <a href="smc.php">Smc</a>
            </div>';
        }
    	elseif ($pagina == "smc") {
        	echo '
            <div class="link_sub">
                <a href="sedi.php">Sedi</a>
            </div>
            <div class="link_sub_active">
                <a href="smc.php">Smc</a>
            </div>';
        }
    	else {
        	echo '
            <div class="link_sub">
                <a href="sedi.php">Sedi</a>
            </div>
            <div class="link_sub">
                <a href="smc.php">Smc</a>
            </div>';
        }
        echo '</div>';
    }

    function inserisci_contenuto(){
        echo <<<HTML
        <div id="contenuto">
HTML;
    }

    function crea_convegni($pagina=NULL){
        echo '<div id="sottomenù">';
        if ($pagina == date('Y')) {
        	echo '
            <div class="link_sub_active">
                <a href="convegni.php?anno='. date('Y') .'">'.date('Y').'</a>
            </div>
            <div class="link_sub">
                <a href="iscrizioni.php">Iscrizioni online</a>
            </div>
            <div class="link_sub">
                <a href="relazioni.php">Relazioni</a>
            </div>
            <div class="link_sub">
                <a href="convegni.php">Tutti i convegni</a>
            </div>';
        }
        elseif ($pagina == 'iscrizioni') {
        	echo '
            <div class="link_sub">
                <a href="convegni.php?anno='. date('Y') .'">'.date('Y').'</a>
            </div>
            <div class="link_sub_active">
                <a href="iscrizioni.php">Iscrizioni online</a>
            </div>
            <div class="link_sub">
                <a href="relazioni.php">Relazioni</a>
            </div>
            <div class="link_sub">
                <a href="convegni.php">Tutti i convegni</a>
            </div>';
        }
        elseif ($pagina == 'relazioni') {
        	echo '
            <div class="link_sub">
                <a href="convegni.php?anno='. date('Y') .'">'.date('Y').'</a>
            </div>
            <div class="link_sub">
                <a href="iscrizioni.php">Iscrizioni online</a>
            </div>
            <div class="link_sub_active">
                <a href="relazioni.php">Relazioni</a>
            </div>
            <div class="link_sub">
                <a href="convegni.php">Tutti i convegni</a>
            </div>';
        }
        elseif ($pagina == 'convegni') {
        	echo '
            <div class="link_sub">
                <a href="convegni.php?anno='. date('Y') .'">'.date('Y').'</a>
            </div>
            <div class="link_sub">
                <a href="iscrizioni.php">Iscrizioni online</a>
            </div>
            <div class="link_sub">
                <a href="relazioni.php">Relazioni</a>
            </div>
            <div class="link_sub_active">
                <a href="convegni.php">Tutti i convegni</a>
            </div>';
        }
        else {
        	echo '
            <div class="link_sub">
                <a href="convegni.php?anno='. date('Y') .'">'.date('Y').'</a>
            </div>
            <div class="link_sub">
                <a href="iscrizioni.php">Iscrizioni online</a>
            </div>
            <div class="link_sub">
                <a href="relazioni.php">Relazioni</a>
            </div>
            <div class="link_sub">
                <a href="convegni.php">Tutti i convegni</a>
            </div>';
        }
        echo "</div>";
    }

    function crea_servizi($pagina=NULL){
        echo '<div id="sottomenù">';
        if ($pagina == "prima") {
        	echo '
            <div class="link_sub_active">
                <a href="prima.php">Preparazione del congresso</a>
            </div>
            <div class="link_sub">
                <a href="durante.php">Svolgimento</a>
            </div>
            <div class="link_sub">
                <a href="dopo.php">Servizi finali</a>
            </div>
            <div class="link_sub">
                <a href="altro.php">Ulteriori servizi</a>
            </div>';
        }
        elseif ($pagina == 'durante') {
        	echo '
            <div class="link_sub">
                <a href="prima.php">Preparazione del congresso</a>
            </div>
            <div class="link_sub_active">
                <a href="durante.php">Svolgimento</a>
            </div>
            <div class="link_sub">
                <a href="dopo.php">Servizi finali</a>
            </div>
            <div class="link_sub">
                <a href="altro.php">Ulteriori servizi</a>
            </div>';
        }
        elseif ($pagina == 'dopo') {
        	echo '
            <div class="link_sub">
                <a href="prima.php">Preparazione del congresso</a>
            </div>
            <div class="link_sub">
                <a href="durante.php">Svolgimento</a>
            </div>
            <div class="link_sub_active">
                <a href="dopo.php">Servizi finali</a>
            </div>
            <div class="link_sub">
                <a href="altro.php">Ulteriori servizi</a>
            </div>';
        }
        elseif ($pagina == 'altro') {
        	echo '
            <div class="link_sub">
                <a href="prima.php">Preparazione del congresso</a>
            </div>
            <div class="link_sub">
                <a href="durante.php">Svolgimento</a>
            </div>
            <div class="link_sub">
                <a href="dopo.php">Servizi finali</a>
            </div>
            <div class="link_sub_active">
                <a href="altro.php">Ulteriori servizi</a>
            </div>';
        }
        else {
        	echo '
            <div class="link_sub">
                <a href="prima.php">Preparazione del congresso</a>
            </div>
            <div class="link_sub">
                <a href="durante.php">Svolgimento</a>
            </div>
            <div class="link_sub">
                <a href="dopo.php">Servizi finali</a>
            </div>
            <div class="link_sub">
                <a href="altro.php">Ulteriori servizi</a>
            </div>';
        }
        echo "</div>";
    }

    function chiudi_pagina() {
    	echo <<<HTML
    	</div>	<!--Chiude il contenitore-->
    	<div id="footer">
    		<div class="link_footer"><a href="mappa.php">Mappa del sito</a> | <a href="info_sito.php">Informazioni sul sito</a> | <a href="privacy.php">Privacy</a> | <a href="cookie.php">Cookies policy</a></div>
    		<div class="crediti">Sito progettato e realizzato da <a href="mailto:ricki.padovani@gmail.com">Riccardo Padovani</a></div>
    	</div>
    	</div><!--Chiude il contenitore generale-->
    	</body>
    	</html>
HTML;
    }
}
?>
