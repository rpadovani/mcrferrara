<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/



//Richiamiamo il file di configurazione che contiene le password di connessione al database, e il file di gestione delle eccezioni
require_once('configurazione.php');
require_once('eccezioni.php');

class gestisci_relazioni {
    private function crea_connessione() {
        //Connettiamoci al server
        $conn = new mysqli(SERVER, NOME, PASSWORD, DATABASE);
            
        //Se c'è un errore solleva un'eccezione
        if (mysqli_connect_errno() !== 0)
            throw new errore_database(mysqli_connect_error());
        
        //Se non sono stati sollevati errori ritorna la connessione al database
        return $conn;  
    }
    
    function elenca_relazioni() {
        //Connettiamoci al database
        $connessione = $this->crea_connessione();
        
        //Impostiamo l'utf-8 come set di connessione
        $connessione->query("SET NAMES 'utf8'");
        
        $relazioni = $connessione->query('SELECT * FROM convegni WHERE relazioni IS NOT NULL ORDER BY anno DESC, mese DESC, giorno DESC');
        
        //Scriviamo le relazioni una alla volta
        while (($relazione = @$relazioni->fetch_assoc()) !== NULL) {
            //Inseriamo la data
            echo "<div class='singolo_convegno'>";
                echo "<div class='data_singolo_convegno'>";
                    echo "<div class='giorno_singolo_convegno'>";    
                        echo $relazione['giorno'];
                    echo "</div>";    
                    echo "<div class='mese_singolo_convegno'>";    
                        $this->converti_mese($relazione['mese']);
                    echo "</div>";
                    echo "<div class='anno_singolo_convegno'>";
                        echo $relazione['anno'];
                    echo "</div>";
                echo "</div>";
                
                //Controlliamo che il titolo non sia troppo lungo; se lo è lo tagliamo
                if ((strlen($relazione['titolo'])) > 79) 
                    $relazione['titolo'] = substr($relazione['titolo'], 0, 80) . '...';
                    
                //Inseriamo il titolo
                echo "<div class='titolo_singolo_convegno'>";
                    echo $relazione['titolo'];
                    //Inseriamo il luogo
                    echo "<div class='luogo_singolo_convegno'>";
                        echo $relazione['luogo'];
                    echo "</div>";
                echo "</div>";
                
                //Eventualmente inseriamo le icone sotto
                if ((($relazione['società'])!==NULL) or (($relazione['download'])!==NULL) or (($relazione['relazioni'])!==NULL)) {
                    echo "<div class='icone_singolo_convegno'>";
                    
                        //Inseriamo l'icona della società
                    if (($relazione['società'])!=='') {
                        switch ($relazione['società']){
                            case "smc":
                                echo "<a href='smc.php' title='Organizzato dalla Smc'><img src='img/segreterie/smc.jpg' alt='Convegno Smc' class='immagine_icona_singolo_convegno' /></a>";
                                 break;
                         }         
                     }
                     
                     //Inseriamo l'icona del progamma in pdf
                     if (($relazione['download'])!=='' and ($relazione['download']!== NULL)) 
                         echo "<a href=".$relazione['download']." title='Scarica il programma del convegno'><img src='img/pdf.jpg' alt='Programma' class='immagine_icona_singolo_convegno' /></a>";
                         
                     //Inseriamo l'icona delle relazioni 
                     if (($relazione['relazioni'])!=='' and ($relazione['relazioni']!== NULL)) 
                         echo "<a href='dettagli_relazione.php?id=".$relazione['id_convegno']."' title='Scarica le relazioni del convegno'><img src='img/relazioni.jpg' alt='Relazioni' class='immagine_icona_singolo_convegno' /></a>";
                   
                    //Chiusura icone del singolo convegno
                    echo "</div>";
                }
                    
            //Inseriamo il link "Leggi tutto" che porta alla pagina dettagli_convegno
            echo "<div class='leggi_tutto_singolo_convegno'><a href='dettagli_relazione.php?id=".$relazione['id_convegno']."'>Elenco relazioni&#8594</a></div>";
                
            //Chiusura singolo convegno
            echo "</div>";  
        }  
        //Chiudiamo la connessione
        $connessione->close();
    }
    
    //Funzione che converte il mese da numero a lettere
    function converti_mese($mese) {
        switch($mese){
                case 1:
                    echo "gennaio";
                    break;
                    
                case 2:
                    echo "febbraio";
                    break;
                    
                case 3:
                        echo "marzo";
                    break;
                    
                case 4:
                    echo "aprile";
                    break;
                    
                case 5:
                    echo "maggio";
                    break;
                
                case 6:
                    echo "giugno";
                    break;
                
                case 7:
                    echo "luglio";
                    break;
                
                case 8:
                    echo "agosto";
                    break;
                
                case 9:
                    echo "settembre";
                    break;
                
                case 10:
                    echo "ottobre";
                    break;
                
                case 11:
                    echo "novembre";
                    break;
                
                case 12:
                    echo "dicembre";
                    break;
            }
    }
    
    function ultime_relazioni() {
        //Connettiamoci al server attraverso la funzione crea_connessione()
        $connessione = $this->crea_connessione();
        
        //Impostiamo l'utf-8 come set di connessione
        $connessione->query("SET NAMES 'utf8'");
        
        //La query deve identificare i prossimi 4 convegni
        $query = "SELECT id_convegno, titolo, giorno, mese, anno, luogo FROM convegni WHERE relazioni IS NOT NULL ORDER BY anno DESC, mese DESC, giorno DESC LIMIT 0, 4";
        
        //Eseguiamo la query
        $risultato = $connessione->query($query); 
        
        //Scriviamo i convegni uno alla volta
        while (($convegno = $risultato->fetch_assoc()) !== NULL) {
            echo '<div class="voce_contenitore_piccolo">
        <div class="titoletto_contenitore_piccolo">';
        //Controlliamo che il titolo non sia troppo lungo; se lo è lo tagliamo
        if ((strlen($convegno['titolo'])) > 35) 
            $convegno['titolo'] = substr($convegno['titolo'], 0, 30) . '...';
         echo '<a href="dettagli_relazione.php?id='.$convegno['id_convegno'].'">'.$convegno['titolo'].'</a></div>
        <div class="dettagli_contenitore_piccolo">'.$convegno['giorno'] . ' ';
        $this->converti_mese($convegno['mese']);
        echo ' '. $convegno['anno'].'</div>
    </div>';
    
        }
        //Chiudiamo la connessione
        $connessione->close();
    }
}
?>
