<?php
require_once('php/generatore.php');

$gen = new generatore("Servizi");
$gen->chiudi_head();
$gen->crea_body("servizi");
$gen->crea_servizi();
$gen->inserisci_contenuto();
?>
<h2>I servizi offerti da Mcr Ferrara</h2>
Mcr Ferrara offre servizi sempre all'avanguardia per soddisfare i bisogni di ogni singolo cliente; <br/>
si parte dalla <a href="prima.php">preparazione del congresso</a>, passando all'organizzazione per il miglior <a href="durante.php">svolgimento</a> dello stesso, concludendo con <a href="dopo.php">servizi finali</a> per rendere indimenticabile l'evento. <br/>
A seconda delle esigenze di ogni cliente offriamo <a href="altro.php">ulteriori servizi</a>, grazie all'aiuto di società terze di indubbia professionalità.<br/>
Ci <a href="mailto:info@mcrferrara.org">contatti</a> per una panoramica completa e per avere un preventivo su misura.
<?php
$gen->chiudi_pagina();
?>
