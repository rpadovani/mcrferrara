<?php
require_once('php/generatore.php');

$gen = new generatore("La nostra storia");
$gen->chiudi_head();
$gen->crea_body("chi_siamo");
$gen->crea_chi_siamo("chi_siamo");
$gen->inserisci_contenuto();
?>
<h2>La nostra storia</h2>
MCR nasce dalla lunga esperienza in campo congressuale della sua titolare, Maria Chiara Rippa. Laureatasi in Lingue e Letterature Straniere Moderne presso la Facoltà di Lettere e Filosofie dell'Università di Bologna, giornalista pubblicista dal 1989 al 1995, ha iniziato a lavorare in campo congressuale fin dal 1986, prima come collaboratrice esterna, poi come libera professionista. Responsabile dell'Ufficio Congressi della Società che ha gestito le manifestazioni celebrative in occasione del VI Centenario dell'Università di Ferrara dal 1989 al 1991, nel 1992 diventa titolare di Pangea Service, società di organizzazione congressuale ben nota in ambito ferrarese, sia per la qualità dei servizi forniti che per il numero di manifestazioni organizzate. Ora, a chiusura della suddetta società, ha deciso di continuare ad offrire l'esperienza acquisita con l'organizzazione di centinaia di eventi e di metterla a frutto in una nuova agenzia. MCR si avvale della collaborazione di vari professionisti dei diversi settori, secondo le esigenze specifiche di ogni Cliente, fornendo i servizi richiesti con la professionalità, la puntualità, la serenità e la cordialità che derivano dalla lunga esperienza della nostra titolare.
<?php
$gen->chiudi_pagina();
?>
