<?php
require_once('php/generatore.php');

$gen = new generatore("Ulteriori servizi");
$gen->chiudi_head();
$gen->crea_body("servizi");
$gen->crea_servizi("altro");
$gen->inserisci_contenuto();
?>
<h2>Ulteriori servizi</h2>
Mcr Ferrara offre ulteriori servizi, affinchè le esigenze di ogni cliente siano pienamente soddisfatte; <br/>
di seguito vengono mostrati vari esempi per diverse categorie. Per ulteriori informazioni ci <a href="mailto:info@mcrferrara.org">contatti</a>.<br/><br/>
<b>Allestimenti: </b>
<ul class="servizi">
   <li>&rArr; Addobbi floreali</li>
   <li>&rArr; Cartelloni</li>
   <li>&rArr; Creazione centri slides</li>
   <li>&rArr; Zone poster attrezzate</li>
   <li>&rArr; Individuazione e affitto sedi congressuali</li>
   <li>&rArr; Striscioni stradali</li>
</ul>
<b>Attrezzature:</b>
<ul class="servizi">
   <li>&rArr; Impianti di amplificazione, registrazione, proiezione audio e video</li>
   <li>&rArr; Impianti di traduzione simultanea con ricevitori via radio o a raggi infrarossi</li>
   <li>&rArr; Impianti di votazione elettronica</li>
   <li>&rArr; Impianti di rilevamento elettronico delle presenze </li>
</ul>
<b>Stampati:</b>
<ul class="servizi">
   <li>&rArr; Locandine, manifesti, inviti</li>
   <li>&rArr; Programmi preliminari e definitivi</li>
   <li>&rArr; Schede di iscrizione e schede alberghiere</li>
   <li>&rArr; Volumi di abstracts e di atti del congresso </li>
   <li>&rArr; Attestati di frequenza</li>
   <li>&rArr; Badges</li>
</ul>
<b>Personale:</b>
<ul class="servizi">
   <li>&rArr; Utilizzo di hostess qualificate multilingue</li>
   <li>&rArr; Utilizzo di interpreti qualificati, iscritti all'<a href="http://www.assointerpreti.it">Assointerpreti</a></li>
</ul>
<b>Ospitalità:</b>
<ul class="servizi">
   <li>&rArr; Gestione delle prenotazioni alberghiere presso la città in cui si svolge il convegno</li>
</ul>
<b>Catering:</b>
<ul class="servizi">
   <li>&rArr; Cene di gala</li>
   <li>&rArr; Cene di lavoro</li>
   <li>&rArr; Cocktails inaugurali</li>
   <li>&rArr; Coffe-breaks</li>
   <li>&rArr; Colazioni di lavoro</li>
</ul>
<b>Trasporti:</b>
<ul class="servizi">
   <li>&rArr; Autovetture con autista, aria condizionata e telefono</li>
   <li>&rArr; Minibus</li>
   <li>&rArr; Pullman</li>
</ul>
<b>Attività sociali:</b>
<ul class="servizi">
   <li>&rArr; Visite guidate alla città e ai suoi musei</li>
   <li>&rArr; Visite alle mostre</li>
   <li>&rArr; Visite alle città limitrofe</li>
   <li>&rArr; Organizzazione di serate musicali</li>
</ul>
<b>Cadeaux:</b>
<ul class="servizi">
   <li>&rArr; Omaggi per i Relatori</li>
   <li>&rArr; Omaggi per le Signore</li>
   <li>&rArr; Omaggi per i congressisti</li>  
</ul>
<b>Servizio stampa:</b>
<ul class="servizi"> 
   <li>&rArr; Contatti con i giornalisti delle testate locali e nazionali</li>
   <li>&rArr; Redazione e invio di comunicati stampa sul significato del congresso</li>
   <li>&rArr; Rapporti con i mass-media</li>
   <li>&rArr; Presenza sul nostro sito web</li>
   <li>&rArr; Creazione di un sito web dedicato</li>
</ul>
<b>Sponsor:</b>
<ul class="servizi">
   <li>&rArr; Ricerca sponsor su indicazione del committente</li>
</ul>
<b>Servizio fotografico:</b>
<ul class="servizi">
   <li>&rArr; Fotoservizio del convegno</li>
</ul>
<b>Gestione servizi amministrativi e fiscali:</b>
<ul class="servizi">
   <li>&rArr; Assistenza societaria</li>
   <li>&rArr; Assistenza fiscale</li>
   <li>&rArr; Elaborazione contabile dati</li>
</ul>
<b>Gestione contabile del convegno:</b>
<ul class="servizi">
   <li>&rArr; Raccolta delle quote di iscrizione al convegno</li>
   <li>&rArr; Ricevimento delle sponsorizzazioni</li>
   <li>&rArr; Gestionde del budget congressuale</li>
</ul>
<?php
$gen->chiudi_pagina();
?>
