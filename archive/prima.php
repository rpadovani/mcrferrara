<?php
require_once('php/generatore.php');

$gen = new generatore("Servizi pre-congressuali");
$gen->chiudi_head();
$gen->crea_body("servizi");
$gen->crea_servizi("prima");
$gen->inserisci_contenuto();
?>
<h2>Servizi pre-congressuali</h2>
Mcr Ferrara ritiene che la preparazione di un evento sia fondamentale per il successo dello stesso. <br/>
Grazie a una esperienza più che ventennale è in grado di offrire un ampio ventaglio di servizi, di cui qua vengono riportati solamente alcuni esempi:
<ul class="servizi">
    <li>&rArr; Contatti con i relatori da Voi indicati con conseguenti istruzioni delle priorità da seguire;</li>
    <li>&rArr; Coordinamento sponsor e raccolta documentazione per Ministero della Sanità;</li>
    <li>&rArr; Raccolta degli interventi dei relatori prima dell'inizio del congresso;</li>
    <li>&rArr; Gestione prenotazioni alberghiere;</li>
    <li>&rArr; Organizzazione programma sociale;</li>
    <li>&rArr; Preparazione materiale da inviare;</li>
    <li>&rArr; Preparazione materiale personalizzato per i congressisti e loro accompagnatori: kit congressuali, badges, omaggi, ecc.;</li>
    <li>&rArr; Assistenza stampa;</li>
    <li>&rArr; Proseguimento rapporti con sponsors intrapresi dal Comitato;</li>
    <li>&rArr; Preparazione contratto per sponsors;</li>
    <li>&rArr; Assistenza all'allestimento stands;</li>
</ul>
<?php
$gen->chiudi_pagina();
?>
