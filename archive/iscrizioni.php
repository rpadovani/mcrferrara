<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*File che crea le schede di iscrizioni*/
//Richiamiamo i file necessari
require_once('php/generatore.php');

//Creiamo la pagina
$gen = new generatore("Iscrizioni online");?>

<!--File jquery per la validazione del form-->
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>

<!--Script js che controlla la validità dei dati-->
<script type="text/javascript">
    $(document).ready(function() {
	    $("#iscrizione_online").validate({
		    rules: {
		        convegno: "required",
		        cognome: "required",
		        nome: "required",
		        //Il codice fiscale, essendo rivolto solamente a persone, deve essere esattamente di 16 caratteri
		        codice_fiscale: {
		            required: true,
		            maxlength: 16,
		            minlength: 16
		        },
		        luogo_nascita: "required",
		        data_nascita: "required",
		        indirizzo: "required",
		        città: "required",
		        provincia: "required",
		        cap: {
		            required: true,
		            maxlength: 5,
		            minlength: 5,
		            number: true
		        },
		        telefono: {
		            required: true,
		            number: true
		        },
		        cellulare: {
		            required: true,
		            number: true
		        },
		        fax: {
		            required: false,
		            number: true
		        },
		        email: {
		            required: true,
		            email: true
		        },
		        ente: "required",
		        ruolo: "required",
		        azienda: "required",
		        termini_1: "required",
		        termini_2: "required"
            },
            messages: {
                convegno: "Selezionare un convegno",
                cognome: "Inserire il cognome",
                nome: "Inserire il nome",
                codice_fiscale: {
                    required: "Il codice fiscale è obbligatorio",
                    maxlength: "La lunghezza del codice fiscale è di 16 caratteri alfanumerici",
                    minlength: "La lunghezza del codice fiscale è di 16 caratteri alfanumerici"
                },
                luogo_nascita: "Inserire il luogo di nascita",
                data_nascita: "Inserire la data di nascita",
                indirizzo: "Inserire l'indirizzo dell'abitazione",
                città: "Inserire la città di residenza",
                provincia: "Inserire la provincia di residenza",
                cap: {
                    required: "Il CAP è obbligatorio",
                    maxlength: "La lunghezza del CAP è di cinque caratteri numerici",
                    minlength: "La lunghezza del CAP è di cinque caratteri numerici",
                    number: "Sono permessi solamente numeri"
                },
                telefono: {
                    required: "Il numero di telefono è obbligatorio",
                    number: "Sono permessi solamente numeri. Niente spazi o trattini."
                },
                cellulare: {
                    required: "Il numero di cellulare è obbligatorio",
                    number: "Sono permessi solamente numeri. Niente spazi o trattini."
                },
                fax: "Sono permessi solamente numeri. Niente spazi o trattini.",
                email: {
                    required: "La mail è obbligatoria",
                    email: "Indirizzo email non valido"
                },
                ente: "Inserire l'ente di appartenenza",
                ruolo: "Selezionare una voce",
                azienda: "Selezionare la propria azienda sanitaria",
                termini_1: "Bisogna accettare il trattamento dei dati da parte dell'Università di Ferrara per iscriversi.",
                termini_2: "Bisogna accettare il trattamento dei dati da parte di Mcr Ferrara per iscriversi."
            }
	});
});
</script>
<?php
$gen->chiudi_head();
$gen->crea_body("convegni");
$gen->crea_convegni("iscrizioni");  

$gen->inserisci_contenuto();

?>
Al momento non sono disponibili iscrizioni online. Per informazioni sui futuri convegni scrivere a <a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a>
<?php /*
<fieldset>
    <legend class="legenda">Form di iscrizione online ai convegni di Mcr Ferrara</legend>
    
    <form name="iscrizione_online" id="iscrizione_online" method="post" action="php/validazione.php">
        <h2>Seleziona convegno</h2>
                <!--Campo di selezione di un convegno-->
                <label for="convegno" class="sx">Convegno:</label>
                <select id="convegno" name="convegno" class='generico'> 
                    <option value>[Seleziona convegno]</option>
                    <option value="1">Contraccezione 2012</option>
                </select>   
             
        <h2>Dati personali</h2>
                <label for="nome" class="sx">Nome:</label>
                <input id="nome" name="nome" type="text" /><br/> 
                  
                <label for="cognome" class="sx">Cognome:</label>
                <input id="cognome" name="cognome" type="text" /><br/>
                
                <label for="codice_fiscale" class="sx">Codice fiscale:</label>
                <input id="codice_fiscale" name="codice_fiscale" type="text" /><br/>
                
                <label for="luogo_nascita" class="sx">Luogo di nascita:</label>
                <input id="luogo_nascita" name="luogo_nascita" type="text" /><br/>
                
                <label for="data_nascita" class="sx">Data di nascita:</label>
                <input id="data_nascita" name="data_nascita" type="text" /><br/>
        
        <h2>Residenza</h2>
                <label for="indirizzo" class="sx">Indirizzo:</label>
                <input id="indirizzo" name="indirizzo" type="text" /><br/>
                
                <label for="città" class="sx">Città di residenza:</label>
                <input id="città" name="città" type="text" /><br/>
                
                <label for="provincia" class="sx">Provincia:</label>
                <input id="provincia" name="provincia" type="text" /><br/>
                
                <label for="cap" class="sx">CAP:</label>
                <input id="cap" name="cap" type="text" /><br/>
        
        <h2>Contatti</h2>
                <label for="telefono" class="sx">Numero di telefono:</label>
                <input id="telefono" name="telefono" type="text" /><br/> 
                
                <label for="cellulare" class="sx">Numero di cellulare:</label>
                <input id="cellulare" name="cellulare" type="text" /><br/>  
                
                <label for="fax" class="sx">Fax (facoltativo):</label>
                <input id="fax" name="fax" type="text" /><br/> 
                
                <label for="email" class="sx">Indirizzo email:</label>
                <input id="email" name="email" type="text" /><br/>
        
        <h2>Ente</h2>
            <label for="ente" class="sx">Ente di appartenenza:</label>
            <input id="ente" name="ente" type="text" /><br/>
            
            <label for="ruolo" class="sx">Ruolo:</label>
            <select id="ruolo" name="ruolo" class='generico'> 
                <option value>[Seleziona ruolo]</option>
                <option value="1">Medico</option>
                <option value="2">Ostetrico/a</option>
            </select> <br/>
            
            <label for="azienda" class="sx">Azienda sanitaria:</label>
            <select id="azienda" name="azienda" class='generico'> 
                <option value>[Seleziona azienda sanitaria]</option>
                <option value="1">AOU “S. Anna” di Ferrara</option>
                <option value="2">Altra Azienda Sanitaria</option>
            </select>    
                
        <h2>Termini d'utilizzo</h2>
                <b>L’iscrizione è gratuita ma consigliata.</b><br/><br/>
                <b>Consenso al trattamento dei dati personali:</b> i dati in oggetto saranno trattati da parte dell’Università di Ferrara con finalità di esecuzione delle richieste presenti su questo form e per fini statistici, di informazione e presentazione di altre iniziative. L’Università di Ferrara garantisce la riservatezza dei dati da Lei forniti in conformità all’art. 3 del D.L. n. 196/2003 “Codice in materia di protezione dei dati personali”.<br/>
                <label for="termini_1">Accetto i suddetti termini</label>
                <input class="termini" id="termini_1" name="termini_1" type="checkbox" /><br/>
                
                <b>Consenso al trattamento dei dati personali:</b> con l'invio della presente autorizzo MCR Ferrara a inserire i miei dati nell'elenco dei suoi clienti per l'invio di materiale informativo congressuale e a consegnarli ad aziende farmaceutiche/elettromedicali. <br/>
In ogni momento, a norma dell'art. 13 legge 675/96, potrò avere accesso ai miei dati, chiederne la modifica o la cancellazione oppure oppormi al loro utilizzo scrivendo a: MCR Ferrara Via Cittadella 31, 44121 Ferrara <br/>
                <label for="termini_2">Accetto i suddetti termini</label>
                <input class="termini" id="termini_2" name="termini_2" type="checkbox" /><br/>
                
                <input class="bottone" type="submit" value="Mi iscrivo!"/>
    </form>
</fieldset>
*/
?>

<?php
$gen->chiudi_pagina();
?>
