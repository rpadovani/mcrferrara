<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011-2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

//Richiamiamo il generatore della pagina
require_once('php/generatore.php');

//Inseriamo il titolo della pagina
$gen = new generatore("Pagina non trovata");
$gen->chiudi_head();
$gen->crea_body("");
$gen->inserisci_contenuto();
?>
<h2>Pagina non trovata</h2>
Siamo spiacenti, la pagina da lei cercata non esiste.<br/>
Forse vuole visionare la <a href="mappa.php">mappa del sito</a> per trovare ciò che stava cercando, oppure preferisce tornare all'<a href="index.php">home page</a>.<br/>
Se l'errore dovesse ripetersi, la preghiamo di <a href="mailto:ricki.padovani@gmail.com">segnalarcelo</a>.
<?php
$gen->chiudi_pagina();
?>
