<?php
require_once('php/generatore.php');

$gen = new generatore("Mappa del sito");
$gen->chiudi_head();
$gen->crea_body("");
$gen->inserisci_contenuto();
?>
<h2>La società</h2>
<ul class="mappa_sito">
    <li><a href="index.php">Home page</a></li>
    <li><a href="chi_siamo.php">Chi siamo</a></li>
    <li><a href="contatti.php">Contatti</a></li>
</ul>

<h2>I servizi offerti</h2>
<ul class="mappa_sito">
    <li><a href="prima.php">I servizi pre-congressuali</a></li>
    <li><a href="durante.php">I servizi durante il congresso</a></li>
    <li><a href="dopo.php">I servizi post-congressuali</a></li>
    <li><a href="altro.php">Servizi aggiuntivi</a></li>
</ul>

<h2>I convegni</h2>
<ul class="mappa_sito">
    <li><a href="iscrizioni.php">Iscrizioni online ai convegni</a></li>
    <li><a href="relazioni.php">Elenco delle relazioni disponibili dei vari convegni</a></li>
    <?php 
        $anno_fine = date("Y");
        $anno_inizio = 2003;
        
        //Creaiamo un while che scriva tutti gli anni presenti, dal primo all'ultimo
        $anno_attuale = $anno_inizio;
        while ($anno_attuale <= $anno_fine){
            echo "<li><a href='convegni.php?anno=$anno_attuale'>Elenco convegni dell'anno $anno_attuale </a></li>";
            $anno_attuale++;
        }
    ?>
</ul>
    
<h2>Le segreterie scientifiche</h2>
<ul class="mappa_sito">
    <li><a href="sedi.php">Sedi</a></li>
    <li><a href="sifl.php">Sifl</a></li>
    <li><a href="smc.php">Smc</a></li>
</ul>
<?php
$gen->chiudi_pagina();
?>
