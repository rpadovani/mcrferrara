<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011-2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5, HTML 4
Tabulazioni: 4 spazi
Nomi separati da underscore
*/ 
//Richiamiamo il file che genera la pagina
require_once('php/generatore.php');

//Impostiamo il titolo della pagina
$gen = new generatore("Sedi");

//Inseriamo il CSS aggiuntivo per le segreterie
echo '<link rel="stylesheet" type="text/css" href="css/segreterie.css" />';

//Chiudiamo il tag <head>
$gen->chiudi_head();

//Inseriamo il menù superiore selezionato su segreterie scientifiche
$gen->crea_body('società');

//Inseriamo il sottomenù selezionato sulla voce "Sedi"
$gen->crea_societa('sedi');

//Inseriamo il body
$gen->inserisci_contenuto();
?>
<h2>Informazioni</h2>
<p><img src="img/segreterie/sedi.jpg" alt="sedi" class="img_sedi_pagina" /><br/>
Il SEDI, sindacato endoscopisti digestivi italiani, è un sindacato medico con la finalità di tutelare ad ogni livello il ruolo dirigente e l'autonomia professionale dei medici endoscopisti digestivi di qualsiasi estrazione anche nel più ampio contesto delle discipline a cui afferiscono (Gastroenterologia, Chirurgia, Medicina interna, Pediatria). Il Sedi è socio fondatore della Fesmed, Federazione Sindacale Medici Dirigenti, che raggruppa , oltre agli endoscopisti del Sedi, i chirurghi dell'ACOI, i ginecologi-ostetrici dell'AOGOI, i cardiologi dell'ANMCO, i medici di direzione Sanitaria del'ANMDO, i medici degli enti previdenziali della FEMEPA e il sindacato generalista SUMI. La Fesmed è sindacato a tutti gli effetti riconosciuto dal Ministero della Sanità, firmatario del Contratto Nazionale della Dirigenza Medica e convocato ad ogni riunione ufficiale sia a livello Nazionale che Regionale che Aziendale.</p> 
<h2>Contatti</h2>
<table class="tabella_segreterie"> 
	<tr><td class="fisso">Sede: </td><td class="descr">Via Comelico 3, 20135 Milano</td></tr> 
	<tr><td class="fisso">Sito internet: </td><td class="descr"><a href="http://www.sindacatosedi.it">www.sindacatosedi.it</a></td></tr> 
	<tr><td class="fisso">Indirizzo email: </td><td class="descr"><a href="mailto:info@sindacatosedi.it">info@sindacatosedi.it</a><br/></td></tr> 
</table>
<?php

//Chiudiamo la pagina
$gen->chiudi_pagina();
?>
