<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011-2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5, HTML 4
Tabulazioni: 4 spazi
Nomi separati da underscore
*/ 
//Richiamiamo il file che genera la pagina
require_once('php/generatore.php');

//Impostiamo il titolo della pagina
$gen = new generatore("Sifl");

//Inseriamo il CSS aggiuntivo per le segreterie
echo '<link rel="stylesheet" type="text/css" href="css/segreterie.css" />';

//Chiudiamo il tag <head>
$gen->chiudi_head();

//Inseriamo il menù superiore selezionato su segreterie scientifiche
$gen->crea_body('società');

//Inseriamo il sottomenù selezionato sulla voce "Sifl"
$gen->crea_societa('sifl');

//Iniziamo il tag body
$gen->inserisci_contenuto();
?>
<h2>Informazioni</h2>
<p><img src="img/segreterie/sifl.jpg" alt="sifl" width="369px" height="72px" class="img_sifl_pagina" /><br/>
	La Società si pone come scopo quello di promuovere e diffondere il corretto approccio diagnostico-terapeutico alle malattie dei linfatici e del sistema venoso. Tale associazione ha altresì lo scopo di definire la figura del flebologo e del linfologo, favorendone il perfezionamento post- universitario al fine di definirne, valorizzarne, qualificarne la professione e la professionalità.</p> 
<h2>Contatti</h2>
<table class="tabella_segreterie"> 
	<tr><td class="fisso">Sito internet: </td><td class="descr"><a href="http://www.sifl.it">www.sifl.it</a><br/></td></tr> 
	<tr><td class="fisso">Indirizzo email: </td><td class="descr"><a href="mailto:sifl@mcrferrara.org">sifl@mcrferrara.org</a><br/></td></tr> 
</table>
<h2>Organigramma</h2>
<table class="tabella_segreterie"> 
	<tr><td class="fisso">Presidente: </td><td class="descr">Prof. Ugo Alonzo</td></tr> 
	<tr><td class="fisso">Segretario esecutivo: </td><td class="descr">Prof. Raffaele Serra </td></tr> 
</table>
<?php
$gen->chiudi_pagina();
?>
