<?php
require_once('php/generatore.php');

$gen = new generatore("Servizi post-congressuali");
$gen->chiudi_head();
$gen->crea_body("servizi");
$gen->crea_servizi("dopo");
$gen->inserisci_contenuto();
?>
<h2>Servizi post-congressuali</h2>
Per far si che l'evento si concluda senza intoppi, Mcr Ferrara offre vari servizi, fra cui:
<ul class="servizi">
    <li>&rArr; Chiusura del convegno;</li>
    <li>&rArr; Redazione bilancio consuntivo;</li>
</ul>
<?php
$gen->chiudi_pagina();
?>
