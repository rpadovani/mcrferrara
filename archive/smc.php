<?php
/*
Project: Mcr Ferrara - Sito ufficiale
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011-2012 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
PHP 5, HTML 4
Tabulazioni: 4 spazi
Nomi separati da underscore
*/
//Richiamiamo il file che genera la pagina
require_once('php/generatore.php');

//Impostiamo il titolo della pagina
$gen = new generatore("Smc");

//Inseriamo il CSS aggiuntivo per le segreterie
echo '<link rel="stylesheet" type="text/css" href="css/segreterie.css" />';

//Chiudiamo il tag <head>
$gen->chiudi_head();

//Inseriamo il menù superiore selezionato su segreterie scientifiche
$gen->crea_body('società');

//Inseriamo il sottomenù selezionato sulla voce "Smc"
$gen->crea_societa('smc');

//Apriamo il body
$gen->inserisci_contenuto();
?>
<h2>Informazioni</h2>
<p>
	<img src="img/segreterie/smc.jpg" alt="smc" height="59px" width="106px" class="img_smc_pagina" /><br/>
	La Società Medico Chirurgica (S.M.C.) di Ferrara, è un organismo scientifico che, da più di 60 anni, ha come scopo principale l'aggiornamento scientifico dei medici della nostra provincia, con una stretta collaborazione tra le varie categorie mediche : universitaria, ospedaliera, medici di famiglia. Vengono organizzati circa 10 incontri l'anno, con la collaborazione delle UU.OO. dell'Arcispedale S.Anna, degli Ospedali del territorio e con l'apporto di esperti nazionali ed internazionali.
</p>
<h2>Contatti</h2>
<table class="tabella_segreterie">
	<tr><td class="fisso">Sede: </td><td class="descr">Via Cittadella 31, 44121 Ferrara<br/></td></tr>
	<tr><td class="fisso">Sito internet: </td><td class="descr"><a href="http://www.ospfe.it">www.ospfe.it</a><br/></td></tr>
	<tr><td class="fisso">Indirizzo email: </td><td class="descr"><a href="mailto:smc@mcrferrara.org">smc@mcrferrara.org</a><br/></td></tr>
</table>
<h2>Organigramma</h2>
<table class="tabella_segreterie">
	<tr><td class="fisso">Presidente: </td><td class="descr">Prof. Roberto Manfredini</td></tr> 
</table>
<?php
$gen->chiudi_pagina();
?>
