<?php
require_once('php/generatore.php');

$gen = new generatore("Relazioni");
$gen->chiudi_head();
$gen->crea_body("convegni");
$gen->crea_convegni('relazioni');
$gen->inserisci_contenuto();
$id_relazione = '0';
if (isset($_GET['id'])) {
    $id_relazione = $_GET['id'];
    $url_prefix = 'download/relazioni/' . $id_relazione . '/';
}

switch ($id_relazione) {
    case '445':
        ?>
        <h2>Il trattamento logopedico nel paziente complesso con Impianto Cocleare</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Barbot.pdf">"L'impianto cocleare nei bambini con ipoplasia del nervo"</a></b> <i>di A. Barbot</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Minazzi.pdf">"L’impianto cocleare nell’anziano: caratteristiche della terapia logopedica"</a></b> <i>di F. Minazzi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Moalli.pdf">"L'impianto cocleare nel bambino prematuro"</a></b> <i>di R. Moalli, M. F. Attardo, A. Galli, A. Cuscunà</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Montino.pdf">"Autismo e sordità: la complessità della valutazione logopedico-percettiva"</a></b> <i>di S. Montino</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Nicastri.pdf">"L’impianto cocleare nel bambino con disprassia"</a></b> <i>di M. Nicastri, P. Mancini, I. Giallini</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Nicastro.pdf">"L’impianto cocleare nei casi di ritardo mentale"</a></b> <i>di R. Nicastro</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '442':
        ?>
        <h2>Percorsi diagnostico-terapeutici in endoscopia digestiva</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Ambrosio.pdf">"I percorsi diagnostici e terapeutici dei NET pancreatici e gastrointestinali"</a></b> <i>di M. R. Ambrosio</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Carradori.pdf">"Integrazione interaziendale: quali orizzonti?"</a></b> <i>di T. Carradori</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Cifala.pdf">"Caso clinico “live”"</a></b> <i>di V. Cifalà</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Fabbri.pdf">"Endoscopia e lesione solide del pancreas"</a></b> <i>di C. Fabbri</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Fabbri2.pdf">"Endoscopia e lesione solide del pancreas: seconda parte"</a></b> <i>di C. Fabbri</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Fantin.pdf">"Cystic Pancreatic Neoplasms"</a></b> <i>di A. Fantin</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Pagano.pdf">"I ruolo dell’endoscopia nella diagnosi e nella cura delle formazioni sottomucose del tratto gastroenterico"</a></b> <i>di N. Pagano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Pezzoli.pdf">"La gestione della litiasi del coledoco"</a></b> <i>di A. Pezzoli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Tilli.pdf">"Metodiche radiologiche di diagnosi delle neoformazioni pancreatiche"</a></b> <i>di M. Tilli</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '441':
        ?>
        <h2>Aggiornamenti in Urologia: Specialisti e MMG a confronto</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Brandimarte.pdf">"Inquadramento clinico della litiasi urinaria"</a></b> <i>di A. Brandimarte</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Bruschi.pdf">"Ematuria all’attenzione del medico di famiglia sintomo o malattia?"</a></b> <i>di B. Bruschi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Galizia.pdf">"Neoplasie uroteliali"</a></b> <i>di G. Galizia</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Galizia2.pdf">"IPB: quale trattamento?"</a></b> <i>di G. Galizia</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Luciano.pdf">"Stato dell'arte su PSA"</a></b> <i>di M. Luciano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Luciano2.pdf">"Trattamento del carcinoma prostatico"</a></b> <i>di M. Luciano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Rinaldi.pdf">"Il tumore prostatico: la chirurgia"</a></b> <i>di R. Rinaldi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Sala.pdf">"Il tumore prostatico: ruolo della risonanza magnetica"</a></b> <i>di S. Sala</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Simone.pdf">"Il tumore prostatico: la chirurgia"</a></b> <i>di M. Simone</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Simone2.pdf">"La litiasi: terapia tailored oppure no?"</a></b> <i>di M. Simone</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Simone3.pdf">"Le neoplasie uroteliali: terapia chirurgica ma non solo"</a></b> <i>di M. Simone</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Zambino.pdf">"Inquadramento clinico: dai LUTS alla Ritenzione Urinaria: Il ruolo del MMG"</a></b> <i>di C. Zambino</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '422':
        ?>
        <h2>Società Italiana di Andrologia - XXI Congresso nazionale della sezione MERS "L'Andrologia Sociale"</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Andriani.pdf">"Andrologia ed internet: sanità 2.0"</a></b> <i>di E. Andriani</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Caraceni.pdf">"La chirurgia protesica: quali risultati quale soddisfazione"</a></b> <i>di E. Caraceni</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Cavallini.pdf">"La gestione andrologica del paziente psicogeno."</a></b> <i>di G. Cavallini</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Fabiani.pdf">"HPV e MST nella pratica ambulatoriale"</a></b> <i>di A. Fabiani</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Franceschelli.pdf">"AZOOSPERMIA - Le tecniche di recupero degli spermatozoi"</a></b> <i>di A. Franceschelli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Gentile.pdf">"Uso e somministrazione selvaggia di farmaci"</a></b> <i>di G. Gentile</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Granieri.pdf">"Il dolore in andrologia"</a></b> <i>di E. Granieri</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Maretti.pdf">"Nuove frontiere nel trattamento dell'infertilità idiopatica"</a></b> <i>di C. Maretti</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Meneghini.pdf">"Andrologia e Urologia: analogie e differenze"</a></b> <i>di A. Meneghini</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Pescatori.pdf">"La chirurgia Protesica: quale protesi per quale paziente"</a></b> <i>di E. Pescatori</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Polito.pdf">"Interpretazione dello spermiogramma"</a></b> <i>di M. Polito</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Quaresma.pdf">"La traumatologia del pene"</a></b> <i>di L. Quaresma</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Scarano.pdf">"Il ruolo delle infezioni sessualmente trasmesse negli insuccessi della PMA"</a></b> <i>di P. Scarano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Scarano2.pdf">"Infertilità e infiammazione"</a></b> <i>di P. Scarano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Troilo.pdf">"Tecniche e significato della selezione spermatica per ICSI"</a></b> <i>di E. Troilo, L. Parmegiani</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Zenico.pdf">"Andrologia : fitoterapici ed integratori alimentari"</a></b> <i>di T. Zenico</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '421':
        ?>
        <h2>Hcv e....... Reumatologia</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Boccia.pdf">"Workshop HCV"</a></b> <i>di S. Boccia</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Sighinolfi.pdf">"Epatite da HCV e HIV in reumatologia"</a></b> <i>di L. Sighinolfi</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '420':
        ?>
        <h2>Hcv e....... Nefrologia</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Boccia.pdf">"Workshop HCV"</a></b> <i>di S. Boccia</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Grilli.pdf">"Proposta di un percorso HCV nei pazienti nefropatici"</a></b> <i>di A. Grilli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Sighinolfi.pdf">"Epatite da HCV e HIV in nefrologia"</a></b> <i>di L. Sighinolfi</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '419':
        ?>
        <h2>I device dei farmaci respiratori: quale ruolo nella scelta terapeutica?</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Centenaro.pdf">"La prescrizione e l’ educazione del paziente all'utilizzo dei device respiratori"</a></b> <i>di G. Centenaro</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Contoli.pdf">"Asma e BPCO: le strategie terapeutiche"</a></b> <i>di M. Contoli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Descovich.pdf">"Esperienze regionali di PDTA nelle patologie respiratorie - AVEC"</a></b> <i>di C. Descovich</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Ermini.pdf">"La prescrizione e l’educazione del paziente all’utilizzo dei device respiratori sul territorio"</a></b> <i>di G. Ermini</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Fedele.pdf">"Il ruolo del farmacista SSN nel corretto uso dei device respiratori"</a></b> <i>di D. Fedele</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Lavorini.pdf">"La rilevanza del device nella scelta della strategia terapeutica e criteri di scelta del device in relazione al paziente"</a></b> <i>di F. Lavorini</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Marata.pdf">"Strategie per l’inserimento in Prontuario dei farmaci respiratori in Emilia Romagna"</a></b> <i>di A. M. Marata</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Papi.pdf">"Gli Studi Clinici sui devices respiratori"</a></b> <i>di A. Papi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Peroni.pdf">"La scelta dei device respiratori in ambito pediatrico in relazione alle strategie terapeutiche"</a></b> <i>di D. Peroni</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Rossi.pdf">"Esperienze regionali di PDTA nelle patologie respiratorie"</a></b> <i>di L. Rossi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Russi.pdf">"Documento di indirizzo relativo ai farmaci per la broncopneumopatia cronica ostruttiva (BPCO)"</a></b> <i>di E. Russi</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '418':
        ?>
        <h2>Ostetricia e Ginecologia 2017 - Riconoscere i rischi associati all'obesità</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Battaglia.pdf">"Menopausa e B.M.I. stima del rischio e prevenzione. Ruolo della MHT"</a></b> <i>di C. Battaglia</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Cancellieri.pdf">"Prospettive d’impiego in uro-ginecologia di trattamenti a base di acido ialuronico a bassissimo peso molecolare"</a></b> <i>di F. Cancellieri</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Costantino.pdf">"Il segreto di una buona gravidanza poggia su corrette abitudini alimentari ed un adeguato esercizio fisico"</a></b> <i>di D. Costantino</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Dinicola.pdf">"ruolo, distribuzione e biodisponibilità dell’acido ialuronico (HA) ad alto e basso peso molecolare"</a></b> <i>di S. Dinicola</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Grandi.pdf">"Sovrappeso. Obesità e la pillola contraccettiva: profilo di tollerabilità e di sicurezza"</a></b> <i>di G. Grandi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Gregoratti.pdf">"Sovrappeso e obesità, perché andare oltre la dieta"</a></b> <i>di G. Gregoratti</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Guaraldi.pdf">"Contraccezione con solo progestinico: intrauterino - sottocutaneo"</a></b> <i>di C. Guaraldi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Petrella.pdf">"Prevenire i rischi perinatali associati all’obesità"</a></b> <i>di E. Petrella</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Roncarati.pdf">"Le sfide nella cura dell’obesità: perché guardare oltre il peso"</a></b> <i>di E. Roncarati</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Tanas.pdf">"Sovrappeso e obesità nella bambina e nell'adolescente"</a></b> <i>di R. Tanas</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;

    case '417':
        ?>
        <h2>Team working in emergenza-urgenza: dal territorio alla medicina d'urgenza in un lavoro di equipe multiprofessionale e multidisciplinare</h2>
        <ul class="elenco_relazioni">
            <li><b><a href="<?php echo $url_prefix; ?>Barchetti.pdf">"Il team e i familiari: presenza sulla scena e comunicazione efficace"</a></b> <i>di E. Barchetti</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Borrelli.pdf">"Dalle aule alla realtà: una esperienza di lavoro in un team multiculturale e multiprofessionale"</a></b> <i>di F. Borelli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Cariani.pdf">"L’addestramento al lavoro in TEAM"</a></b> <i>di D. Cariani</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Chierici.pdf">"Competenze infermieristiche nella gestione del paziente con dispnea acuta in PS/Shock Room"</a></b> <i>di F. Chierici</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Ferrari.pdf">"Pump o Lung Failure? Ventimask, CPAP o NIV?"</a></b> <i>di R. Ferrari</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Gangitano.pdf">"Tavola rotonda: “Ventiliamo dunque siamo...” gestione del paziente in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto"</a></b> <i>di G. Gangitano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Gangitano1.pdf">"Conclusioni tavola rotonda"</a></b> <i>di G. Gangitano</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Gelati.pdf">"La comunicazione all’interno del Team: mezzo di riduzione del rischio clinico e eventi avversi ?"</a></b> <i>di E. Gelati</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Giacometti.pdf">"IL POLITRAUMA: quale ruolo per l’ospedale non Trauma Center Regionale (Hub e Spoke) nella catena gestionale dall’extra all’intraospedaliero"</a></b> <i>di M. Giacometti</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Govoni.pdf">"Insufficienza respiratoria acuta in età pediatrica. Il ruolo del pediatra in PS"</a></b> <i>di M. R. Govoni</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Guatteri.pdf">"L’esperienza dell’auto infermieristica di Reggio Emilia"</a></b> <i>di R. Guatteri</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Guzzinati.pdf">"Tavola rotonda: “Ventiliamo dunque siamo...“ gestione del pz in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto"</a></b> <i>di I. Guzzinati</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Lupacciolu.pdf">"Approccio razionale nel pz con insufficienza respiratoria acuta dall’extra all’intraospedaliero"</a></b> <i>di S. Lupacciolu</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Minelli.pdf">"Tavola rotonda: « ventiliamo dunque siamo ...» gestione del paz in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto"</a></b> <i>di E. Minelli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Morelli.pdf">"Ventilazioni in MURG – Ravenna"</a></b> <i>di E. Morelli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Morselli.pdf">"Quando a casa manca il respiro"</a></b> <i>di C. Morselli</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Perin.pdf">"Tavola rotonda: “ventiliamo dunque siamo...” gestione del paziente in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto"</a></b> <i>di T. Perin</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Polito.pdf">"Quale stabilizzazione possibile in un PS SPOKE prima del trasferimento ad un PS HUB"</a></b> <i>di J. Polito</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Previati.pdf">"Insufficienza respiratoria acuta in età pediatrica l’intervento del medico d’urgenza"</a></b> <i>di R. Previati</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Serra1.pdf">"Quale addestramento al lavoro in team nel percorso formativo degli specializzandi in medicina d’emergenza urgenza: una valutazione del CoSMEU"</a></b> <i>di E. Serra</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Serra2.pdf">"Standardizzazione della Comunicazione tra Centri Spoke e Hub nella Centralizzazione e Decentralizzazione delle patologie tempo dipendenti"</a></b> <i>di E. Serra</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Smorgon.pdf">"Gestione avanzata delle vie aeree in emergenza"</a></b> <i>di C. Smorgon</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Trazzi.pdf">"Ruolo dell’Infermiere nel Trauma Team in extra-ospedaliero"</a></b> <i>di A. Trazzi</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Vitali.pdf">"Tavola rotonda: Ventiliamo quindi siamo...La gestione delaziente in NIV in Medicina d’ Urgenza diversi modelli rganizzativi a confronto"</a></b> <i>di E. Vitali</i></li>
            <li><b><a href="<?php echo $url_prefix; ?>Zecchi.pdf">"La gestione extra ed intraospedaliera di un trauma grave nell’HUB: sistema organizzativo e dati dell’OCSAE di Baggiovara"</a></b> <i>di G. Zecchi</i></li>
        </ul>
        <h2>Note</h2>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.<br/>
        Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
        <?php

        break;
  case '416':
?>
<h2>La gestione multidisciplinare delle Infezioni Complicate delle Vie Urinarie nel terzo millennio</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/416/Bartolesi.pdf">"Patogeni emergenti nelle infezioni urinarie complicate"</a></b> <i>di A. M. Bartolesi</i></li>
  <li><b><a href="download/relazioni/416/Cattaneo.pdf">"La terapia antibiotica nel paziente con insufficienza renale: confronto con il nefrologo..."</a></b> <i>di D. Cattaneo</i></li>
  <li><b><a href="download/relazioni/416/Giuri.pdf">"Anamnesi Patologica Remota"</a></b> <i>di G. Giuri</i></li>
  <li><b><a href="download/relazioni/416/LaManna.pdf">"Infezioni nel trapianto di rene: aspetti epidemiologici, clinici e terapeutici"</a></b> <i>di G. La Manna</i></li>
  <li><b><a href="download/relazioni/416/Mandreoli.pdf">"La terapia antibiotica nel paziente con insufficienza renale: farmacologo e nefrologo a confronto"</a></b> <i>di M. Mandreoli</i></li>
  <li><b><a href="download/relazioni/416/Matteelli.pdf">"La tubercolosi urinaria"</a></b> <i>di A. Matteelli</i></li>
  <li><b><a href="download/relazioni/416/Nalesso.pdf">"Riconoscimento precoce e gestione clinica della sepsi urinaria: il ruolo del nefrologo"</a></b> <i>di F. Nalesso</i></li>
  <li><b><a href="download/relazioni/416/Prati.pdf">"La Chirurgia Urologica nelle infezione complicate delle vie urinarie"</a></b> <i>di A. Prati</i></li>
  <li><b><a href="download/relazioni/416/Tascini.pdf">"Micosi urinaria: come discriminare tra patologia e colonizzazione e indicazioni sull’impiego degli antifungini sistemici."</a></b> <i>di C. Tascini</i></li>
  <li><b><a href="download/relazioni/416/Tumietto.pdf">"Aspetti clinici e nuove opportunità terapeutich"</a></b> <i>di F. Tumietto</i></li>
  <li><b><a href="download/relazioni/416/Volta.pdf">"Riconoscimento precoce e gestione clinica della sepsi urinaria: ruolo dell’intensivista"</a></b> <i>di C. A. Volta</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;

  case '400':
?>
<h2>Meeting regionale GASTROCARE</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/400/Bassi.pdf">"La preparazione ottimale nella colonscopia"</a></b> <i>di R. Bassi</i></li>
  <li><b><a href="download/relazioni/400/Bruni.pdf">"Sedazione e Sicurezza: il monitoraggio del paziente"</a></b> <i>di A. Bruni</i></li>
  <li><b><a href="download/relazioni/400/Graziadio.pdf">"Preparazione e gestione del paziente a breve termine"</a></b> <i>di M. Graziadio</i></li>
  <li><b><a href="download/relazioni/400/Inca.pdf">"Il percorso della terapia nelle IBD"</a></b> <i>di R. D'Incà</i></li>
  <li><b><a href="download/relazioni/400/Matarese.pdf">"Come affrontare e diluire l’ansia"</a></b> <i>di G. C. Matarese</i></li>
  <li><b><a href="download/relazioni/400/Merighi.pdf">"Sedazione e Sicurezza: il monitoraggio del paziente"</a></b> <i>di A. Merighi, A. Bruni</i></li>
  <li><b><a href="download/relazioni/400/Triossi.pdf">"La colonscopia di qualità nell’epoca dello screening. La preparazione ottimale"</a></b> <i>di O. Triossi</i></li>
  <li><b><a href="download/relazioni/400/Tumedei.pdf">"IBD UNIT: L'esperienza forlivese del «CASE MANAGEMENT». Verso un modello gestionale multiprofessional"</a></b> <i>di D. Tumedei</i></li>
  <li><b><a href="download/relazioni/400/Valpiani.pdf">"IBD Unit: modello multidisciplinare e multiprofessionale in continua evoluzione"</a></b> <i>di D. Valpiani</i></li>
  <li><b><a href="download/relazioni/400/Zanotto.pdf">"Obiettivo paziente"</a></b> <i>di V. Zanotto</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;

  case '399':
?>
<h2>Ostetricia e Ginecologia 2016</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/399/Costantino.pdf">"Acido alfa-lipoico vs progesterone nella minaccia di aborto: quali evidenze?"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/399/Costantino2.pdf">"Preliminari di uno studio ala e diabete gestazionale"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/399/DeSeta.pdf">"Contraccezione e via vaginale"</a></b> <i>di F. De Seta</i></li>
  <li><b><a href="download/relazioni/399/Fruzzetti.pdf">"Contraccettivo orale a regime prolungato e benefici terapeutici dalla riduzione della frequenza mestruale"</a></b> <i>di F. Fruzzetti</i></li>
  <li><b><a href="download/relazioni/399/Guaraldi.pdf">"Ipercontrattilità uterina"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/399/Guaraldi2.pdf">"Ruolo del ginecologo nel trattamento delle patologie mammarie benigne"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/399/Neri.pdf">"Ruolo della L-Arginina nella prevenzione e nel trattamento della preeclampsia"</a></b> <i>di I. Neri</i></li>
  <li><b><a href="download/relazioni/399/Paoletti.pdf">"Nuove opportunità in contraccezione: l’associazione EE e GSD per via transdermica"</a></b> <i>di A. M. Paoletti</i></li>
  <li><b><a href="download/relazioni/399/Zannoni.pdf">"L’evoluzione della terapia medica nel trattamento dell’endometriosi"</a></b> <i>di L. Zannoni</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;

  case '398':
?>
<h2>Le malattie infettive del migrante e del viaggiatore</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/398/Bartoloni.pdf">"Infezioni correlate ai viaggi e Arbovirosi"</a></b> <i>di A. Bartoloni</i></li>
  <li><b><a href="download/relazioni/398/Cacopardo.pdf">"MST: i dati della Sicilia"</a></b> <i>di B. Cacopardo</i></li>
  <li><b><a href="download/relazioni/398/Calleri.pdf">"Chemioprofilassi antimalarica"</a></b> <i>di G. Calleri</i></li>
  <li><b><a href="download/relazioni/398/Chirianni.pdf">"HIV/AIDS: attualità epidemiologiche e prospettive terapeutiche nella popolazione migrante in Italia"</a></b> <i>di A. Chirianni</i></li>
  <li><b><a href="download/relazioni/398/DiNuzzo.pdf">"Tubercolosi extrapolmonare nella popolazione straniera a Ferrara"</a></b> <i>di M. C. Di Nuzzo</i></li>
  <li><b><a href="download/relazioni/398/Geraci.pdf">"Rifugiati e Migranti: implicazioni sociali e sanitarie"</a></b> <i>di S. Geraci</i></li>
  <li><b><a href="download/relazioni/398/Goletti.pdf">"Tubercolosi e Screening tra i Migranti"</a></b> <i>di D. Goletti</i></li>
  <li><b><a href="download/relazioni/398/Mazzariol.pdf">"Movimenti migratori e resistenza agli antibiotic"</a></b> <i>di A. M. Mazzariol</i></li>
  <li><b><a href="download/relazioni/398/Mazzocco.pdf">"Correlazione tra TB e Malnutrizione Acuta Severa nei pazienti pediatrici"</a></b> <i>di M. Mazzocco</i></li>
  <li><b><a href="download/relazioni/398/Prestileo.pdf">"L’insegnamento di Lampedusa: il risvolto della medaglia"</a></b> <i>di T. Prestileo</i></li>
  <li><b><a href="download/relazioni/398/Puoti.pdf">"Strategie terapeutiche attuali nel migrante con epatite virale"</a></b> <i>di M. Puoti</i></li>
  <li><b><a href="download/relazioni/398/Segala.pdf">"Patologie infettive tra i migranti rifugiati e richiedenti asilo nella provincia di Ferrara"</a></b> <i>di D. Segala</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;

  case '397':
?>
<h2>Convegno Nazionale “Microbiologia Clinica e Infettivologia: una combinazione sinergica per una nuova realtà sanitaria”</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/397/Pan.pdf">"Clostridium difficile: terapia eziologica e preventiva"</a></b> <i>di A. Pan</i></li>
  <li><b><a href="download/relazioni/397/Sarti.pdf">"I dati epidemiologici a supporto delle strategie di Antimicrobial Stewardship"</a></b> <i>di M. Sarti</i></li>
  <li><b><a href="download/relazioni/397/Viale.pdf">"Fast microbiology: Applicazioni cliniche"</a></b> <i>di P. Viale</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
  case '388':
?>
<h2>Medicina, cura e genere</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/388/Andreati.pdf">"Aspetti di Genere in Geriatria"</a></b> <i>di C. Andreati</i></li>
  <li><b><a href="download/relazioni/388/Bagnaresi.pdf">"Espressioni di genere nell’arte moderna"</a></b> <i>di I. Bagnaresi</i></li>
  <li><b><a href="download/relazioni/388/Gallerani.pdf">"Sepsi e differenze di genere"</a></b> <i>di M. Gallerani</i></li>
  <li><b><a href="download/relazioni/388/Loss.pdf">"Salute e genere nella cultura infermieristica"</a></b> <i>di C. Loss</i></li>
  <li><b><a href="download/relazioni/388/Manfredini.pdf">"ardiomiopatia Tako-Tsubo: solo una malattia della donna?"</a></b> <i>di R. Manfredini</i></li>
  <li><b><a href="download/relazioni/388/Montanari.pdf">"Laboratorio, appropriatezza e genere"</a></b> <i>di E. Montanari</i></li>
  <li><b><a href="download/relazioni/388/Sassone.pdf">"Morte improvvisa: differenze o disparità di genere"</a></b> <i>di B. Sassone</i></li>
  <li><b><a href="download/relazioni/388/Signani.pdf">"Un caso di andragogia: La depressione dimenticata degli uomini"</a></b> <i>di F. Signani</i></li>
  <li><b><a href="download/relazioni/388/Storari.pdf">"Aspetti di genere in nefrologia"</a></b> <i>di A. Storari</i></li>
  <li><b><a href="download/relazioni/388/Zoli.pdf">"DM.I.C.I. Esistono differenze di Genere?"</a></b> <i>di G. Zoli</i></li>
  <li><b><a href="download/relazioni/388/Zucchi.pdf">"‘Gufo’ o ‘Allodola’: cronotipo e implicazioni di genere"</a></b> <i>di B. Zucchi</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;



  case '383':
?>
<h2>La geriatria in Emilia Romagna 2016</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/383/Bonati.pdf">"La gestione del paziente anziano comorbido: rete delle cronicità o presa in carico globale nella rete dei servizi?"</a></b> <i>di P. A. Bonati</i></li>
  <li><b><a href="download/relazioni/383/Fabbo.pdf">"Reablement nella continuità assistenziale e nelle dimissioni protette"</a></b> <i>di A. Fabbo</i></li>
  <li><b><a href="download/relazioni/383/Ferrari.pdf">"Il Paziente Anziano in Ospedale : I Vantaggi di un approccio dedicato alla luce delle evidenze disponibili"</a></b> <i>di A. Ferrari</i></li>
  <li><b><a href="download/relazioni/383/Martini.pdf">"Il delirium post operatorio nell'anziano"</a></b> <i>di E. Martini</i></li>
  <li><b><a href="download/relazioni/383/Romagnoni.pdf">"Il problema dei ricoveri da C.R.A."</a></b> <i>di F. Romagnoni</i></li>
  <li><b><a href="download/relazioni/383/Vanelli.pdf">"MoCA: studio normativo italiano e applicazione clinica nella diagnosi di MCI"</a></b> <i>di M. Vanelli Coralli</i></li>
  <li><b><a href="download/relazioni/383/Volpato.pdf">"Diagnosi e Trattamento della Sarcopenia nell’Anziano"</a></b> <i>di S. Volpato</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
  case '382':
?>
<h2>Diabete gestazionale, ipotiroidismo ed anemia in gravidanza</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/382/Bigoni.pdf">"Emoglobinopatie: screening e gestione clinica"</a></b> <i>di S. Bigoni</i></li>
  <li><b><a href="download/relazioni/382/Capucci.pdf">"Gravida obesa: quali rischi materni e fetali?"</a></b> <i>di R. Capucci</i></li>
  <li><b><a href="download/relazioni/382/Costantino.pdf">"Ruolo degli integratori nel controllo delle alterazioni del metabolismo glucidico in gravidanza"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/382/Costantino_1.pdf">"Fisiologia della tiroide"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/382/Furicchia.pdf">"Emorragia postpartum: gestione multidisciplinare"</a></b> <i>di G. Furicchia</i></li>
  <li><b><a href="download/relazioni/382/Graziani.pdf">"Il diabete pregestazionale"</a></b> <i>di R. Graziani</i></li>
  <li><b><a href="download/relazioni/382/Greco.pdf">"Prevalence of congenital abnormalities among offsprings of pregestational diabetic mothers"</a></b> <i>di P. Greco</i></li>
  <li><b><a href="download/relazioni/382/Gregoratti.pdf">"La dieta nel diabete pregravidico e gestazionale"</a></b> <i>di G. Gregoratti</i></li>
  <li><b><a href="download/relazioni/382/Guaraldi.pdf">"Il diabete gestazionale: management ostetrico"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/382/Guaraldi_1.pdf">"Le anemie in gravidanza: meglio prevenirle"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/382/Manzato.pdf">"Disturbo da alimentazione incontrollata e diabete gestazionale"</a></b> <i>di E. Manzato</i></li>
  <li><b><a href="download/relazioni/382/Mello.pdf">"Lo screening del diabete gestazionale: dall’HAPO Study alle nuove linee guida del 2011. Il punto della situazione"</a></b> <i>di G. Mello, S. Ottanelli</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;

  case '381':
?>
<h2>Società Italiana di Flebolinfologia: Congresso del Trentennale</h2>

<h4>Corso di aggiornamento: indicazioni, materiali e tecniche nella terapia compressiva e nel drenaggio linfatico manuale</h4>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/381/gio/Boemia.pdf">"Tecniche di D.L.M. e risultati clinici"</a></b> <i>di K. Boemia</i></li>
  <li><b><a href="download/relazioni/381/gio/Gazzabin.pdf">"Materiali per la compressione e loro criteri di scelta"</a></b> <i>di L. Gazzabin</i></li>
  <li><b><a href="download/relazioni/381/gio/Ligas.pdf">"Bendaggio multistrato: vantaggi e limiti"</a></b> <i>di B. M. Ligas</i></li>
  <li><b><a href="download/relazioni/381/gio/Moneta.pdf">"Terapia decongestionante: il management clinico"</a></b> <i>di G. Moneta</i></li>
  <li><b><a href="download/relazioni/381/gio/Oliva.pdf">"La pressoterapia: come e quando"</a></b> <i>di E. Oliva</i></li>
</ul>

<h4>Congresso Nazionale del Trentennale SIFL</h4>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/381/ven/Alonzo.pdf">"Trombosi della vena mesenterica superiore"</a></b> <i>di U. Alonzo</i></li>
  <li><b><a href="download/relazioni/381/ven/Aluigi.pdf">"Trombosi venosa degli arti superiori e intraddominali"</a></b> <i>di L. Aluigi</i></li>
  <li><b><a href="download/relazioni/381/ven/Annoni.pdf">"Lo studio eco color doppler e il mappaggio emodinamico in corso di malattia venosa cronica"</a></b> <i>di F. Annoni</i></li>
  <li><b><a href="download/relazioni/381/ven/Arpaia.pdf">"La Recidiva di TVP: Aspetti Clinici"</a></b> <i>di G. Arpaia</i></li>
  <li><b><a href="download/relazioni/381/ven/Benedetti.pdf">"Durata della terapia anticoagulante del tev"</a></b> <i>di I. Benedetti</i></li>
  <li><b><a href="download/relazioni/381/ven/Boschetti.pdf">"Trattamento del reflusso safenico mediante tecnica MOCA: indicazioni, risultati ed esperienza personale"</a></b> <i>di L. Boschetti</i></li>
  <li><b><a href="download/relazioni/381/ven/Caggiati.pdf">"Anatomia normale e varianti della SFJ e della SPJ: ricadute pratiche cliniche"</a></b> <i>di A. Caggiati</i></li>
  <li><b><a href="download/relazioni/381/ven/Campana.pdf">"Scleromousse degli assi safenici"</a></b> <i>di F. Campana</i></li>
  <li><b><a href="download/relazioni/381/ven/Camporese.pdf">"Il trombo residuo: mito o realtà?"</a></b> <i>di G. Camporese</i></li>
  <li><b><a href="download/relazioni/381/ven/Cestari.pdf">"Cosa evitare nel pdta del linfedema"</a></b> <i>di M. Cestari</i></li>
  <li><b><a href="download/relazioni/381/ven/Cimminiello.pdf">"Indicazioni all’ASA dopo la TAO"</a></b> <i>di C. Cimminiello</i></li>
  <li><b><a href="download/relazioni/381/ven/Corcos.pdf">"Le recidive della regione (Safeno)Poplitea: come diagnosticarle e quale terapia EBM"</a></b> <i>di L. Corcos</i></li>
  <li><b><a href="download/relazioni/381/ven/Coscia.pdf">"Fluid Dynamical modeling: Clinical-Practical Aspects"</a></b> <i>di V. Coscia</i></li>
  <li><b><a href="download/relazioni/381/ven/Danese.pdf">"Procedure flebologiche ambulatoriali o in day-surgery: aspetti medico legali"</a></b> <i>di M. Danese</i></li>
  <li><b><a href="download/relazioni/381/ven/DelGuercio.pdf">"Flebopatie e lea: luci ed ombre"</a></b> <i>di M. Del Guercio</i></li>
  <li><b><a href="download/relazioni/381/ven/DiPaola.pdf">"Il remboursement delle flebopatie nelle varie realtà italiane"</a></b> <i>di G. Di Paola</i></li>
  <li><b><a href="download/relazioni/381/ven/Ferrini.pdf">"Procedure endovascolari: i risultati della letteratura internazionale"</a></b> <i>di M. Ferrini</i></li>
  <li><b><a href="download/relazioni/381/ven/Fonti.pdf">"Tecniche endovascolari in DS: sempre possibili?"</a></b> <i>di M. Fonti</i></li>
  <li><b><a href="download/relazioni/381/ven/Franco.pdf">"La scelta della crossectomia: quando e come"</a></b> <i>di E. Franco</i></li>
  <li><b><a href="download/relazioni/381/ven/Galeotti.pdf">"La diagnostica per immagini"</a></b> <i>di R. Galeotti</i></li>
  <li><b><a href="download/relazioni/381/ven/Ghirarduzzi.pdf">"Le complicanze in corso di NAO e TAO"</a></b> <i>di A. Ghirarduzzi</i></li>
  <li><b><a href="download/relazioni/381/ven/Izzo.pdf">"Modificazioni cliniche e tessutali degli stadi avanzati dell’ivc"</a></b> <i>di M. Izzo</i></li>
  <li><b><a href="download/relazioni/381/ven/Lugli.pdf">"Ruolo della chirurgia open ed endovascolare"</a></b> <i>di M. Lugli</i></li>
  <li><b><a href="download/relazioni/381/ven/Mollo.pdf">Trombosi venosa degli arti superiori e intraddominali"</a></b> <i>di P. E. Mollo</i></li>
  <li><b><a href="download/relazioni/381/ven/Monaco.pdf">"L'infiammazione al cuore della MVC: aspetti clinici e ruolo della strategia terapeutica"</a></b> <i>di S. Moncao</i></li>
  <li><b><a href="download/relazioni/381/ven/Pacelli.pdf">"Trattamento scleroterapico delle recidive di cross"</a></b> <i>di W. Pacelli</i></li>
  <li><b><a href="download/relazioni/381/ven/Pola.pdf">"Genetica e Malattia Venosa Cronica"</a></b> <i>di R. Pola</i></li>
  <li><b><a href="download/relazioni/381/ven/Polichetti.pdf">"Criteri di scelta nei percorsi del paziente flebopatico"</a></b> <i>di R. Polichetti</i></li>
  <li><b><a href="download/relazioni/381/ven/Ricci.pdf">"Terapia medica, fisica riabilitativa e chirurgica: come e quando integrarle"</a></b> <i>di M. Ricci</i></li>
  <li><b><a href="download/relazioni/381/ven/Rocca.pdf">"Ruolo della fibrinolisi nella TVP prossimale"</a></b> <i>di T. Rocca</i></li>
  <li><b><a href="download/relazioni/381/ven/Serantoni.pdf">"Inquadramento fisiopatologico e aspetti clinici dell’ulcera venosa"</a></b> <i>di S. Serantoni</i></li>
  <li><b><a href="download/relazioni/381/ven/Sozio.pdf">"Standard per l’accreditamento dei centri flebologici"</a></b> <i>di G. Sozio</i></li>
  <li><b><a href="download/relazioni/381/ven/Traina.pdf">"Sindrome di Paget-Schroetter"</a></b> <i>di L. Traina</i></li>
  <li><b><a href="download/relazioni/381/ven/Zamboni.pdf">"La chirurgia della Sdr Post-Trombotica. Il contributo italiano ad una sfida"</a></b> <i>di P. Zamboni</i></li>
</ul>

<h4>Corso di aggiornamento: la diagnostica clinica non invasiva nelle flebopatie</h4>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/381/sab/Ermini.pdf">"La Cartografia"</a></b> <i>di S. Ermini</i></li>
  <li><b><a href="download/relazioni/381/sab/Izzo.pdf">"La diagnostica tessutale in corso di i.v.c."</a></b> <i>di M. Izzo</i></li>
  <li><b><a href="download/relazioni/381/sab/Medini.pdf">"Prove semeiologiche in corso di esame non invasivo per lo studio del sistema venoso degli arti inferiori"</a></b> <i>di C. Medini</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;

  case '380':
?>
<h2>Imaging gastro-intestinale: l’adulto e il bambino</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/380/Aliberti.pdf">"Emorragie del tubo digerente: Imaging e Radiologia Interventistica"</a></b> <i>di C. Aliberti</i></li>
  <li><b><a href="download/relazioni/380/Gagliano.pdf">"Patologia neoplastica dell’intestino tenue"</a></b> <i>di M. Gagliano, S. Sala</i></li>
  <li><b><a href="download/relazioni/380/Malaventura.pdf">"Patologia del tubo digerente in età pediatrica: inquadramento clinico"</a></b> <i>di C. Malaventura</i></li>
  <li><b><a href="download/relazioni/380/Miele.pdf">"Traumi intestinali e mesenterici"</a></b> <i>di V. Miele</i></li>
  <li><b><a href="download/relazioni/380/Monteduro.pdf">"Le Ostruzioni meccaniche del tubo Digerente"</a></b> <i>di F. Monteduro</i></li>
  <li><b><a href="download/relazioni/380/Rizzati.pdf">"Patologia infiammatoria acuta"</a></b> <i>di R. Rizzati, S. Leprotti</i></li>
  <li><b><a href="download/relazioni/380/Tilli.pdf">"Patologia ischemica ileo-colica"</a></b> <i>di M. Tilli e M. T. Cannizzaro</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
  case '378':
?>
<h2>Nuove acquisizioni in tema di riabilitazione del pavimento pelvico e diagnosi prenatale</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/378/Baldi.pdf">"Lo screening con ricerca del DNA fetale su sangue materno"</a></b> <i>di M. Baldi</i></li>
  <li><b><a href="download/relazioni/378/Costantino.pdf">"I Folati: quello che le gravide sanno e quello che dovrebbero sapere"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/378/Costantino_1.pdf">"Il danno perinale da parto e post isterectomia"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/378/Costantino_2.pdf">"Il test combinato, ancora attuale?"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/378/Guaraldi.pdf">"Classificazione e diagnosi delle disfunzioni perinali"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/378/Guaraldi_2.pdf">"L'integrazione come supporto nella patologia del pavimento pelvico"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/378/Tess.pdf">"Valutazione uro-ginecologica ed esame obiettivo: la valutazione funzione del pavimento pelvico"</a></b> <i>di M. Tess</i></li>
  <li><b><a href="download/relazioni/378/Toselli.pdf">"Pavimento pelvico: prevenzione, cura e riabilitazione"</a></b> <i>di S. Toselli</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
  case '376':
  ?>
  <h2>Workshop “La gestione multidisciplinare della complessità delle infezioni endoaddominali: esperienze a confronto”</h2>
  <ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/376/Antonelli_Libanore.pdf">"Il contributo della Farmacologia clinica"</a></b> <i>di T. Antonelli, M. Libanore</i></li>
    <li><b><a href="download/relazioni/376/Catena.pdf">"WSES Guidelines e Farmacoeconomia in chirurgia addominale"</a></b> <i>di F. Catena</i></li>
    <li><b><a href="download/relazioni/376/Galeotti.pdf">"Imaging delle infezioni intraddominali: approccio interventistico"</a></b> <i>di R. Galeotti</i></li>
    <li><b><a href="download/relazioni/376/Giuri.pdf">"Terapia empirica e raccomandazioni WSES nelle realtà locali: aspetti organizzativi, chirurgici e infettivologici"</a></b> <i>di G. Giuri</i></li>
    <li><b><a href="download/relazioni/376/Govoni.pdf">"Imola: un’esperienza di gestione integrata"</a></b> <i>di Govoni</i></li>
    <li><b><a href="download/relazioni/376/Malagodi.pdf">"Antibiotico profilassi perioperatoria"</a></b> <i>di M. Malagodi</i></li>
    <li><b><a href="download/relazioni/376/Occhionorelli.pdf">"Gestione Multidisciplinare della Complessità delle Infezioni Endoaddominali"</a></b> <i>di S. Occhionorelli</i></li>
    <li><b><a href="download/relazioni/376/Righini.pdf">"Sepsi addominale: approccio intensivistico"</a></b> <i>di E. Righini, M. Malagodi</i></li>
    <li><b><a href="download/relazioni/376/Rossi.pdf">"Patogeni emergenti: impatto epidemiologico"</a></b> <i>di M. R. Rossi</i></li>
    <li><b><a href="download/relazioni/376/Sarti.pdf">"ERCP and KPC and others CPE"</a></b> <i>di M. Sarti</i></li>
    <li><b><a href="download/relazioni/376/Viale.pdf">"Infezioni in chirurgia: nuove acquisizioni per nuove indicazioni"</a></b> <i>di P. Viale</i></li>
    <li><b><a href="download/relazioni/376/Volta.pdf">"Sepsi addominale: approccio intensivistico"</a></b> <i>di C. A. Volta</i></li>
  </ul>
  <h2>Note</h2>
  Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
  Per scaricare una relazione cliccare sul titolo della stessa.<br/>
  Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
  <?php
    break;
  case '374':
?>
<h2>Sarcopenia e Malnutrizione. Problemi diagnostici e terapeutici nell'anziano </h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/374/Zamboni_Rossi.pdf">"Malnutrizione e Sarcopenia"</a></b> <i>di M. Zamboni, A. Rossi</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
  case '363':
?>
<h2>Appropriatezza degli esami di laboratorio</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/363/Bettoli.pdf">"La corretta esecuzione di indagini di laboratorio: in Dermatologia e Allergologia"</a></b> <i>di V. Bettoli</i></li>
  <li><b><a href="download/relazioni/363/Camerotto.pdf">"Ermete, la conoscenza nel momento esatto della prescrizione"</a></b> <i>di A. Camerotto</i></li>
  <li><b><a href="download/relazioni/363/Fiumana.pdf">"La corretta esecuzione delle indagini di laboratorio nel bambino"</a></b> <i>di E. Fiumana</i></li>
  <li><b><a href="download/relazioni/363/Gallerani.pdf">"Attivazione in Az. Ospedaliero Universitaria"</a></b> <i>di G. Gallerani</i></li>
  <li><b><a href="download/relazioni/363/Guerra.pdf">"Problematiche delle interfacce con i diversi data entry"</a></b> <i>di G. Guerra</i></li>
  <li><b><a href="download/relazioni/363/Montanari.pdf">"Base per l’appropriatezza"</a></b> <i>di E. Montanari</i></li>
  <li><b><a href="download/relazioni/363/Pelizzola.pdf">"La corretta ripetizione degli esami di Laboratorio"</a></b> <i>di D. Pelizzola</i></li>
  <li><b><a href="download/relazioni/363/Toniutti.pdf">"La corretta ripetizione degli esami di Laboratorio: problematiche tecnico informatiche"</a></b> <i>di A. Toniutti</i></li>
  <li><b><a href="download/relazioni/363/Trenti.pdf">"Esperienza del Dipartimento di Medicina di Laboratorio di Modena nel governo dell'appropriatezza diagnostica degli esami di laboratorio"</a></b> <i>di T. Trenti</i></li>
  <li><b><a href="download/relazioni/363/Vagnoni.pdf">"I risvolti economici"</a></b> <i>di E. Vagnoni</i></li>
  <li><b><a href="download/relazioni/363/Zamboni.pdf">"Appropriatezza degli esami di laboratorio"</a></b> <i>di A. Zamboni</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
  case '362':
?>
<h2>Le infezioni del basso tratto urogenitale femminile</h2>
<ul class="elenco_relazioni">
  <li><b><a href="download/relazioni/362/Costantino.pdf">"Lattoferrina come agente terapeutico delle infenzioni urogenitali"</a></b> <i>di D. Costantino</i></li>
  <li><b><a href="download/relazioni/362/Dante.pdf">"L'ecosistema vaginale: dalla pubertà alla menopausa"</a></b> <i>di G. Dante</i></li>
  <li><b><a href="download/relazioni/362/Fanti.pdf">"Terapie convenzionali delle vulvovaginiti"</a></b> <i>di S. Fanti</i></li>
  <li><b><a href="download/relazioni/362/Guaraldi.pdf">"Fermenti lattici tindalizzati: nuove opportunità terapeutiche nel trattamento delle vaginiti"</a></b> <i>di C. Guaraldi</i></li>
  <li><b><a href="download/relazioni/362/Murina.pdf">"Contraccezione ormonale, ecosistema vaginale ed esposizione alle infezioni del basso tratto uro-genitale femminile"</a></b> <i>di F. Murina</i></li>
  <li><b><a href="download/relazioni/362/Sileo.pdf">"Infezioni delle basse vie urinarie in gravidanza: problematiche materne ed embrio-fetali"</a></b> <i>di F. Sileo</i></li>
  <li><b><a href="download/relazioni/362/Tumietto.pdf">"Urinocoltura, antibiogramma, antibioticoterapia, antibiotico resistenza: riflessioni"</a></b> <i>di F. Tumietto</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
  break;
    case '359':
?>
<h2>I neutroceutici al femminile</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/359/Bonaccorsi.pdf">"La Cimicifuga Racemosa nel trattamento della sindrome climaterica"</a></b> <i>di G. Bonaccorsi</i></li>
    <li><b><a href="download/relazioni/359/Costantino.pdf">"Nutraceutica e poliabortività"</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/359/Fila.pdf">"Sindrome genito-urinaria in postmenopausa: approccio terapeutico non ormonale."</a></b> <i>di E. Fila</i></li>
    <li><b><a href="download/relazioni/359/Guaraldi.pdf">"Fitoterapici e nutraceutici: definizione e claims di salute"</a></b> <i>di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/359/Marci.pdf">"Alimentazione, nutraceutica ed infertilità femminile"</a></b> <i>di R. Marci e C. Borghi</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;
    case '357':
?>
<h2>Casi clinici difficili in Patologia Infettiva</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/357/Bassi.pdf">"Meningite da pneumococco resistente alla penicillina"</a></b> <i>di P. Bassi</i></li>
    <li><b><a href="download/relazioni/357/Biagetti.pdf">"Sepsi da KPC a partenza dalle vie biliari: case report e spunti gestionali"</a></b> <i>di C. Biagetti</i></li>
    <li><b><a href="download/relazioni/357/Calzetti.pdf">"Empiema della colecisti da enterobatteri ESBL positivi"</a></b> <i>di C. Calzetti</i></li>
    <li><b><a href="download/relazioni/357/Cancellieri.pdf">"Caso clinico II: Infezione di tasca di Pace – maker da MRSA con MIC >1 per vancomicina"</a></b> <i>di C. Cancellieri</i></li>
    <li><b><a href="download/relazioni/357/Codeluppi.pdf">"Sepsi Da VRE in trapianto"</a></b> <i>di M. Codeluppi</i></li>
    <li><b><a href="download/relazioni/357/Cultrera.pdf">"Casi difficili in patologia infettiva su un cado di infenzione di device cardiaco"</a></b> <i>di R. Cultrera</i></li>
    <li><b><a href="download/relazioni/357/Giuri.pdf">"Infezione di tasca di Pace – maker da MRSA con MIC >1 per vancomicina"</a></b> <i>di G. Giuri</i></li>
    <li><b><a href="download/relazioni/357/Libanore.pdf">"La resistenza agli antibiotici : un problema globale"</a></b> <i>di M. Libanore</i></li>
    <li><b><a href="download/relazioni/357/Pantaleoni.pdf">"Caso clinico III: Meningite da Streptococco pneumoniae PR"</a></b> <i>di M. Pantaleoni</i></li>
    <li><b><a href="download/relazioni/357/Sambri.pdf">"Appropriatezza e tempestività nella diagnostica microbiologica"</a></b> <i>di V. Sambri</i></li>
    <li><b><a href="download/relazioni/357/Sarti.pdf">"Appropriatezza e tempestività nella diagnostica microbiologica"</a></b> <i>di M. Sarti</i></li>
    <li><b><a href="download/relazioni/357/Sighinolfi.pdf">"Sepsi addominale da KPC in paziente con AIDS"</a></b> <i>di L. Sighinolfi</i></li>
    <li><b><a href="download/relazioni/357/Viale.pdf">"Linee guida: valori e limiti"</a></b> <i>di P. Viale</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;
	case '355':
?>
<h2>La "terapia multifattoriale" nel deterioramento cognitivo dell'anziano: quali evidenze?</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/355/Amenta.pdf">"Colina alfoscerato nella terapia del deterioramento cognitivo dell'anziano."</a></b> <i>di F. Amenta</i></li>
    <li><b><a href="download/relazioni/355/Bonetti.pdf">"La terapia multifattoriale nel deterioramento cognitivo dell'anziano: quali evidenze?"</a></b> <i>di F. Bonetti</i></li>
    <li><b><a href="download/relazioni/355/Cervellati.pdf">"Stress ossidativo e deterioramento cognitivo dell'anziano."</a></b> <i>di C. Cervellati</i></li>
    <li><b><a href="download/relazioni/355/De_Carolis.pdf">"L'acetilcarnitina e trattamento della demenza senile"</a></b> <i>di S. De Carolis</i></li>
    <li><b><a href="download/relazioni/355/De_Vreese.pdf">"Gingko Biloba: tra mito e realtà."</a></b> <i>di L. De Vreese</i></li>
    <li><b><a href="download/relazioni/355/Lucchetti.pdf">"Alimenti (AFSM) e neuroinfiammazione: la palmitoiletanolamide."</a></b> <i>di L. Lucchetti</i></li>
    <li><b><a href="download/relazioni/355/Menozzi.pdf">"Omotaurina (tramiprosato) nella malattia di Alzheimer."</a></b> <i>di L. Menozzi</i></li>
    <li><b><a href="download/relazioni/355/Perotta.pdf">"Dieta e Medical Food nel declino cognitivo lieve."</a></b> <i>di D. Perotta</i></li>
    <li><b><a href="download/relazioni/355/Trabucchi.pdf">"Il deterioramento cognitivo e la pluralità delle sue determinanti."</a></b> <i>di M. Trabucchi</i></li>
    <li><b><a href="download/relazioni/355/Zuliani.pdf">"Alzheimer's disease drug-development pipeline: few candidates, frequent failures."</a></b> <i>di G. Zuliani</i></li>
    <li><b><a href="download/relazioni/355/Zurlo.pdf">"Vitamine Liposolubili e Declino Cognitivo: quale ruolo per Vitamina E e Vitamina D?"</a></b> <i>di A. Zurlo</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;
	case '352':
?>
<h2>Festival dell'apparato digerente</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/352/Caselli.ppt">"Gut microflora have to be modernly considered a “micromial organ” placed within a host organ."</a></b> <i>di M. Caselli</i></li>
    <li><b><a href="download/relazioni/352/Mazzella.pptx">"Epidemiologia della malattie di fegato"</a></b> <i>di G. Mazzella</i></li>
    <li><b><a href="download/relazioni/352/Stockbrugger.ppt">"Irritable Bowel Syndrome. A functional or an organic condition?"</a></b> <i>di R. Stockbrugger</i></li>
    <li><b><a href="download/relazioni/352/Zagari.ppt">"Epidemiologia della malattia da reflusso gastro-esofageo: una prospettiva globale"</a></b> <i>di R. Zagari</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Microsoft&reg; Power Point&reg; 2010 (.pptx)
<?php
    break;
    case '350':
?>
<h2>Valutazione dei medici ospedalieri e sistemi premianti in una sanità che cambia: esperienze e progetti</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/350/Caselli.pdf">"Diagnosis non invasive tests"</a></b> <i>di M. Caselli</i></li>
    <li><b><a href="download/relazioni/350/Figura.pdf">"Un nuovo metodo diagnostico per la rilevazione di helicobacter pylori"</a></b> <i>di N. Figura</i></li>
    <li><b><a href="download/relazioni/350/Figura2.pdf">"Nuove composizioni per il trattamento dell’infezione da helicobacter pylori"</a></b> <i>di N. Figura</i></li>
    <li><b><a href="download/relazioni/350/LaCorte.pdf">"Consensus H pylori management"</a></b> <i>di R. La Corte</i></li>
    <li><b><a href="download/relazioni/350/Zagari.pdf">"Diagnosi"</a></b> <i>di R. M. Zagari</i></li>
    <li><b><a href="download/relazioni/350/Zagari1.pdf">"Preliminary speech"</a></b> <i>di R. M. Zagari</i></li>
    <li><b><a href="download/relazioni/350/Zagari2.pdf">"Questioni aperte"</a></b> <i>di R. M. Zagari</i></li>
    <li><b><a href="download/relazioni/350/Zagari3.pdf">"Problema delle resistenze batteriche"</a></b> <i>di R. M. Zagari</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;
	case '347':
?>
<h2>Valutazione dei medici ospedalieri e sistemi premianti in una sanità che cambia: esperienze e progetti</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/347/Cavazza.pdf">"Il significato della valutazione individuale nelle Aziende Sanitarie"</a></b> <i>di M. Cavazza</i></li>
    <li><b><a href="download/relazioni/347/Laus.pdf">"La valutazione individuale del Medico Ospedaliero: l’esperienza del Policlinico S. Orsola-Malpighi"</a></b> <i>di M. Laus</i></li>
    <li><b><a href="download/relazioni/347/Pizzoferrato.pdf">"Il quadro legislativo e contrattuale"</a></b> <i>di A. Pizzoferrato</i></li>
    <li><b><a href="download/relazioni/347/Rinaldi.pdf">"Gli incarichi dirigenziali: dalla amministrazione del Personale alla gestione strategica delle Risorse Umane"</a></b> <i>di G. Rinaldi</i></li>
    <li><b><a href="download/relazioni/347/Saltari.pdf">"Sistema di valutazione del personale dirigente ASL Ferrara"</a></b> <i>di P. Saltari</i></li>
    <li><b><a href="download/relazioni/347/Sarchielli.pdf">"Il sistema premiante: effetti personali e collettivi della valorizzazione differenziale in base al merito professionale"</a></b> <i>di G. Sarchielli</i></li>
    <li><b><a href="download/relazioni/347/Zoppellari.pdf">"La valutazione individuale dei medici ospedalieri"</a></b> <i>di R. Zoppellari</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '329':
?>
<h2>Ostetricia e Ginecologia</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/329/Costantino2.pdf">"Test combinato: è ancora attuale?"</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/329/Costantino.pdf">"La nuova dimensione della contraccezione"</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/329/Giugliano.pdf">"Approccio contraccettivo nell’iperandrogenismo"</a></b> <i>di E. Giugliano</i></li>
    <li><b><a href="download/relazioni/329/Graziano.pdf">"Integratori in gravidanza: Lusso o Necessità?"</a></b> <i>di A. Graziano</i></li>
    <li><b><a href="download/relazioni/329/Guaraldi.pdf">"Quando l'allattamento diventa contraccezione: allattamento e contraccezione efficace"</a></b> <i>di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/329/LoMonte.pdf">"Screening per GBS: utilità e controversie"</a></b> <i>di G. Lo Monte</i></li>
    <li><b><a href="download/relazioni/329/Moreno.pdf">"Ruolo dell’ostetrica nella gestione della gravidanza fisiologica"</a></b> <i>di A. Moreno</i></li>
    <li><b><a href="download/relazioni/329/Pagliarusco.pdf">"Il danno perineale da parto: definizione e prevenzione"</a></b> <i>di G. Pagliarusco</i></li>
    <li><b><a href="download/relazioni/329/Segato.pdf">"Il trattamento delle infezioni da candida in ostetricia e ginecologia nella pratica clinica"</a></b> <i>di M. Segato</i></li>
    <li><b><a href="download/relazioni/329/Tess.pdf">"Rieducazione e riabilitazione del pavimento pelvico: dalla valutazione del danno al primo approccio riabilitativo"</a></b> <i>di M. Tess</i></li>
    <li><b><a href="download/relazioni/329/Zanella.pdf">"Contraccezione d'emergenza e responsabilità del medico"</a></b> <i>di C. Zanella</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

	    case '328':
?>
<h2>Ostetricia e Ginecologia</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/328/Bandiera.pdf">"Ruolo dell’ostetrica nella gestione della gravidanza fisiologica"</a></b> <i>di E. Bandiera</i></li>
    <li><b><a href="download/relazioni/328/Bianchini.pdf">"Il danno perineale da parto: definizione e prevenzione"</a></b> <i>di G. Bianchini</i></li>
    <li><b><a href="download/relazioni/328/Costantino.pdf">"Test combinato: è ancora attuale?"</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/328/Costantino2.pdf">"La nuova dimensione della contraccezione"</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/328/Giugliano.pdf">"Approccio contraccettivo nell’iperandrogenismo"</a></b> <i>di E. Giugliano</i></li>
    <li><b><a href="download/relazioni/328/Graziano.pdf">"Integratori in gravidanza: Lusso o Necessità?"</a></b> <i>di A. Graziano</i></li>
    <li><b><a href="download/relazioni/328/Guaraldi.pdf">"Quando l'allattamento diventa contraccezione: allattamento e contraccezione efficace"</a></b> <i>di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/328/Mossuto.pdf">"Eccessivo incrememnto ponderale in gravidanza: rischi materno-fetali"</a></b> <i>di E. Mossuto</i></li>
    <li><b><a href="download/relazioni/328/Toselli.pdf">"Pavimento Pelvico: Prevenzione Cura e Riabilitazione"</a></b> <i>di S. Toselli</i></li>
    <li><b><a href="download/relazioni/328/Vesce.pdf">"Diagnosi prenatale: stato dell'arte"</a></b> <i>di F. Vesce</i></li>
    <li><b><a href="download/relazioni/328/Zanella.pdf">"Contraccezione d'emergenza e responsabilità del medico"</a></b> <i>di C. Zanella</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '327':
?>
<h2>Di CaFE' in CaFE' - Le esperienze ferrraresi a confronto con la realtà nazionale</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/327/Carafelli.pdf">"Un caffè dalla Regione: politiche a sostegno delle attività a bassa soglia"</a></b> <i>di A. Carafelli</i></li>
    <li><b><a href="download/relazioni/327/Cristofori.pdf">"L’esperienza di Portomaggiore: Ri-troviamoci al cafè"</a></b> <i>di R. Cristofori, I. Pedriali, P. Veronesi</i></li>
    <li><b><a href="download/relazioni/327/Tola.pdf">"Il progetto locale: l’accordo di programma e le attività correlate"</a></b> <i>di M.R. Tola</i></li>
    <li><b><a href="download/relazioni/327/Tulipani.pdf">"L’esperienza di Cento: la fatica di ripartire"</a></b> <i>di C. Tulipani</i></li>
    <li><b><a href="download/relazioni/327/Veronesi.pdf">"L’evoluzione di una storia"</a></b> <i>di P. Veronesi</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '324':
?>
<h2>1° Corso Regionale Sie di Aggiornamento in Endocrinologia Clinica - Emilia Romagna</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/324/Ambrosio.pdf">"PHPT"</a></b> <i>di M.R. Ambrosi</i></li>
    <li><b><a href="download/relazioni/324/Balsamo.pdf">"Deficit di 21 idrossilasi"</a></b> <i>di A. Balsamo, A. Gambineri</i></li>
    <li><b><a href="download/relazioni/324/Bondanelli.pdf">"Novità Farmacologiche in Endocrinologia"</a></b> <i>di M. Bondanelli</i></li>
    <li><b><a href="download/relazioni/324/Buci.pdf">"Nuove formulazioni di levo-tiroxina"</a></b> <i>di L. Buci</i></li>
    <li><b><a href="download/relazioni/324/Corona.pdf">"Diagnosi e terapia del deficit di GH in età pediatrica"</a></b> <i>di G. Corona e D. Ribichini</i></li>
    <li><b><a href="download/relazioni/324/Faustini.pdf">"Tolvaptan"</a></b> <i>di M. Faustini Fustini</i></li>
    <li><b><a href="download/relazioni/324/Madeo.pdf">"Denosumab"</a></b> <i>di B. Madeo</i></li>
    <li><b><a href="download/relazioni/324/Minoia.pdf">"Pasireotide"</a></b> <i>di M. Minoia</i></li>
    <li><b><a href="download/relazioni/324/Nizzoli.pdf">"Quali farmaci dopo la metformina ?"</a></b> <i>di M. Nizzoli</i></li>
    <li><b><a href="download/relazioni/324/Pellicano.pdf">"La scelta del farmaco: valutazione costo beneficio"</a></b> <i>di F. Pellicano</i></li>
    <li><b><a href="download/relazioni/324/Tomasi.pdf">"Target glicemici personalizzati?"</a></b> <i>di F. Tomasi</i></li>
    <li><b><a href="download/relazioni/324/Trimarchi.pdf">"Tiroide e gravidanza (nutrizione iodica)"</a></b> <i>di F. Trimarchi</i></li>
    <li><b><a href="download/relazioni/324/Zatelli.pdf">"Nodulo tiroideo in paziente gravida"</a></b> <i>di M.C. Zatelli</i></li>
    <li><b><a href="download/relazioni/324/Zavaroni.pdf">"La riduzione del rischio cardiovascolare principale obiettivo della terapia"</a></b> <i>di I. Zavaroni</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

	case '323':
?>
<h2>Il Diabete: dall'adolescenza alla menopausa</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/323/Feo.pdf">"Chirurgia per i Polipi del Colon"</a></b> <i>di C. Feo</i></li>
    <li><b><a href="download/relazioni/323/Ravenna.pdf">"Malattia da reflusso gastroesofageo"</a></b> <i>di F. Ravenna</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
	break;

    case '315':
?>
<h2>Il Diabete: dall'adolescenza alla menopausa</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/315/Costantino.pdf">"Nuovi approcci terapeutici per il controllo delle alterazioni del metabolismo glucidico"</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/315/D_Ambrosio.pdf">"Counseling psicologico per la gestione del peso e dell'obesità"</a></b> <i>di F. D'Ambrosio</i></li>
    <li><b><a href="download/relazioni/315/D_Ambrosio2.pdf">"L'approccio psicologico nella paziente diabetica"</a></b> <i>di F. D'Ambrosio</i></li>
    <li><b><a href="download/relazioni/315/Guaraldi.pdf">"Diabete gestazionale ed infezioni del tratto genito-urinario: nuove opportunità terapeutiche"</a></b> <i>di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/315/Lanzoni.pdf">"L'insulino-resistenza nelle diverse fasi della vita della donna"</a></b> <i>di C. Lanzoni</i></li>
    <li><b><a href="download/relazioni/315/Marci.pdf">"Modalità e timing del parto"</a></b> <i>di R. Marci</i></li>
    <li><b><a href="download/relazioni/315/Nedea.pdf">"Alimentazione, gravidanza e migrazione"</a></b> <i>di D. Nedea</i></li>
    <li><b><a href="download/relazioni/315/Ultori.pdf">"Il ruolo dell’ecografia nella sorveglianza fetale"</a></b> <i>di E. Ultori</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '298':
?>
<h2>Patologie endocrine e chirurgia: innovazioni tecnologiche e trattamenti mini-invasivi.</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/298/Ambrosio.pdf">"Inquadramento diagnostico dei tumori neuroendocrini del pancreas"</a></b> <i>di M. R. Ambrosio </i></li>
    <li><b><a href="download/relazioni/298/Ansaldo.pdf">"Stato dell’arte nel trattamento chirurgico del carcinoma differenziato della tiroide"</a></b> <i>di G. Ansaldo</i></li>
    <li><b><a href="download/relazioni/298/Bondanelli.pdf">"Inquadramento Clinico dell’Incidentaloma Surrenalico"</a></b> <i>di M. Bondanelli</i></li>
    <li><b><a href="download/relazioni/298/Cavazzini.pdf">"Relazione"</a></b> <i>di L. Cavazzini</i></li>
    <li><b><a href="download/relazioni/298/Feo.pdf">"Surrenectomia Laparoscopica"</a></b> <i>di C. Feo</i></li>
    <li><b><a href="download/relazioni/298/Franceschetti.pdf">"Deficit di vitamina d e iperaparatiroidismo secondario"</a></b> <i>di P. Franceschetti</i></li>
    <li><b><a href="download/relazioni/298/Gianotti.pdf">"Applicazioni cliniche dei calciomimetici"</a></b> <i>di L. Gianotti</i></li>
    <li><b><a href="download/relazioni/298/Guerra.pdf">"Dosaggio intraoperatorio del PTH"</a></b> <i>di G. Guerra</i></li>
    <li><b><a href="download/relazioni/298/Trasforini.pdf">"Nodulo tiroideo ed ecografia"</a></b> <i>di G. Trasforini</i></li>
    <li><b><a href="download/relazioni/298/Verdecchia.pdf">"AJCC/WHO/UICC 2010"</a></b> <i>di G. Verdecchia</i></li>
    <li><b><a href="download/relazioni/298/Zaccaroni.pdf">"Pianificazione dell’intervento chirurgico: indicazioni, localizzazione e tecniche"</a></b> <i>di A. Zaccaroni</i></li>
    <li><b><a href="download/relazioni/298/Zatelli.pdf">"Il percorso diagnostico del nodulo tiroideo: il ruolo dell’analisi molecolare"</a></b> <i>di M. C. Zatelli</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '297':
?>
<h2>Convegno regionale Simeu - Il Pronto Soccorso ed il ricovero appropriato</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/297/Bozzolani.pdf">"Malattia Tromboembolica - Luci e ombre"</a></b> <i>di G. Bozzolani</i></li>
    <li><b><a href="download/relazioni/297/Cavalli.pdf">"Il Problema del Ricovero - La Bed Capacity"</a></b> <i>di M. Cavalli</i></li>
    <li><b><a href="download/relazioni/297/Cesari.pdf">"Il reparto “polmone”, in quali mani? L’eperienza della Azienda USL di Bologna: la Medicina F"</a></b> <i>di C. Cesari</i></li>
    <li><b><a href="download/relazioni/297/Dall_Olmi.pdf">"Bed management strategie e buone pratiche"</a></b> <i>di E. Dall'Olmi</i></li>
    <li><b><a href="download/relazioni/297/De_Pietri.pdf">"Score prognostici nell’emorragia digestiva superiore non varicosa: percorso assistenziale in una rete ospedaliera provinciale"</a></b> <i>di S. De Pietri</i></li>
    <li><b><a href="download/relazioni/297/Farinatti.pdf">"Score polmoniti: casi clinici"</a></b> <i>di M. Farinatti</i></li>
    <li><b><a href="download/relazioni/297/Felletti.pdf">"La stratificazione del rischio nelle emorragie digestive del tratto superiore"</a></b> <i>di A. Felletti Spadazzi</i></li>
    <li><b><a href="download/relazioni/297/Guidetti_Giuffre.pdf">"La Sincope"</a></b> <i>di A. Guidetti, E. Giuffrè</i></li>
    <li><b><a href="download/relazioni/297/Iervese.pdf">"Il boarding e i suoi rischi"</a></b> <i>di T. Iervese</i></li>
    <li><b><a href="download/relazioni/297/Lenzi.pdf">"I nuovi anticoagulanti orali (NAO). Cosa cambia?"</a></b> <i>di T. Lenzi</i></li>
    <li><b><a href="download/relazioni/297/Magnacavallo.pdf">"Molti scores necessari per stratificare il rischio e definire il trattamento ottimale"</a></b> <i>di A. Magnacavallo</i></li>
    <li><b><a href="download/relazioni/297/Pinelli.pdf">"Utilità degli score: luci ed ombre. Le Polmoniti."</a></b> <i>di G. Pinelli</i></li>
    <li><b><a href="download/relazioni/297/Prati.pdf">"L'OBI e la medicina d'urgenza: come quantificare il carico di lavoro infermieristico"</a></b> <i>di K. Prati</i></li>
    <li><b><a href="download/relazioni/297/Rastelli.pdf">"Il ricovero in appoggio in reparti "incompetenti""</a></b> <i>di G. Rastelli</i></li>
    <li><b><a href="download/relazioni/297/Scappin_Mule.pdf">"Gli score nella valutazione del dolore toracico"</a></b> <i>di S. Scappin, P. Mulè</i></li>
    <li><b><a href="download/relazioni/297/Sighinolfi.pdf">"Transient Ischemic Attack"</a></b> <i>di D. Sighinolfi</i></li>
    <li><b><a href="download/relazioni/297/Squarzoni.pdf">"Il Ricovero in "letti aggiuntivi""</a></b> <i>di G. Squarzoni</i></li>
    <li><b><a href="download/relazioni/297/Tampieri.pdf">"Score nella Fibrillazione atriale"</a></b> <i>di A. Tampieri</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '296':
?>
<h2>Il trattamento multidisciplinare del cancro del retto</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/296/Coratti.ppt">"La TME robotica"</a></b> <i>di A. Coratti, M. di Marino</i></li>
    <li><b><a href="download/relazioni/296/De_Togni.ppt">"Epidemiologia e screening del tumore del retto "</a></b> <i>di A. De Togni</i></li>
    <li><b><a href="download/relazioni/296/Feo.pptx">"Escissione Totale del Mesoretto: Laparotomia vs Laparoscopia"</a></b> <i>di C. Feo</i></li>
    <li><b><a href="download/relazioni/296/Frassoldati.ppt">"Basi razionali della terapia perioperatoria"</a></b> <i>di A. Frassoldati</i></li>
    <li><b><a href="download/relazioni/296/Pezzoli.ppt">"I colostent: un ponte verso la chirurgia?"</a></b> <i>di A. Pezzoli</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Microsoft&reg; Power Point&reg; 2010 (.pptx)
<?php
    break;

    case '291':
?>
<h2>Le demenze nell'anziano: dalla diagnosi alla terapia</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/291/Cagnin.pdf">"Prospettive future nella terapia della demenza di Alzheimer"</a></b> <i>di A. Cagnin</i></li>
    <li><b><a href="download/relazioni/291/De_Ronchi.ppt">"Frontotemporal Dementia and Related Disorders: Organic Psychoses?"</a></b> <i>di D. De Ronchi, A. Rita</i></li>
    <li><b><a href="download/relazioni/291/Fabbo.ppt">"Il trattamento non farmacologico dei BPSD: quali evidenze?"</a></b> <i>di A. Fabbo</i></li>
    <li><b><a href="download/relazioni/291/Govoni.pdf">"Diagnosi della demenza di Alzheimer"</a></b> <i>di S. Govoni</i></li>
    <li><b><a href="download/relazioni/291/Mecocci.ppt">"La Demenza con corpi di Lewy: diagnosi e trattamento"</a></b> <i>di P. Mecocci</i></li>
    <li><b><a href="download/relazioni/291/Nichelli.ppt">"La demenza nel morbo di Parkinson"</a></b> <i>di P. Nichelli</i></li>
    <li><b><a href="download/relazioni/291/Rozzini.ppt">"Depressione e demenza oppure demenza e depressione?"</a></b> <i>di R. Rozzini</i></li>
    <li><b><a href="download/relazioni/291/Zanetti.ppt">"Stato d’arte della terapia della demenza di Alzheimer"</a></b> <i>di O. Zanetti</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

case '288':
?>
<h2>1° incontro regionale Radiologia Pediatrica in Emilia Romagna</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/288/Atti.pdf">"L’ecografia dell’anca neonatale"</a></b> <i>di G. Atti</i></li>
    <li><b><a href="download/relazioni/288/Atti_2.pdf">"Diagnosi della Displasia Evolutiva dell’Anca"</a></b> <i>di G. Atti</i></li>
    <li><b><a href="download/relazioni/288/Ciliberti.pdf">"Casi Clinici"</a></b> <i>di M. Ciliberti, V. Frisullo</i></li>
    <li><b><a href="download/relazioni/288/Di_Pancrazio.pdf">"Dall’embriogenesi all’imaging"</a></b> <i>di L. Di Pancrazio</i></li>
    <li><b><a href="download/relazioni/288/Lombardi.pdf">"Back to school: dall’Embriologia all’Imaging dell’Apparato Digerente"</a></b> <i>di A. A. Lombardi</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '282':
?>
<h2>Festival dell'apparato digerente</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/282/locandina.pdf">Locandina del convegno</b></li>
    <li><b><a href="download/relazioni/282/folder_esterno.pdf">Folder esterno</b></li>
    <li><b><a href="download/relazioni/282/folder_interno.pdf">Folder interno</b></li>
    <li><b><a href="download/relazioni/282/Ascanelli.ppt">"Sindrome da ostruita defecazione"</a></b> <i>di S. Ascanelli</i></li>
    <li><b><a href="download/relazioni/282/De_Togni.pdf">"Risultati del programma di screening dei tumori del colon retto a Ferrara"</a></b> <i>di A. De Togni</i></li>
    <li><b><a href="download/relazioni/282/Feo.ppt">"Il cancro del colon: aspetti chirurgici"</a></b> <i>di C. Feo</i></li>
    <li><b><a href="download/relazioni/282/Gasbarrini.ppt">"Terapia dell'esofagite da reflusso"</a></b> <i>di A. Gasbarrini</i></li>
    <li><b><a href="download/relazioni/282/Matarese.ppt">"Aspetti endoscopici"</a></b> <i>di V. G. Matarese</i></li>
    <li><b><a href="download/relazioni/282/Pansini.ppt">"Lesioni precancerose & tumori dell'esofago"</a></b> <i>di G. Pansini</i></li>
    <li><b><a href="download/relazioni/282/Pazzi.ppt">"Gli inibitori della secrezione gastrica: sempre utili, sempre innocui?"</a></b> <i>di P. Pazzi</i></li>
    <li><b><a href="download/relazioni/282/Zagari.ppt">"Eradicazione H.pylori, quando e come"</a></b> <i>di R. M. Zagari</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '278':
?>
<h2>La cefalea in età pediatrica</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/278/Arveda.pptx">"La cefalea e il dentista"</a></b> <i>di N. Arveda</i></li>
    <li><b><a href="download/relazioni/278/Ferronato.pptx">"Trattamento comportamentale: esperienza su volontari del gruppo di autoaiuto di Ferrara"</a></b> <i>di C. Ferronato</i></li>
    <li><b><a href="download/relazioni/278/Fiumana.ppt">"Impatto della cefalea sul sistema sanitario nazionale"</a></b> <i>di E. Fiumana </i></li>
    <li><b><a href="download/relazioni/278/Galli.pptx">"Le terapie non farmacologiche"</a></b> <i>di F. Galli</i></li>
    <li><b><a href="download/relazioni/278/Gaudio.ppt">"La disabilità e gli aspetti medico-legali"</a></b> <i>di R. M. Gaudio</i></li>
    <li><b><a href="download/relazioni/278/Merighi.ppt">"La cefalea in età pediatrica"</a></b> <i>di L. Merighi</i></li>
    <li><b><a href="download/relazioni/278/Tassorelli.ppt">"Fisiopatologia della cefalea emicranica nelle varie età della vita."</a></b> <i>di C. Tassorelli</i></li>
    <li><b><a href="download/relazioni/278/Terrazzino.pptx">"La Farmacogenomica"</a></b> <i>di S. Terrazzino</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Microsoft&reg; Power Point&reg; 2010 (.pptx)
<?php
    break;

    case '266':
?>
<h2>Contraccezione 2012</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/266/Bruni.ppt">"Focus su Drospirenone"</a></b> <i>di V. Bruni</i></li>
    <li><b><a href="download/relazioni/266/Costantino.ppt">"Il futuro della contraccezione ormonale: quale sarà il ruolo occupato dall’etonogestrel? "</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/266/Ferrari.ppt">"Clormadinone, perché prescriverlo."</a></b> <i>di S. Ferrari</i></li>
    <li><b><a href="download/relazioni/266/Furicchia.ppt">"Estroprogestinici e Rischio Anestesiologico"</a></b> <i>di G. Furicchia</i></li>
    <li><b><a href="download/relazioni/266/Guaraldi.pptx">"Scelte contraccettive in Italia vs Europa ed America"</a></b> <i>di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/266/Marci.ppt">"La contraccezione ormonale nella terapia dell’endometriosi"</a></b> <i>di R. Marci</i></li>
    <li><b><a href="download/relazioni/266/Morgante.pdf">"La contraccezione nell’adolescenza"</a></b> <i>di G. Morgante</i></li>
    <li><b><a href="download/relazioni/266/Nedea.pptx">"Lo iud è ancora richiesto? Perchè e quante donne lo preferiscono?"</a></b> <i>di D. Nedea</i></li>
    <li><b><a href="download/relazioni/266/Vaccaro.ppt">"COC e TROMBOSI"</a></b> <i>di V. Vaccaro</i></li>
    <li><b><a href="download/relazioni/266/Valerio.ppt">"Levonorgestrel, desogestrel, gestodene e nomegestrolo a confronto"</a></b> <i>di A. Valerio</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Microsoft&reg; Power Point&reg; 2010 (.pptx) o in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '258':
?>
<h2>Elenco relazioni del 57° raduno Gruppo 'Altaitalia'</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/258/Ciorba.ppt">"Alterazioni fisiopatologiche indotte dal tumore della laringe"</a></b> <i>di A. Ciorba</i></li>
    <li><b><a href="download/relazioni/258/Malagutti_Aimoni_Ciorba.ppt">"Le alterazioni fisio-patologiche legate al tumore del cavo orale"</a></b> <i>di N. Malagutti, C. Aimoni, A. Ciorba</i></li>
    <li><b><a href="download/relazioni/258/Merlo_Raffaelli_Pastore.ppt">"Alterazioni funzionali iatrogene e strategie riabilitative chirurgiche"</a></b> <i>di R. Merlo, R. Raffaelli, A. Pastore</i></li>
    <li><b><a href="download/relazioni/258/Pastore.ppt">"La riabilitazione funzionale nella chirurgia oncologica oro-faringo-laringea"</a></b> <i>di A. Pastore</i></li>
    <li><b><a href="download/relazioni/258/Poletti.ppt">"Il consenso informato "</a></b> <i>di D. Poletti</i></li>
    <li><b><a href="download/relazioni/258/Stomeo.ppt">"Riabilitazione post-chirurgica fonoarticolatoria"</a></b> <i>di F. Stomeo</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt)
<?php
    break;

    case '255':
?>
<h2>Elenco relazioni del convegno "L'appropriatezza in endoscopia digestiva"</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/255/Caletti.ppt">"L’accreditamento dei servizi in Endoscopia Digestiva dalla Teoria alla Pratica. Quali ricadute sui Servizi?"</a></b> <i>di G. Ciorba</i></li>
    <li><b><a href="download/relazioni/255/Di_Giulio.ppt">"Appropriatezza e Costi"</a></b> <i>di E. Di Giulio</i></li>
    <li><b><a href="download/relazioni/255/Fabbri.ppt">"Il Percorso del paziente con tumore del colon retto"</a></b> <i>di C. Fabbri</i></li>
    <li><b><a href="download/relazioni/255/Fusaroli.ppt">"Role of EUS in colon lesions"</a></b> <i>di P. Fusaroli</i></li>
    <li><b><a href="download/relazioni/255/Gaiani.ppt">"L’appropriatezza delle prestazioni infermieristiche in Endoscopia Digestiva"</a></b> <i>di R. Gaiani</i></li>
    <li><b><a href="download/relazioni/255/Gullini.ppt">"Presentazionde del convegno"</a></b> <i>di S. Gullini</i></li>
    <li><b><a href="download/relazioni/255/Matarese.ppt">"Controlli e follow-up: quando si e quando no"</a></b> <i>di V. G. Matarese</i></li>
    <li><b><a href="download/relazioni/255/Merighi.ppt">"La richiesta urgente: quando, dove, con chi"</a></b> <i>di A. Merighi</i></li>
    <li><b><a href="download/relazioni/255/Petrini.ppt">"Appropriatezza della sedazione: tra linee guida e legislazione. Quando, come e chi la deve fare"</a></b> <i>di S. Petrini</i></li>
    <li><b><a href="download/relazioni/255/Ruina.ppt">"L'appropriatezza in endoscopia digestiva"</a></b> <i>di M. Ruina</i></li>
    <li><b><a href="download/relazioni/255/Sensi.ppt">"La malattia di parkinson in fase avanzata: opzioni terapeutiche"</a></b> <i>di M. Sensi</i></li>
    <li><b><a href="download/relazioni/255/Stroppa.ppt">"Appropriatezza e  rischio clinico in endoscopia digestiva"</a></b> <i>di I. Stroppa</i></li>
    <li><b><a href="download/relazioni/255/Tafi.ppt">"Bioetica e nutrizione artificiale"</a></b> <i>di A. Tafi</i></li>
    <li><b><a href="download/relazioni/255/Trevisani.ppt">"Appropriatezza delle Richieste: Esperienza di Ferrara"</a></b> <i>di L. Trevisani</i></li>
    <li><b><a href="download/relazioni/255/Triossi.ppt">"Appropriatezza dell’operatività in corso di colonscopia: polipectomia, mucosectomia, dissezione sottomucosa "</a></b> <i>di O. Triossi</i></li>
    <li><b><a href="download/relazioni/255/Zoppellari.pptx">"Appropriatezza della PEG nel paziente neurologico complesso: stato vegetativo permanente"</a></b> <i>di R. Zoppellari</i></li>
    <li><b><a href="download/relazioni/255/Zurlo.ppt">"La PEG nel paziente anziano demente: il punto di vista del Geriatra"</a></b> <i>di A. Zurlo</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
I file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Microsoft&reg; Power Point&reg; 2010 (.pptx)
<?php
    break;

    case '254':
?>
<h2>Elenco relazioni del convegno "Ostetricia e ginecologia "novità""</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/254/Costantino.ppt">"Trattamento della dismenorrea e del dolore post-episiotomico con ibuprofene sale di arginina."</a></b> <i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/254/D'Ambrosio.ppt">"Maternità e dipendenze patologiche: il ruolo dello psicologo"</a></b> <i>di F. D'Ambrosio</i></li>
    <li><b><a href="download/relazioni/254/De_Francesco.ppt">"La tristezza che assale la mamma dopo il parto, cause, sintomi e rimedi."</a></b> <i>di L. De Francesco</i></li>
    <li><b><a href="download/relazioni/254/Furicchia.ppt">"Il management della gravida tossicodipendente: il ruolo dell’anestesista-rianimatore"</a></b> <i>di G. Furicchia</i></li>
    <li><b><a href="download/relazioni/254/Giugliano.ppt">"L-Metionina, Ibiscus sabdariffea e Boswellia serrata nel trattamento delle infezioni urinarie in gravidanza"</a></b> <i>di E. Giugliano</i></li>
    <li><b><a href="download/relazioni/254/Guaraldi.ppt">"Alcool e gravidanza: sindrome feto alcolica."</a></b> <i>di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/254/Guerra.ppt">"Antiossidanti e neurotrofici in ginecologia: nuove prospettive terapeutiche"</a></b> <i>di L. Guerra</i></li>
    <li><b><a href="download/relazioni/254/Larocca.ppt">"La vita prima di nascere"</a></b> <i>di F. Larocca</i></li>
    <li><b><a href="download/relazioni/254/Mantegazza_Murina.pptx">"Dolore pelvico cronico: nuovi approcci terapeutici"</a></b> <i>di V. Mantegazza, F. Murina</i></li>
    <li><b><a href="download/relazioni/254/Marci.ppt">"La sindrome premestruale: eziopatogenesi, diagnosi e terapia"</a></b> <i>di R. Marci</i></li>
    <li><b><a href="download/relazioni/254/Migliaccio.pptx">"Eziopatogenesi della vulvodinia: nuovi approcci terapeutici"</a></b> <i>di R. Migliaccio</i></li>
    <li><b><a href="download/relazioni/254/Morgante.pptx">"Tabagismo ed Alcool: il loro ruolo nella infertilità maschile e femminile."</a></b> <i>di G. Morgante</i></li>
    <li><b><a href="download/relazioni/254/Ricchieri.ppt">"Le proantocianidine nella profilassi delle infezioni urinarie"</a></b> <i>di F. Ricchieri</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt) o in formato Microsoft&reg; Power Point&reg; 2010 (.pptx)
<?php
    break;

    case '229':
?>
<h2>Elenco relazioni del convegno "Ostetricia e Ginecologia 'Novità'"</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/229/Bonaccorsi.pdf">"La sindrome metabolica in postmenopausa"</a></b> <i>di G. Bonaccorsi</i></li>
    <li><b><a href="download/relazioni/229/Bruni.pdf">"Contraccezione per via orale e riduzione dell’intervallo libero da ormoni: quali benefici"</a></b> <i>di V. Bruni</i></li>
    <li><b><a href="download/relazioni/229/Cagnacci.pdf">"Estradiolo e Dienogest: quali altri vantaggi oltre la contracceziione"</a></b> <i>di A. Cagnacci</i></li>
    <li><b><a href="download/relazioni/229/Carlomagno.pdf">"Inositol, Melatonin and oocyte quality"</a></b><i> di G. Carlomagno</i></li>
    <li><b><a href="download/relazioni/229/Costantino.pdf">"L'adelmidrol nelle iperalgesie vulvovaginali nel post-parto e puerperio"</a></b><i> di D. Costantino</i></li>
    <li><b><a href="download/relazioni/229/Costantino_2.pdf">"L’inositolo"</a></b><i>di D. Costantino</i></li>
    <li><b><a href="download/relazioni/229/Dante_Facchinetti.pdf">"L’inositolo nella terapia della PCOS"</a></b><i> di G. Dante, F. Facchinetti</i></li>
    <li><b><a href="download/relazioni/229/Farina.pdf">"Anello vaginale ed effetti metabolici"</a></b><i> di A. E. Farina</i></li>
    <li><b><a href="download/relazioni/229/Furicchia.pdf">"Il ruolo dell’anestesista nella terapia del dolore cronico"</a></b><i> di G. Furicchia</i></li>
    <li><b><a href="download/relazioni/229/Giugliano.pdf">"Impiego non contraccettivo della pillola con solo progestinico"</a></b><i> di E. Giugliano</i></li>
    <li><b><a href="download/relazioni/229/Guaraldi.pdf">"Micronutrienti in gravidanza"</a></b><i> di C. Guaraldi</i></li>
    <li><b><a href="download/relazioni/229/Marelli.pdf">"Inositolo, anti-ossidanti e apparato riproduttivo maschile"</a></b><i>di G. Marelli</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;

    case '84':
?>
<h2>Il Linfonodo Sentinella in Chirurgia: Attualità e Prospettive</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/84/Dionigi.ppt">"Il monitoraggio nervoso intraoperatorio"</a></b> <i>di G. Dionigi</i></li>
    <li><b><a href="download/relazioni/84/Feggi.ppt">"Medicina nucleare e biopsia del linfonodo sentinella"</a></b> <i>di L. M. Feggi</i></li>
    <li><b><a href="download/relazioni/84/Feo.ppt">"Colon: Il punto di vista del chirurgo"</a></b> <i>di C. Feo</i></li>
    <li><b><a href="download/relazioni/84/Lanza.ppt">"Il punto di vista dell’anatomo-patologo"</a></b><i> di G. Lanza</i></li>
    <li><b><a href="download/relazioni/84/Rossi.ppt">"Ecografia preoperatoria ed indicazioni all’exeresi del linfonodo sentinella "</a></b><i> di R. Rossi</i></li>
    <li><b><a href="download/relazioni/84/Verdecchia.ppt">"Controversie nella biopsia del linfonodo sentinella nel melanoma"</a></b><i>di G. M. Verdecchia</i></li>
    <li><b><a href="download/relazioni/84/Zanzi.ppt">"L'esperienza ferrarese"</a></b><i> di M. V. Zanzi</i></li>
    <li><b><a href="download/relazioni/84/Zatelli.ppt">"Analisi genetica del nodulo tiroideo: implicazioni per l’esecuzione del linfonodo sentinella"</a></b><i> di M. C. Zatelli</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Microsoft&reg; Power Point&reg; 2003 (.ppt)
<?php
    break;

    case '79':
?>
<h2>Elenco relazioni del convegno "Ostetricia e Ginecologia 'Novità'"</h2>
<ul class="elenco_relazioni">
    <li><b><a href="download/relazioni/79/Bertolazzi.pdf">"Lesioni da caustici. Gestione pre-endoscopica nel DEA"</a></b> <i>di P. Bertolazzi</i></li>
    <li><b><a href="download/relazioni/79/Bozzolani.pdf">"Trattamento intensivo pre-endoscopico nel paziente con emorragia digestiva"</a></b> <i>di G. Bozzolani</i></li>
    <li><b><a href="download/relazioni/79/Carlini.pdf">"Percorsi Integrati in Endoscopia Digestiva: perché questa scelta?"</a></b> <i>di E. Carlini</i></li>
    <li><b><a href="download/relazioni/79/Cifalà.pdf">"Il timing dell’endoscopia e descrizione degli Algoritmi"</a></b> <i>di V. Cifalà</i></li>
    <li><b><a href="download/relazioni/79/Galeotti_Salviato.pdf">"Le Emorragie Digestive Varicose e Non-Varicose - Ruolo della Radiologia Interventistica"</a></b> <i>di R. Galeotti, E. Salviato</i></li>
    <li><b><a href="download/relazioni/79/Giacometti.pdf">"Il Corpo Estraneo: Gestione pre-endoscopica nel DEA"</a></b> <i>di M. Giacometti</i></li>
    <li><b><a href="download/relazioni/79/Giampreti_Locatelli.pdf">"I Caustici – Il punto di vista del CAV"</a></b> <i>di A. Giampreti, C. Locatelli</i></li>
    <li><b><a href="download/relazioni/79/Locatelli_Giampreti.pdf">"La decontaminazione mediante EGD"</a></b> <i>di C. Locatelli, A. Giampreti</i></li>
    <li><b><a href="download/relazioni/79/Matarese.pdf">"I caustici: descrizione degli algoritmi"</a></b> <i>di V. G. Matarese</i></li>
    <li><b><a href="download/relazioni/79/Merighi.pdf">"L’esperienza AIGO-SIED-SIGE-SIMEU Emilia-Romagna in tema di emergenze-urgenze in Endoscopia Digestiva"</a></b> <i>di A. Merighi</i></li>
    <li><b><a href="download/relazioni/79/Michelini.pdf">"L'endoscopia d'urgenza in età pediatrica"</a></b> <i>di M. E. Michelini</i></li>
    <li><b><a href="download/relazioni/79/Osti.pdf">"Il ruolo dell’anestesista rianimatore nella gestione del paziente con ingestione di caustici"</a></b> <i>di D. Osti</i></li>
    <li><b><a href="download/relazioni/79/Pezzoli.pdf">"La colonscopia in urgenza: quando e come"</a></b> <i>di A. Pezzoli</i></li>
    <li><b><a href="download/relazioni/79/Ricciardelli.pdf">"Interfaccia AUSL-AOU"</a></b> <i>di A. Ricciardelli</i></li>
    <li><b><a href="download/relazioni/79/Rossi.pdf">"Caustici: Ruolo dell’E.D."</a></b> <i>di A. Rossi</i></li>
    <li><b><a href="download/relazioni/79/Trevisani1.pdf">"Presentazione del gruppo di lavoro"</a></b> <i>di L. Trevisani</i></li>
    <li><b><a href="download/relazioni/79/Trevisani2.pdf">"Le emorragie varicose e non varicose: descrizione degli algoritmi"</a></b> <i>di L. Trevisani</i></li>
    <li><b><a href="download/relazioni/79/Vasquez.pdf">"Ruolo del Chirurgo nei percorsi d’urgenza"</a></b> <i>di G. Vasquez</i></li>
    <li><b><a href="download/relazioni/79/Zanotti.pdf">"L’interfaccia Azienda USL - Azienda Ospedaliera-Universitaria Ferrara"</a></b> <i>di C. Zanotti</i></li>
</ul>
<h2>Note</h2>
Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
Per scaricare una relazione cliccare sul titolo della stessa.<br/>
Tutti i file sono in formato Adobe&reg; Portable Document Format (.pdf)
<?php
    break;
    default:
        echo "Nessuna relazione disponibile. <a href='relazioni.php'>Torna all'elenco delle relazioni disponibili</a>";
}
$gen->chiudi_pagina();
?>
