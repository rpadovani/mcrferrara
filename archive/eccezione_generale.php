<?php
require_once('php/generatore.php');

$gen = new generatore("Eccezione generale");
$gen->chiudi_head();
$gen->crea_body("eccezione");
$gen->inserisci_contenuto();
?>
<h2>Ci scusiamo per il disagio</h2>
Qualcosa di imprevisto è successo che non ha permesso ai nostri server di soddisfare la sua richiesta. <br/>
Una email è già stata inviata ai nostri tecnici in modo che riescano ad indagare sul fatto. <br/>
Scusandoci per il disagio le consigliamo di provare a ripetere l'operazione, e nel caso ritornasse ancora a questa pagina di riprovarci tra qualche giorno. <br/>
<br/>
Nel frattempo potrebbe volerci contattare, nel caso la rimandiamo alla pagina con i nostri <a href="contatti.php">contatti</a>
<?php
$gen->chiudi_pagina();
?>
