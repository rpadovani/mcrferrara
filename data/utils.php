<?php

function set_language($headerValue, $getValue) {
    $locale = locale_accept_from_http($headerValue);

    if (!$locale) {
        $locale = 'it_IT';
    } else if ($getValue) {
        $locale = $getValue;
    }

    putenv("LANG=" . $locale);
    setlocale(LC_ALL, $locale);
    bindtextdomain("messages", "i18n");
    bind_textdomain_codeset("messages", "UTF-8");
}