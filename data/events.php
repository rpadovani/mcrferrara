<?php

require_once(__DIR__ . '/../config/config.php');

class events
{
    protected $connection = null;

    public function __construct()
    {
        $dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_SERVER . ';charset=UTF8';
        $user = DB_USERNAME;
        $password = DB_PASSWORD;

        try {
            $this->connection = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            echo '<!--Connection failed: ' . $e->getMessage() . '-->';
        }
    }

    public function __destruct()
    {
        $this->connection = null;
    }

    /**
     * @param $limit
     * @param int $skip
     * @return array
     * @throws Exception
     */
    function next_events($limit, $skip = 0)
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT id_convegno, titolo, luogo, start_date, end_date from events 
                  WHERE start_date >= CURDATE() 
                  ORDER BY start_date ASC 
                  LIMIT :skipValue, :limitValue";

        $sth = $this->connection->prepare($query);
        $sth->bindParam(':skipValue', $skip, PDO::PARAM_INT);
        $sth->bindParam(':limitValue', $limit, PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $year
     * @return array
     * @throws Exception
     */
    function events_by_year($year)
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }


        $query = "SELECT * from events 
                  WHERE YEAR(start_date) = :yearValue 
                  ORDER BY start_date ASC";

        $sth = $this->connection->prepare($query);
        $sth->bindParam(':yearValue', $year, PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     * @throws Exception
     */
    function counts_events_by_year()
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT COUNT(*) AS count, YEAR(start_date) AS year FROM events 
                  GROUP BY YEAR(start_date) DESC";

        $sth = $this->connection->prepare($query);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    function get_event_by_id($id)
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT * from events 
                  WHERE id_convegno = :id_convegno";

        $sth = $this->connection->prepare($query);
        $sth->bindParam(':id_convegno', $id, PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $limit
     * @param int $skip
     * @return array
     * @throws Exception
     */
    function last_relations($limit, $skip = 0)
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT id_convegno, titolo, luogo, start_date from events 
                  WHERE relazioni = 1 
                  ORDER BY start_date DESC 
                  LIMIT :skipValue, :limitValue";

        $sth = $this->connection->prepare($query);
        $sth->bindParam(':skipValue', $skip, PDO::PARAM_INT);
        $sth->bindParam(':limitValue', $limit, PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     * @throws Exception
     */
    function get_all_relations()
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT * from events 
                  WHERE relazioni = 1 
                  ORDER BY start_date DESC";

        $sth = $this->connection->prepare($query);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @return array
     * @throws Exception
     */
    function get_next_online_subscriptions()
    {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT id_convegno, titolo, start_date from events 
                  WHERE start_date >= CURDATE() AND iscrizioni = 1
                  ORDER BY start_date ASC";

        $sth = $this->connection->prepare($query);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $event_id
     * @return array
     * @throws Exception
     */
    function get_roles_for_event($event_id) {
        if ($this->connection === null) {
            throw new Exception('No database connection');
        }

        $query = "SELECT * from roles 
              WHERE event_id = :event_id";

        $sth = $this->connection->prepare($query);
        $sth->bindParam(':event_id', $event_id, PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}