<?php
require_once('php/generatore.php');
require_once('data/events.php');

$events = new events();

$id_relazione = '0';
if (isset($_GET['id'])) {
    $id_relazione = $_GET['id'];
}

try {
    $event = $events->get_event_by_id($id_relazione);
} catch (Exception $e) {
    ?>
    <div class="alert alert-danger" role="alert">
        Abbiamo riscontrato un problema, i nostri tecnici sono stati informati e sono al lavoro per risolvere.
        Per favore riprovi fra qualche ora.
    </div>
    <?php
}

if ($event == null || $event['relazioni'] != 1) {
    header('Location: https://www.mcrferrara.org/relazioni.php');
}

$gen = new generatore("Relazioni evento " . $event['titolo']);
$gen->chiudi_head();
$gen->crea_body();

function print_relation($file_name, $title, $author = null)
{
    if (isset($_GET['id'])) {
        $id_relazione = $_GET['id'];
        $url_prefix = 'download/relazioni/' . $id_relazione . '/';

        ?>
        <li>
            <b><a href="<?= $url_prefix . $file_name ?>">“<?= $title ?>”</a></b> <?php
            if ($author) { ?><i>di <?= $author ?></i><?php } ?>
        </li>
        <?php
    }
}


?>

    <div class="container">

    <div class="card">
                <div class="card-header text-white bg-mcr">
                    <?= strftime('%d %B %Y', strtotime($event['start_date'])) ?>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?= $event['titolo'] ?></h5>
                    <p class="card-text"><?= $event['luogo'] ?>.</p>
                    <a href="dettagli_convegno.php?id=<?= $event['id_convegno'] ?>"
                       class="btn text-white bg-mcr float-right">Vedi scheda evento &rarr;</a>
                </div>
            </div>

        <h2>Relazioni</h2>

        <ul>
            <?php

            switch ($id_relazione) {

            case '511':
                print_relation('Bartolomei.pdf', "Il ruolo della PET: inquadramento medico-nucleareo", "M. Bartolomei");
                print_relation('Bianchini.pdf', "La gestione peri-operatoria: dalla valutazione dello stato nutrizionale alla presa in carico logopedica", "C.Bianchini, N.De Luca");
                print_relation('Carandina.pdf', "La chemioterapia e l'immunoterapia", "I. Carandina");
                print_relation('Contedini.pdf', "PDTA del paziente con neoplasie del distretto cervico facciale", "F. Contedini");
                print_relation('Geminiani.pdf', "Il follow up", "C. Bianchini, M. Geminiani");
                print_relation('Mandrioli.pdf', "Il ruolo della Chirurgia Maxillo Facciale", "S. Mandrioli");
                print_relation('Montinari.pdf', "La diagnosi cito-istologica", "E. Montinari");
                print_relation('Tombesi.pdf', "Imaging ecografico: i limiti", "P. Tombesi");
                print_relation('Vecchiatini.pdf', "Il punto di vista dell'odontoiatra", "R. Vecchiatini");
            break;

            case '510':
                print_relation('Busin.pdf', "Il trapianto di cornea nel XXI secolo", "M. Busin");
                print_relation('D_Angelo.pdf', "Novità nel trattamento dell'edema maculare diabetico", "S. D'Angelo");
                print_relation('Lamberti.pdf', "Gestione dell'edema maculare nel paziente glaucomatoso", "G. Lamberti");
                print_relation('Parmeggiani.pdf', "Terapia Genica nelle Distrofie Retiniche Ereditarie", "F. Parmeggiani");
                print_relation('Salgari.pdf', "Chirurgia della cataratta negli occhi sottoposti a cheratoplastica", "N. Salgari");
                print_relation('Sebastiani.pdf', "Disciplina dei rapporti fra Servizio Sanitario Nazionale ed Università", "Sebastiani");
                print_relation('Spena.pdf', "Approccio al Paziente con Ulcera Corneale", "R. Spena");
                print_relation('Vason.pdf', "Aspetti di Anestesia Pediatrica e Neonatale in Chirurgia Oculistica", "M. Vason");
            break;

            case '509':
                print_relation('Brunori.pdf', "Cronicità e fragilità in ambito neurosensoriale nel paziente anziano", "M. Brunori");
                print_relation('Cammaroto.pdf', "OSAS nell'anziano", "G. Cammaroto");
                print_relation('Cerritelli.pdf', "Gli acufeni nell'anziano: Epidemiologia, clinica e trattamento", "L. Cerritelli");
                print_relation('Chiarello.pdf', "I disturbi della deglutizione nell'anziano", "G. Chiarello");
                print_relation('Malago.pdf', "Vertigine periferica nell'anziano", "L.Crema, M.Malagò");
                print_relation('Stomeo.pdf', "La voce senile: aspetti clinici", "S. Savini, F. Stomeo");
                print_relation('Trevisan.pdf', "Disequilibrio nell’anziano e prevenzione delle cadute", "C. Trevisan");
            break;

            case '507':
                print_relation('Bagni.pdf', "Quadri anatomo-patologici", "I. Bagni");
                print_relation('Bondanelli.pdf', "Definizione, eziologia ed epidemiologi", "M. Bondanelli");
                print_relation('Daniele.pdf', "Inquadramento clinico e laboratoristico", "A. Daniele");
                print_relation('Gagliardi.pdf', "Il Follow-up", "I. Gagliardi");
                print_relation('Galeotti.pdf', "Il sampling delle vene surrenaliche", "R. Galeotti");
                print_relation('Ragazzi.pdf', "La gestione perioperatoria", "R. Ragazzi");
                print_relation('Tilli.pdf', "Inquadramento radiologico", "M. Tilli, E. Raimondi");
                print_relation('Urso.pdf', "Inquadramento Medico-Nucleare", "L. Urso");
            break;

            case '506':
                print_relation('Gagliano.pdf', "La stadiazione radiologica: limiti", "M. Gagliano");
                print_relation('Lancia.pdf', "Carcinoma prostatico: il punto di vista oncologico", "F. Lancia");
                print_relation('Malorgio.pdf', "Il percorso terapeutico e il follow-up: il punto di vista radioterapico", "A. Malorgio");
                print_relation('Nieri.pdf', "Il ruolo dell'imaging medico-nucleare PET/CT", "A. Nieri");
                print_relation('Rocca.pdf', "Neoplasia Prostatica: Ruolo del Chirurgo", "G. C. Rocca");
                print_relation('Simone.pdf', "Il PSA: quando richiedere un approfondimento e la biopsia", "M. Simone");
                print_relation('Tilli.pdf', "Risonanza multi-parametrica della prostata", "M. Tilli");
            break;

            case '505':
                print_relation('Locatelli.pdf', "Sfide emergenti in tossicologia clinica", "C. Locatelli");
            break;

            case '504':
                print_relation('Buda.pdf', "Intelligenza artificiale per la colonscopia: serve davvero?", "A. Buda");
                print_relation('Gianni.pdf', "La gestione integrata dei pazienti affetti da encefalopatia epatica tra ospedale e territorio", "S. Gianni");
                print_relation('Imondi_1.pdf', "Caso clinico #1", "A. Imondi");
                print_relation('Imondi_2.pdf', "Caso clinico #2", "A. Imondi");
                print_relation('Imondi_3.pdf', "Caso clinico #3", "A. Imondi");
                print_relation('Imondi_4.pdf', "Caso clinico #4", "A. Imondi");
                print_relation('Imondi_5.pdf', "Caso clinico #5", "A. Imondi");
                print_relation('Palmonari.pdf', "Screening del cancro del Colon-retto: si può aumentare l'adesione?", "C. Palmonari");
                print_relation('Pezzoli.pdf', "Endoscopic match: intelligenza artificiale, serve davvero?", "A. Pezzoli");
                print_relation('Savarino.pdf', "Fecal Transplantation: Come e Quando", "E. V. Savarino");
            break;

            case '503':
                print_relation('Banzi.pdf', "Ecografia ginecologica: dalle Linee guida alla realtà consultoriale", "C. Banzi");
                print_relation('Bruschi.pdf', "DSA2 ostetrico: AUSL Ferrara stato dell'arte", "C. Bruschi");
                print_relation('Constantino.pdf', "Associazione tra i differenti inositoli e risultati clinici della PCOS", "D. Constantino");
                print_relation('Constantino_2.pdf', "Rigenase e Poliesanide effetti positivi sul microambiente vaginale", "D. Constantino");
                print_relation('Di_Primio.pdf', "Riabilitazione del pavimento pelvico: il percorso consultoriale", "I. Di Primio");
                print_relation('Gotti.pdf', "Regione Emilia Romagna: il nuovo programma di screening per il cervicocarcinoma", "G. Gotti");
                print_relation('Guaraldi.pdf', "Anemia ferropriva e rischi materno fetali", "C. Guaraldi");
                print_relation('Guaraldi_2.pdf', "Amenorrea: inquadramento diagnostico. Il ruolo del consultorio", "C. Guaraldi");
                print_relation('Licori.pdf', "COC e Acne: una nuova opzione terapeutica", "D. Licori");
                print_relation('Piccolotti.pdf', "Sindrome genitourinaria: quale terapia?", "I. Piccolotti");
                print_relation('Pisati.pdf', "Nutraceutici e rischio di malattia cardio-vascolare in menopausa: oltre la gestione della dislipidemia", "R. Pisati");
                print_relation('Pontesilli.pdf', "Sovrappeso-obesità e infertilità femminile. Approcci preventivi e terapeutici", "G. M. Pontesilli");
            break;

            case '501':
                print_relation('Bandiera.pdf', "Iperemesi gravidica, non un semplice malessere ma un disturbo invalidante", "E. Bandiera");
                print_relation('Banzi.pdf', "L'ecografia del I trimestre solo datazione o vaso di Pandora", "C. Banzi");
                print_relation('Benfenati.pdf', "Contraccezione gratuita. Cosa si è fatto e cosa si deve ancora fare", "S. Benfenati");
                print_relation('Costantino.pdf', "La chiroglutina nel trattamento del diabete gestazionale", "D. Costantino");
                print_relation('Costantino_2.pdf', "Nuovo approcio dell'ipoferremia in gravidanza", "D. Costantino");
                print_relation('Fioretti.pdf', "Il ruolo dell'energia mitocondriale nella fisiologia del sistema riproduttivo femminile", "B. Fioretti.pdf");
                print_relation('Gotti.pdf', "Vitamina D: quanto è davvero importante assumerla in gravidanza", "G. Gotti");
                print_relation('Guaraldi.pdf', "Dispositivo sottocutaneo. Contraccezione e non solo...", "C. Guaraldi");
                print_relation('Guaraldi_2.pdf', "Minaccia di aborto, accuratezza diagnostica, protocolli terpaeutici nebulosi e realtà consultoriale", "C.Guaraldi");
                print_relation('Morano.pdf', "Ecografia del III° trimestre.Importante per il monitoraggio della gravidanza o solamente ostetricia difenziva", "D. Morano");
                print_relation('Pasi.pdf', "Tibolone 30 anni e non sentirli", "A. Pasi");
                print_relation('Rossi.pdf', "Covid 19 e stato emotivo in epoca perinatale", "I. Rossi");
                print_relation('Sabbioni.pdf', 'Contraccezione con estrogeno naturale e dati di real life', 'L. Sabbioni');
                print_relation('Savarese.pdf', "DNA fetale -NIPT Stato dell'Arte", "G. Savarese");
                print_relation('Selvetti.pdf', "La contraccezione orale con solo progestinico", "M. Selvetti");

            break;

            case '500':

                    print_relation('Bagni.pdf', 'Citologia e istologia', 'I. Bagni');
                    print_relation('Bianchini.pdf', 'Intro casi clinici', 'C. Bianchini');
                    print_relation('Bianchini_2.pdf', 'Quale indicazione per la chirurgia delle metastasi linfonodali', 'C. Bianchini');
                    print_relation('Cattaneo.pdf', 'Intro casi clinici', 'C. Cattaneo');
                    print_relation('Gagliardi.pdf', 'Diagnostica molecolare', 'I. Gagliardi');
                    print_relation('Nieri.pdf', 'Intro casi clinici', 'A. Nieri');
                    print_relation('Nieri.pdf', 'Quando fare la terapia radiometabolica con 131I', 'A. Nieri');
                    print_relation('Sibilla.pdf', 'Intro casi clinici', 'M.G. Sibilla');
                    print_relation('Sibilla_2.pdf', 'La terapia chirurgica tiroidectomia o emitiroidectomia', 'M.G. Sibilla');
                    print_relation('Stefanelli.pdf', 'Il carcinoma tiroideo avanzato metastatico', 'A. Stefanelli');
                    print_relation('Stefanelli_2.pdf', 'Intro casi clinici', 'A. Stefanelli');
                    print_relation('Tamburini.pdf', 'Intro casi clinici', 'N. Tamburini');
                    print_relation('Tamburini_2.pdf', 'Quale ruolo per il chirurgo', 'N. Tamburini');
                    print_relation('Vason.pdf', 'Intro casi clinici', 'M. Vason, M. Verri');
                    print_relation('Vason_2.pdf', 'Quali sono le possibili complicanze intra e perioperatorie ', 'M. Vason, M. Verri');

            break;

                case '471':

                    print_relation('Anastasi.pdf', 'La diagnosi preimpianto oggi', 'A. Anastasi');
                    print_relation('Bernardi.pdf', 'L\'intervento di gruppo con le coppie. Obiettivi, metodologie e risultati', 'S. Bernardi');
                    print_relation('Capodanno.pdf', 'Tumore e preservazione della fertilità. A che punto siamo', 'F. Capodanno');
                    print_relation('Costantino.pdf', 'Le linee guida territoriali nella Procreazione Medicalmente Assistita (PMA)', 'D. Costantino');
                    print_relation('Favero.pdf', 'Efficacia dello studio morfocinetico dell\'embrione', 'F. Favero');
                    print_relation('Guaraldi.pdf', 'La gravidanza dopo ovodonazione. Stima del rischio ostetrico e perinatale', 'C. Guaraldi');
                    print_relation('Guglielmino.pdf', 'Natalità e fertilità. Cosa è cambiato negli ultimi decenni', 'A. Guglielmino');
                    print_relation('Morgante.pdf', 'Terapia medica dei fibromi in donne con desiderio di gravidanza', 'G. Morgante');
                    print_relation('Palini.pdf', 'miRNA. biomrkers embrionale e cross talk endometrio-embrione', 'S. Palini');
                    print_relation('Rambelli.pdf', 'La preservazione della fertilità nelle pazienti oncologiche', 'V. Rambelli');
                    print_relation('Rossi.pdf', 'La tutela della fertilità in Emilia Romagna dopo la Legge 40 del 2004 LEA, proposta Regionale e accreditamento', 'S. Rossi');

                    break;

                case '468':

                    print_relation('Antonioli.pdf', 'Sorveglianza e controllo delle infezioni in riabilitazione come coniugare le diverse esigenze', 'P. Antonioli, S. Lavezzi');
                    print_relation('Borre.pdf', 'Le infezioni osteoarticolari e protesiche', ' S. Borre');
                    print_relation('Carli.pdf', 'Infezioni e outcome riabilitativo', 'S. Carli');
                    print_relation('Cavallo.pdf', 'Le infezioni post neurochirurgiche come affrontarle', 'M. A. Cavallo');
                    print_relation('Cojutti.pdf', 'Therateuty drug monitoring of Beta Lactams and other Antibiotics in the intensive care unit which agents which patients and which infections', 'P. G. Cojutti');
                    print_relation('Crapis.pdf', 'Infezioni delle vie urinarie complicate nel mielo e cerebroleso', 'M. Crapis');
                    print_relation('Cultrera.pdf', 'Le infezioni post neurochirurgiche come affrontarle', 'R. Cultrera');
                    print_relation('Giordano.pdf', 'Le infezioni post neurochirurgiche come affrontarle', 'F. Giordano');
                    print_relation('Libanore.pdf', 'I patogeni emergenti e i fattori di rischio CAP e compagne di viaggio', 'M.Libanore, C. Carillo');
                    print_relation('Pan.pdf', 'Epidemiologia generale', 'A. Pan');
                    print_relation('Rizzi.pdf', 'Infezioni da devices ed endocarditi', 'M. Rizzi');
                    print_relation('Tascini.pdf', 'Infezione da Clostridium difficile una patologia emergente in un contesto articolato', 'C. Tascini');
                    print_relation('Viale.pdf', 'Shock settico e sepsi diagnosi precoce e gestione terapeutica', 'P. L. Viale');
                    print_relation('Volta.pdf', 'Shock settico e sepsi diagnosi e gestione terapeutica', 'C. A. Volta');


                    break;

                case '451':

                    print_relation("Azzolini.pdf", "10 anni di POEM: quale bilancio", "F. Azzolini");
                    print_relation("Carradori.pdf", "Sanità e sostenibilità, uscire da un approccio contingente e reattivo", "T. Carradori");
                    print_relation("Cifala.pdf", "Caso clinico", "V. Cifalà");
                    print_relation("Fusetti.pdf", "Caso clinico", "N. Fusetti");
                    print_relation("Messina.pdf", "Caso clinico", "O. Messina");
                    print_relation("Petrone.pdf", "La diagnosi isto-citologica in ecoendoscopia: FNA, FNB, ROSE", "M.C. Petrone");
                    print_relation("Rizzati.pdf", "Colonscopia Virtuale: nuove applicazioni cliniche nella pratica quotidiana", "R. Rizzati");
                    print_relation("Simone.pdf", "Caso clinico", "L. Simone");
                    print_relation("Solimando.pdf", "La preparazione intestinale per la colonscopia: vecchie e nuove prospettive", "R. Solimando");
                    print_relation("Zoppelari.pdf", "La sedazione nelle procedure endoscopiche", "R. Zoppelari");

                    break;

                case '450':

                    print_relation("Benfenati.pdf", "Sessualità e gravidanza. Un tema da affrontare con consapevolezza", "S. Benfenati");
                    print_relation("Casolati.pdf", "Nausea e Vomito in gravidanza: novità nella terapia farmacologica", "E. Casolati");
                    print_relation("Costantino.pdf", "Probiotici, microbiota intestinale e benessere dell'apparato orogenitale", "D. Costantino");
                    print_relation("DeFrancesco.pdf", "Modifiche relazionali e dell’attività sessuale dei futuri papà durante la gravidanza e nel post-parto.", "L. De Francesco");
                    print_relation("Gotti.pdf", "Sindrome premestruale. Eziopatogenesi, diagnosi, terapia", "G. Gotti");
                    print_relation("Guaraldi.pdf", "Anemia in gravidanza: patologia di genere? Dalla prevenzione alla terapia.", "C. Guaraldi");
                    print_relation("Guaraldi2.pdf", "Effetti benefici dell'equolo nel trattamento della sintomatologia perimenopausale", "C. Guaraldi");
                    print_relation("Iannacci.pdf", "Microbiota e cross-talk intermuscolare. la strategia 3R nelle patologie ginecologiche intestino-correlate", "M.C. Iannacci");
                    print_relation("Meriggiola.pdf", "Atrofia vulvo-vaginale", "M. C. Meriggiola");
                    print_relation("Monari.pdf", "L'autonomia dell'ostetrica nella gestione dell'ambulatorio di gravidanza a basso rischio", "C. Monari");
                    print_relation("Morgante.pdf", "Ovaio policistico e sindrome metabolica. Il ruolo terapeutico degli inositoli", "G. Morgante");
                    print_relation("Pavani.pdf", "Modello organizzativo-gestionale del Consultorio Familiare dell'azienda USL di Ferrara", "C. Pavani");
                    print_relation("Rubini.pdf", "Carenza di folati e supplementazione in gravidanza", "M. Rubini");
                    print_relation("Stoppa.pdf", "Il ruolo del territorio dopo la gravidanza", "D. Stoppa");
                    print_relation("Zinno.pdf", "I Consultori Familiari tra innovazione e tradizione. Vecchi e nuovi compiti costituzionali", "G. Zinno");

                    break;

                case '449':

                    print_relation("Acciarri.pdf", "Timing and result of CEA in acute settings", "P. F. Acciarri");
                    print_relation("Alperin.pdf", "Investigating the separate effects of venous outflow and breathing on the CFS flow dynamix using 2 types of phase contrast sequences", "N. Alperin");
                    print_relation("Alperin2.pdf", "The effect of sleep quality on a MCI susceptible brain regions in cognitively intact elderly subjects", "N. Alperin");
                    print_relation("Bavera.pdf", "Effects of ballon angioplasty on symptoms in CCSVI-MS patients", "P. Bavera");
                    print_relation("Bergsland.pdf", "Perfusion abnormalities and cardiovascular comorbidities in neurological disorders", "N. Bergsland");
                    print_relation("Bernardi.pdf", "Coagulation circulating proteins in Multiple Sclerosis", "F. Bernardi");
                    print_relation("Bruno.pdf", "CCSVI prevalence and jugular PTA in meniere's disesase", "A. Bruno");
                    print_relation("Califano.pdf", "Result of jugular PTA on symptoms of meniere's diseases with concomitant CCSVI", "L. Califano");
                    print_relation("Casani.pdf", "What is known and what in unknown in inner ear diseases. The role of neuroinflamation", "A. P. Casani");
                    print_relation("Casetta.pdf", "How late is too late to recanalize carotid and intercranial thrombosis after stroke", "I. Casetta");
                    print_relation("Chiesa.pdf", "Neck vessels pulsatility and dementia", "S. Chiesa");
                    print_relation("DeBonis.pdf", "The Jedi syndrome", "P. De Bonis");
                    print_relation("Frigatti.pdf", "Techniques in urgent carotid surgery", "P. Frigatti");
                    print_relation("Gadda.pdf", "A multiscale model for the simulation of cerebral and extracerebral blood flows and pressures in humans", "G. Gadda");
                    print_relation("Gambaccini.pdf", "MR imaging of preivascular spaces in veno-obstructive diseases", "M. Gambaccini, Y. Ge, E. Toro");
                    print_relation("Ge.pdf", "New perspectives on age-related white matter hyperintensities in VCID research", "Y. Ge");
                    print_relation("Giaquinta.pdf", "Headache sustained relief following extracranial venous angioplastic", "A. Giaquinta");
                    print_relation("Greco.pdf", "An animal model of cerebral venous insufficiency. Implication for neuroinflammation", "A. Greco");
                    print_relation("Grego.pdf", "Asymptomatic patients. Surgery, antiplatelets, or angioplasty and stenting", "F. Grego");
                    print_relation("Hu.pdf", "Interaction between the CSF and the venous system", "J. Hu");
                    print_relation("Jakimovski.pdf", "Decrease in secondary neck vessels and cerebral aqueduct enlargement in multiple sclerosis. A 5-year longitudinal MRI study", "D. Jakimovski");
                    print_relation("Jakimovski2.pdf", "Diet, life style, and cerebral inflow effects on neuroinflammation-neurodegeneration in MS patients", "D. Jakimovski");
                    print_relation("Lagana.pdf", "Concurrent assessment of perfusion and functional connectivity in Parkinson's disease", "M. M. Laganà");
                    print_relation("Lopez.pdf", "Genetic architecture of menieres's diseasese", "J.A. Lopez Escamez");
                    print_relation("Manfredini.pdf", "Monitoring the intrecerebral flow during CEA under general anesthesia", "F. Manfredini");
                    print_relation("Mascoli.pdf", "Is local anaesthesia the best method to avoid complications during carotid cross clamping", "F. Mascoli");
                    print_relation("Mascoli.m4v", "Video of Shunt", "F. Mascoli");
                    print_relation("Neri.pdf", "Sigmoid and transverse sinus disorders related to vertigo, dizziness and headache", "G. Neri");
                    print_relation("Onorati.pdf", "The intra and extracranial veins in relatonship with chronic migraine", "P. Onorati");
                    print_relation("Petrov.pdf", "How late is too late to recanalize carotid and intercranial thrombosis after stroke", "I. Petrov");
                    print_relation("Pulli.pdf", "Better embolic protection may reinvigorate CAS", "R. Pulli");
                    print_relation("Rapezzi.pdf", "state of the art on jugular venous pulse", "C. Rapezzi");
                    print_relation("Scalfani.pdf", "Reasons for and solutions to failures of endovascular treatment of CCSVI", "S. Scalfani");
                    print_relation("Setacci.pdf", "Better embolic protection may reinvigorate CAS", "C. Setacci");
                    print_relation("Spinelli.pdf", "Accepted manuscript - General anaesthesia versus local anaesthesia in carotid endarterectomy: a systematic review and meta-analysis", "F. Spinelli");
                    print_relation("Spinelli2.pdf", "Anesthesia type determines risk of cerebral infarction", "F. Spinelli");
                    print_relation("Spinelli3.pdf", "Is local anaesthesia the best method to avoid complication during carotid cross clamping", "F. Spinelli");
                    print_relation("Spath.pdf", "Celebral protection in aortic arch surgery", "P. Spath");
                    print_relation("Taurino.pdf", "Timing and result of CEA in acute settings", "M. Taurino");
                    print_relation("Toro.pdf", "Computational inner ear circulation", "E. Toro");
                    print_relation("Volpato.pdf", "Risk factors for alzheimer and dementia", "S. Volpato");
                    print_relation("Wang.pdf", "Non-contrast Enhanced MRI in Stroke", "M. Wang");
                    print_relation("Zivadinov.pdf", "Epidemiology of cardiovascular comorbidities in aging of multiple sclerosis", "R. Zivadinov");

                    break;

                case '448':

                    print_relation("Pieraccini.pdf", "Aderenza alla terapia come sicurezza del paziente? Ruolo del farmacista in distribuzione diretta farmaci", "F. Pieraccini");
                    print_relation("Scanavacca.pdf", "L’erogazione diretta in Emilia Romagna - l’esperienza di Ferrara", "Paola Scanavacca");

                    break;

                case '447':

                    print_relation("Alteri.pdf", "Infezione da HIV: Possibilità di eradicazione", "C. Alteri");
                    print_relation("Bisoffi.pdf", "Flussi migratori: rischio infettivo e disagio sociale", "Z. Bisoffi");
                    print_relation("Calleri.pdf", "I rischi nel viaggiatore intercontinentale e la prevenzione", "G. Calleri");
                    print_relation("Calza.pdf", "Infezione da HIV: comorbidità", "L. Calza");
                    print_relation("Ceccherini.pdf", "HVC dal punto di vista virologico", "F. Ceccherini-Silberstein");
                    print_relation("Dancona.pdf", "Vaccinazioni: un dibattito aperto", "F. P. D'Ancona");
                    print_relation("Foschi.pdf", "Infezione da HCV", "F. Foschi");
                    print_relation("Garofani.pdf", "Tossicosi : nuovi scenari e strategie future", "L. Garofani");
                    print_relation("Libanore.pdf", "Infezioni da Arbovirus Emergenti: aspetti clinici", "M. Libanore");
                    print_relation("Libanore2.pdf", "Terapia Antibiotica di Precisione: utopia o realtà", "M. Libanore");
                    print_relation("Marchetti.pdf", "Microbioma intestinale: falsità e verità (nell’infezione da HIV)", "G. Marchetti");
                    print_relation("Pan.pdf", "Garantire un’opportunità di cura per il futuro", "A. Pan");
                    print_relation("Sighinolfi.pdf", "Gestione delle “popolazioni speciali”", "L. Sighinolfi");
                    print_relation("Sighinolfi2.pdf", "L’infezione da HIV oggi", "L. Sighinolfi");
                    print_relation("Sighinolfi3.pdf", "La prevenzione delle MST: non solo HIV", "L. Sighinolfi");
                    print_relation("Venturi.pdf", "Infezioni da arbovirus emergenti: il piano nazionale di sorveglianza e la prevenzione", "G. Venturi");

                    break;


                case '446':

                    print_relation("Besozzi.pdf", "TBC. Una storia infinita", "G. Besozzi");
                    print_relation("Borrini.pdf", "Epidemiologia: la nuova realtà", "B. M. Borrini");
                    print_relation("Castellotti.pdf", "TBC: il profilo attuale della malattia", "P. F. Castellotti");
                    print_relation("Contoli.pdf", "BPCO e Tubercolosi: un binomio pericoloso", "M. Contoli");
                    print_relation("Cultrera.pdf", "La tubercolosi extrapolmonare", "R. Cultrera");
                    print_relation("De_Lorenzo.pdf", "Micobatteriosi atipiche: un terreno minato", "S. De_Lorenzo");
                    print_relation("Libanore.pdf", "L’Infezione tubercolare latente (ITL) negli Operatori Sanitari", "M. Libanore");
                    print_relation("Lodi.pdf", "E’ possibile migliorare il sistema di sorveglianza?", "M. Lodi");
                    print_relation("Tadolini.pdf", "Terapia delle forme multi-resistenti", "M. Tadolini");
                    print_relation("Tortoli.pdf", "Nuove opportunità nella diagnostica di laboratorio della TBC e delle micobatteriosi", "E. Tortoli");
                    print_relation("Zoppellari.pdf", "La gestione della tubercolosi in rianimazione", "R. Zoppellari");

                    break;

                case '445':

                    print_relation("Barbot.pdf", "L'impianto cocleare nei bambini con ipoplasia del nervo", "A. Barbot");
                    print_relation("Benincasa.pdf", "L'impianto cocleare nei bambini con bilinguismo", "P. Benincasa");
                    print_relation("Minazzi.pdf", "L’impianto cocleare nell’anziano: caratteristiche della terapia logopedica", "F. Minazzi");
                    print_relation("Moalli.pdf", "L'impianto cocleare nel bambino prematuro", "R. Moalli, M. F. Attardo, A. Galli, A. Cuscunà");
                    print_relation("Montino.pdf", "Autismo e sordità: la complessità della valutazione logopedico-percettiva", "S. Montino");
                    print_relation("Nicastri.pdf", "L’impianto cocleare nel bambino con disprassia", "M. Nicastri, P. Mancini, I. Giallini");
                    print_relation("Nicastro.pdf", "L’impianto cocleare nei casi di ritardo mentale", "R. Nicastro");
                    print_relation("Rosignoli.pdf", "Dal mappaggio alla manutenzione dell'impianto cocleare: il contributo del logopedista.", "M. Rosignoli");

                    break;

                case '444':

                    print_relation("Bartoletti.pdf", "Le candidemie oggi: una gestione articolata", "M. Bartoletti");
                    print_relation("Cojutti.pdf", "La farmacologia in aiuto", "P.G. Cojutti, F. Pea");
                    print_relation("Cristini.pdf", "La Terapia Antibiotica Off – Label: un problema aperto", "F. Cristini");
                    print_relation("Del_Bono.pdf", "Pseudomonas ed Acinetobacter MDR", "V. Del Bono");
                    print_relation("Durante_Mangoni.pdf", "Stafilococco aureo ed Enterococco", "E. Durante Mangoni");
                    print_relation("Libanore.pdf", "Enterobatteri ESBL positivi", "M. Libanore");
                    print_relation("Murri.pdf", "Enterobatteri produttori di carbapenemasi", "R. Murri");
                    print_relation("Pan.pdf", "Clostridium difficile: prevenzione e terapia.", "A. Pan");
                    print_relation("Tumietto.pdf", "La Prevenzione come punto di partenza.", "F. Tumietto");

                    break;

                case '442':

                    print_relation("Ambrosio.pdf", "I percorsi diagnostici e terapeutici dei NET pancreatici e gastrointestinali", "M. R. Ambrosio");
                    print_relation("Carradori.pdf", "Integrazione interaziendale: quali orizzonti?", "T. Carradori");
                    print_relation("Cifala.pdf", "Caso clinico “live”", "V. Cifalà");
                    print_relation("Fabbri.pdf", "Endoscopia e lesione solide del pancreas", "C. Fabbri");
                    print_relation("Fabbri2.pdf", "Endoscopia e lesione solide del pancreas: seconda parte", "C. Fabbri");
                    print_relation("Fantin.pdf", "Cystic Pancreatic Neoplasms", "A. Fantin");
                    print_relation("Pagano.pdf", "I ruolo dell’endoscopia nella diagnosi e nella cura delle formazioni sottomucose del tratto gastroenterico", "N. Pagano");
                    print_relation("Pezzoli.pdf", "La gestione della litiasi del coledoco", "A. Pezzoli");
                    print_relation("Tilli.pdf", "Metodiche radiologiche di diagnosi delle neoformazioni pancreatiche", "M. Tilli");

                    break;

                case '441':
                    print_relation("Brandimarte.pdf", "Inquadramento clinico della litiasi urinaria", "A. Brandimarte");
                    print_relation("Bruschi.pdf", "Ematuria all’attenzione del medico di famiglia sintomo o malattia?", "B. Bruschi");
                    print_relation("Galizia.pdf", "Neoplasie uroteliali", "G. Galizia");
                    print_relation("Galizia2.pdf", "IPB: quale trattamento?", "G. Galizia");
                    print_relation("Luciano.pdf", "Stato dell'arte su PSA", "M. Luciano");
                    print_relation("Luciano2.pdf", "Trattamento del carcinoma prostatico", "M. Luciano");
                    print_relation("Rinaldi.pdf", "Il tumore prostatico: la chirurgia", "R. Rinaldi");
                    print_relation("Sala.pdf", "Il tumore prostatico: ruolo della risonanza magnetica", "S. Sala");
                    print_relation("Simone.pdf", "Il tumore prostatico: la chirurgia", "M. Simone");
                    print_relation("Simone2.pdf", "La litiasi: terapia tailored oppure no?", "M. Simone");
                    print_relation("Simone3.pdf", "Le neoplasie uroteliali: terapia chirurgica ma non solo", "M. Simone");
                    print_relation("Zambino.pdf", "Inquadramento clinico: dai LUTS alla Ritenzione Urinaria: Il ruolo del MMG", "C. Zambino");

                    break;

                case '431':
                    print_relation("Capucci.pdf", "Stili di vita ed inquinanti ambientali come fattore di rischio per la fertilità", "R. Capucci");
                    print_relation("Costantino.pdf", "Ipofertilità e sterilità: il ruolo delle infezioni dell'apparato genitale femminile", "D. Costantino");
                    print_relation("Gotti.pdf", "Dalla contraccezione agli iperandrogenismi", "G. Gotti");
                    print_relation("Grandi.pdf", "Estradiolo in contraccezione ormonale: vera evoluzione o vino vecchio in una nuova botte?", "G. Grandi");
                    print_relation("Gregoratti.pdf", "La dieta della fertilità: cibi pro gravidanza", "G. Gregoratti");
                    print_relation("Guaraldi.pdf", "MHT senza progestinico: razionale d’uso", "G. Guaraldi");
                    print_relation("Meneghini.pdf", "La depressione post-natale (DPN) e l’ambulatorio psicologico dedicato", "C. Meneghini");
                    print_relation("Pennacchio.pdf", "MMG: Comune di Fiscaglia", "G. Pennacchio");
                    print_relation("Zinno.pdf", "Il ruolo del Ginecologo Consultoriale nell’approccio della coppia infertile", "G. Zinno");

                    break;

                case '422':

                    print_relation("Andriani.pdf", "Andrologia ed internet: sanità 2.0", "E. Andriani");
                    print_relation("Caraceni.pdf", "La chirurgia protesica: quali risultati quale soddisfazione", "E. Caraceni");
                    print_relation("Cavallini.pdf", "La gestione andrologica del paziente psicogeno.", "G. Cavallini");
                    print_relation("Fabiani.pdf", "HPV e MST nella pratica ambulatoriale", "A. Fabiani");
                    print_relation("Franceschelli.pdf", "AZOOSPERMIA - Le tecniche di recupero degli spermatozoi", "A. Franceschelli");
                    print_relation("Gentile.pdf", "Uso e somministrazione selvaggia di farmaci", "G. Gentile");
                    print_relation("Granieri.pdf", "Il dolore in andrologia", "E.Granieri");
                    print_relation("Maretti.pdf", "Nuove frontiere nel trattamento dell'infertilità idiopatica", "C. Maretti");
                    print_relation("Meneghini.pdf", "Andrologia e Urologia: analogie e differenze", "A. Meneghini");
                    print_relation("Pescatori.pdf", "La chirurgia Protesica: quale protesi per quale paziente", "E. Pescatori");
                    print_relation("Polito.pdf", "Interpretazione dello spermiogramma", "M.Polito");
                    print_relation("Quaresma.pdf", "La traumatologia del pene", "L.Quaresma");
                    print_relation("Scarano.pdf", "Il ruolo delle infezioni sessualmente trasmesse negli insuccessi della PMA", "P. Scarano");
                    print_relation("Scarano2.pdf", "Infertilità e infiammazione", "P.Scarano");
                    print_relation("Troilo.pdf", "Tecniche e significato della selezione spermatica per ICSI", "E. Troilo, L. Parmegiani");
                    print_relation("Zenico.pdf", "Andrologia : fitoterapici ed integratori alimentari", "T. Zenico");

                    break;

                case '421':

                    print_relation("Boccia.pdf", "Workshop HCV", "S. Boccia");
                    print_relation("Sighinolfi.pdf", "Epatite da HCV e HIV in reumatologia", "L. Sighinolfi");

                    break;

                case '420':

                    print_relation("Boccia.pdf", "Workshop HCV", "S. Boccia");
                    print_relation("Grilli.pdf", "Proposta di un percorso HCV nei pazienti nefropatici", "A. Grilli");
                    print_relation("Sighinolfi.pdf", "Epatite da HCV e HIV in nefrologia", "L. Sighinolfi");

                    break;

                case '419':

                    print_relation("Centenaro.pdf", "La prescrizione e l’ educazione del paziente all'utilizzo dei device respiratori", "G. Centenaro");
                    print_relation("Contoli.pdf", "Asma e BPCO: le strategie terapeutiche", "M. Contoli");
                    print_relation("Descovich.pdf", "Esperienze regionali di PDTA nelle patologie respiratorie - AVEC", "C. Descovich");
                    print_relation("Ermini.pdf", "La prescrizione e l’educazione del paziente all’utilizzo dei device respiratori sul territorio", "G. Ermini");
                    print_relation("Fedele.pdf", "Il ruolo del farmacista SSN nel corretto uso dei device respiratori", "D. Fedele");
                    print_relation("Lavorini.pdf", "La rilevanza del device nella scelta della strategia terapeutica e criteri di scelta del device in relazione al paziente", "F. Lavorini");
                    print_relation("Marata.pdf", "Strategie per l’inserimento in Prontuario dei farmaci respiratori in Emilia Romagna", "A. M. Marata");
                    print_relation("Papi.pdf", "Gli Studi Clinici sui devices respiratori", "A. Papi");
                    print_relation("Peroni.pdf", "La scelta dei device respiratori in ambito pediatrico in relazione alle strategie terapeutiche", "D. Peroni");
                    print_relation("Rossi.pdf", "Esperienze regionali di PDTA nelle patologie respiratorie", "L. Rossi");
                    print_relation("Russi.pdf", "Documento di indirizzo relativo ai farmaci per la broncopneumopatia cronica ostruttiva (BPCO)", "E. Russi");

                    break;

                case '418':

                    print_relation("Battaglia.pdf", "Menopausa e B.M.I. stima del rischio e prevenzione. Ruolo della MHT", "C. Battaglia");
                    print_relation("Cancellieri.pdf", "Prospettive d’impiego in uro-ginecologia di trattamenti a base di acido ialuronico a bassissimo peso molecolare", "F. Cancellieri");
                    print_relation("Costantino.pdf", "Il segreto di una buona gravidanza poggia su corrette abitudini alimentari ed un adeguato esercizio fisico", "D. Costantino");
                    print_relation("Dinicola.pdf", "ruolo, distribuzione e biodisponibilità dell’acido ialuronico (HA) ad alto e basso peso molecolare", "S. Dinicola");
                    print_relation("Grandi.pdf", "Sovrappeso. Obesità e la pillola contraccettiva: profilo di tollerabilità e di sicurezza", "G. Grandi");
                    print_relation("Gregoratti.pdf", "Sovrappeso e obesità, perché andare oltre la dieta", "G. Gregoratti");
                    print_relation("Guaraldi.pdf", "Contraccezione con solo progestinico: intrauterino - sottocutaneo", "C. Guaraldi");
                    print_relation("Petrella.pdf", "Prevenire i rischi perinatali associati all’obesità", "E. Petrella");
                    print_relation("Roncarati.pdf", "Le sfide nella cura dell’obesità: perché guardare oltre il peso", "E. Roncarati");
                    print_relation("Tanas.pdf", "Sovrappeso e obesità nella bambina e nell'adolescente", "R. Tanas");

                    break;

                case '417':

                    print_relation("Barchetti.pdf", "Il team e i familiari: presenza sulla scena e comunicazione efficace", "E. Barchetti");
                    print_relation("Borrelli.pdf", "Dalle aule alla realtà: una esperienza di lavoro in un team multiculturale e multiprofessionale", "F. Borelli");
                    print_relation("Cariani.pdf", "L’addestramento al lavoro in TEAM", "D. Cariani");
                    print_relation("Chierici.pdf", "Competenze infermieristiche nella gestione del paziente con dispnea acuta in PS/Shock Room", "F. Chierici");
                    print_relation("Ferrari.pdf", "Pump o Lung Failure? Ventimask, CPAP o NIV?", "R. Ferrari");
                    print_relation("Gangitano.pdf", "Tavola rotonda: “Ventiliamo dunque siamo...” gestione del paziente in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto", "G. Gangitano");
                    print_relation("Gangitano1.pdf", "Conclusioni tavola rotonda", "G. Gangitano");
                    print_relation("Gelati.pdf", "La comunicazione all’interno del Team: mezzo di riduzione del rischio clinico e eventi avversi ?", "E. Gelati");
                    print_relation("Giacometti.pdf", "IL POLITRAUMA: quale ruolo per l’ospedale non Trauma Center Regionale (Hub e Spoke) nella catena gestionale dall’extra all’intraospedaliero", "M. Giacometti");
                    print_relation("Govoni.pdf", "Insufficienza respiratoria acuta in età pediatrica. Il ruolo del pediatra in PS", "M. R. Govoni");
                    print_relation("Guatteri.pdf", "L’esperienza dell’auto infermieristica di Reggio Emilia", "R. Guatteri");
                    print_relation("Guzzinati.pdf", "Tavola rotonda: “Ventiliamo dunque siamo...“ gestione del pz in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto", "I. Guzzinati");
                    print_relation("Lupacciolu.pdf", "Approccio razionale nel pz con insufficienza respiratoria acuta dall’extra all’intraospedaliero", "S. Lupacciolu");
                    print_relation("Minelli.pdf", "Tavola rotonda: « ventiliamo dunque siamo...» gestione del paz in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto", "E. Minelli");
                    print_relation("Morelli.pdf", "Ventilazioni in MURG – Ravenna", "E. Morelli");
                    print_relation("Morselli.pdf", "Quando a casa manca il respiro", "C. Morselli");
                    print_relation("Perin.pdf", "Tavola rotonda: “ventiliamo dunque siamo...” gestione del paziente in NIV in Medicina d’Urgenza: diversi modelli organizzativi a confronto", "T. Perin");
                    print_relation("Polito.pdf", "Quale stabilizzazione possibile in un PS SPOKE prima del trasferimento ad un PS HUB", "J. Polito");
                    print_relation("Previati.pdf", "Insufficienza respiratoria acuta in età pediatrica l’intervento del medico d’urgenza", "R. Previati");
                    print_relation("Serra1.pdf", "Quale addestramento al lavoro in team nel percorso formativo degli specializzandi in medicina d’emergenza urgenza: una valutazione del CoSMEU", "E. Serra");
                    print_relation("Serra2.pdf", "Standardizzazione della Comunicazione tra Centri Spoke e Hub nella Centralizzazione e Decentralizzazione delle patologie tempo dipendenti", "E. Serra");
                    print_relation("Smorgon.pdf", "Gestione avanzata delle vie aeree in emergenza", "C. Smorgon");
                    print_relation("Trazzi.pdf", "Ruolo dell’Infermiere nel Trauma Team in extra-ospedaliero", "A. Trazzi");
                    print_relation("Vitali.pdf", "Tavola rotonda: Ventiliamo quindi siamo... La gestione del paziente in NIV in Medicina d’ Urgenza diversi modelli rganizzativi a confronto", "E. Vitali");
                    print_relation("Zecchi.pdf", "La gestione extra ed intraospedaliera di un trauma grave nell’HUB: sistema organizzativo e dati dell’OCSAE di Baggiovara", "G. Zecchi");

                    break;

                case '416':

                    print_relation("Bartolesi.pdf", "Patogeni emergenti nelle infezioni urinarie complicate", "A. M. Bartolesi");
                    print_relation("Cattaneo.pdf", "La terapia antibiotica nel paziente con insufficienza renale: confronto con il nefrologo...", "D. Cattaneo");
                    print_relation("Giuri.pdf", "Anamnesi Patologica Remota", "G. Giuri");
                    print_relation("LaManna.pdf", "Infezioni nel trapianto di rene: aspetti epidemiologici, clinici e terapeutici", "G. La Manna");
                    print_relation("Mandreoli.pdf", "La terapia antibiotica nel paziente con insufficienza renale: farmacologo e nefrologo a confronto", "M. Mandreoli");
                    print_relation("Matteelli.pdf", "La tubercolosi urinaria", "A. Matteelli");
                    print_relation("Nalesso.pdf", "Riconoscimento precoce e gestione clinica della sepsi urinaria: il ruolo del nefrologo", "F. Nalesso");
                    print_relation("Prati.pdf", "La Chirurgia Urologica nelle infezione complicate delle vie urinarie", "A. Prati");
                    print_relation("Tascini.pdf", "Micosi urinaria: come discriminare tra patologia e colonizzazione e indicazioni sull’impiego degli antifungini sistemici. ", "C. Tascini");
                    print_relation("Tumietto.pdf", "Aspetti clinici e nuove opportunità terapeutiche", "F. Tumietto");
                    print_relation("Volta.pdf", "Riconoscimento precoce e gestione clinica della sepsi urinaria: ruolo dell’intensivista", "C. A. Volta");

                    break;

                case '400':

                    print_relation("Bassi.pdf", "La preparazione ottimale nella colonscopia", "   R. Bassi");
                    print_relation("Bruni.pdf", "Sedazione e Sicurezza: il monitoraggio del paziente", "A. Bruni");
                    print_relation("Graziadio.pdf", "Preparazione e gestione del paziente a breve termine", "M. Graziadio");
                    print_relation("Inca.pdf", "Il percorso della terapia nelle IBD", "   R. D'Incà");
                    print_relation("Matarese.pdf", "Come affrontare e diluire l’ansia", "   G. C. Matarese");
                    print_relation("Merighi.pdf", "Sedazione e Sicurezza: il monitoraggio del paziente", "A. Merighi, A. Bruni");
                    print_relation("Triossi.pdf", "La colonscopia di qualità nell’epoca dello screening. La preparazione ottimale", "O. Triossi");
                    print_relation("Tumedei.pdf", "IBD UNIT: L'esperienza forlivese del «CASE MANAGEMENT». Verso un modello gestionale multiprofessional", "D. Tumedei");
                    print_relation("Valpiani.pdf", "IBD Unit: modello multidisciplinare e multiprofessionale in continua evoluzione", "D. Valpiani");
                    print_relation("Zanotto.pdf", "Obiettivo paziente", "V. Zanotto");

                    break;

                case '399':

                    print_relation("Costantino.pdf", "Acido alfa - lipoico vs progesterone nella minaccia di aborto: quali evidenze ? ", "D. Costantino");
                    print_relation("Costantino2.pdf", "Preliminari di uno studio ala e diabete gestazionale", "D. Costantino");
                    print_relation("DeSeta.pdf", "Contraccezione e via vaginale", "F. De Seta");
                    print_relation("Fruzzetti.pdf", "Contraccettivo orale a regime prolungato e benefici terapeutici dalla riduzione della frequenza mestruale", "F. Fruzzetti");
                    print_relation("Guaraldi.pdf", "Ipercontrattilità uterina", "C. Guaraldi");
                    print_relation("Guaraldi2.pdf", "Ruolo del ginecologo nel trattamento delle patologie mammarie benigne", "C. Guaraldi");
                    print_relation("Neri.pdf", "Ruolo della L - Arginina nella prevenzione e nel trattamento della preeclampsia", "I. Neri");
                    print_relation("Paoletti.pdf", "Nuove opportunità in contraccezione: l’associazione EE e GSD per via transdermica", "A. M. Paoletti");
                    print_relation("Zannoni.pdf", "L’evoluzione della terapia medica nel trattamento dell’endometriosi", "L. Zannoni");

                    break;

                case '398':

                    print_relation("Bartoloni.pdf", "Infezioni correlate ai viaggi e Arbovirosi", "A. Bartoloni");
                    print_relation("Cacopardo.pdf", "MST: i dati della Sicilia", "B. Cacopardo");
                    print_relation("Calleri.pdf", "Chemioprofilassi antimalarica", "G. Calleri");
                    print_relation("Chirianni.pdf", "HIV / AIDS: attualità epidemiologiche e prospettive terapeutiche nella popolazione migrante in Italia", "A. Chirianni");
                    print_relation("DiNuzzo.pdf", "Tubercolosi extrapolmonare nella popolazione straniera a Ferrara", "M. C. Di Nuzzo");
                    print_relation("Geraci.pdf", "Rifugiati e Migranti: implicazioni sociali e sanitarie", "S. Geraci");
                    print_relation("Goletti.pdf", "Tubercolosi e Screening tra i Migranti", "D. Goletti");
                    print_relation("Mazzariol.pdf", "Movimenti migratori e resistenza agli antibiotic", "A. M. Mazzariol");
                    print_relation("Mazzocco.pdf", "Correlazione tra TB e Malnutrizione Acuta Severa nei pazienti pediatrici", "M. Mazzocco");
                    print_relation("Prestileo.pdf", "L’insegnamento di Lampedusa: il risvolto della medaglia", "T. Prestileo");
                    print_relation("Puoti.pdf", "Strategie terapeutiche attuali nel migrante con epatite virale", "M. Puoti");
                    print_relation("Segala.pdf", "Patologie infettive tra i migranti rifugiati e richiedenti asilo nella provincia di Ferrara", "D. Segala");

                    break;

                case '397':

                    print_relation("Pan.pdf", "Clostridium difficile: terapia eziologica e preventiva", "A. Pan");
                    print_relation("Sarti.pdf", "I dati epidemiologici a supporto delle strategie di Antimicrobial Stewardship", "M. Sarti");
                    print_relation("Viale.pdf", "Fast microbiology: Applicazioni cliniche", "P. Viale");

                    break;

                case '388':

                    print_relation("Andreati.pdf", "Aspetti di Genere in Geriatria", "   C. Andreati");
                    print_relation("Bagnaresi.pdf", "Espressioni di genere nell’arte moderna", "I. Bagnaresi");
                    print_relation("Gallerani.pdf", "Sepsi e differenze di genere", "   M. Gallerani");
                    print_relation("Loss.pdf", "Salute e genere nella cultura infermieristica", "C. Loss");
                    print_relation("Manfredini.pdf", "ardiomiopatia Tako - Tsubo: solo una malattia della donna ? ", "R. Manfredini");
                    print_relation("Montanari.pdf", "Laboratorio, appropriatezza e genere", "E. Montanari");
                    print_relation("Sassone.pdf", "Morte improvvisa: differenze o disparità di genere", "B. Sassone");
                    print_relation("Signani.pdf", "Un caso di andragogia: La depressione dimenticata degli uomini", "F. Signani");
                    print_relation("Storari.pdf", "Aspetti di genere in nefrologia", "   A. Storari");
                    print_relation("Zoli.pdf", "DM. I. C. I. Esistono differenze di Genere ? ", "G.     Zoli");
                    print_relation("Zucchi.pdf", "‘Gufo’ o ‘Allodola’: cronotipo e implicazioni di genere", "B. Zucchi");

                    break;

                case '383':

                    print_relation("Bonati.pdf", "La gestione del paziente anziano comorbido: rete delle cronicità o presa in carico globale nella rete dei servizi ? ", "P. A.     Bonati");
                    print_relation("Fabbo.pdf", "Reablement nella continuità assistenziale e nelle dimissioni protette", "A. Fabbo");
                    print_relation("Ferrari.pdf", "Il Paziente Anziano in Ospedale : I Vantaggi di un approccio dedicato alla luce delle evidenze disponibili", "A. Ferrari");
                    print_relation("Martini.pdf", "Il delirium post operatorio nell'anziano", "   E. Martini");
                    print_relation("Romagnoni.pdf", "Il problema dei ricoveri da C.R.A.", "F. Romagnoni");
                    print_relation("Vanelli.pdf", "MoCA: studio normativo italiano e applicazione clinica nella diagnosi di MCI", "M. Vanelli Coralli");
                    print_relation("Volpato.pdf", "Diagnosi e Trattamento della Sarcopenia nell’Anziano", "S. Volpato");

                    break;

                case '382':

                    print_relation("Bigoni.pdf", "Emoglobinopatie: screening e gestione clinica", "S. Bigoni");
                    print_relation("Capucci.pdf", "Gravida obesa: quali rischi materni e fetali?", "R. Capucci");
                    print_relation("Costantino.pdf", "Ruolo degli integratori nel controllo delle alterazioni del metabolismo glucidico in gravidanza", "D. Costantino");
                    print_relation("Costantino_1.pdf", "Fisiologia della tiroide", "D. Costantino");
                    print_relation("Furicchia.pdf", "Emorragia postpartum: gestione multidisciplinare", "G. Furicchia");
                    print_relation("Graziani.pdf", "Il diabete pregestazionale", "R. Graziani");
                    print_relation("Greco.pdf", "Prevalence of congenital abnormalities among offsprings of pregestational diabetic mothers", "P. Greco");
                    print_relation("Gregoratti.pdf", "La dieta nel diabete pregravidico e gestazionale", "G. Gregoratti");
                    print_relation("Guaraldi.pdf", "Il diabete gestazionale: management ostetrico", "C. Guaraldi");
                    print_relation("Guaraldi_1.pdf", "Le anemie in gravidanza: meglio prevenirle", "C. Guaraldi");
                    print_relation("Manzato.pdf", "Disturbo da alimentazione incontrollata e diabete gestazionale", "E. Manzato");
                    print_relation("Mello.pdf", "Lo screening del diabete gestazionale: dall’HAPO Study alle nuove linee guida del 2011. Il punto della situazione", "G. Mello, S. Ottanelli");

                    break;

                case '381':

                    echo "</ul>";
                    echo "<h4> Corso di aggiornamento: indicazioni, materiali e tecniche nella terapia compressiva e nel drenaggiolinfatico manuale</h4>";
                    echo "<ul>";

                    print_relation("gio/Boemia.pdf", "Tecniche di D.L.M. e risultati clinici", "K. Boemia");
                    print_relation("gio/Gazzabin.pdf", "Materiali per la compressione e loro criteri di scelta", "L. Gazzabin");
                    print_relation("gio/Ligas.pdf", "Bendaggio multistrato: vantaggi e limiti", "B. M. Ligas");
                    print_relation("gio/Moneta.pdf", "Terapia decongestionante: il management clinico", "G. Moneta");
                    print_relation("gio/Oliva.pdf", "La pressoterapia: come e quando", "   E. Oliva");

                    echo "</ul>";
                    echo "<h4> Congresso Nazionale del Trentennale SIFL </h4>";
                    echo "<ul>";

                    print_relation("ven/Alonzo.pdf", "Trombosi della vena mesenterica superiore", "U. Alonzo");
                    print_relation("ven/Aluigi.pdf", "Trombosi venosa degli arti superiori e intraddominali", "L. Aluigi");
                    print_relation("ven/Annoni.pdf", "Lo studio eco color doppler e il mappaggio emodinamico in corso di malattia venosa cronica", "F. Annoni");
                    print_relation("ven/Arpaia.pdf", "La Recidiva di TVP: Aspetti Clinici", "G. Arpaia");
                    print_relation("ven/Benedetti.pdf", "Durata della terapia anticoagulante del tev", "I. Benedetti");
                    print_relation("ven/Boschetti.pdf", "Trattamento del reflusso safenico mediante tecnica MOCA: indicazioni, risultati ed esperienza personale", "L. Boschetti");
                    print_relation("ven/Caggiati.pdf", "Anatomia normale e varianti della SFJ e della SPJ: ricadute pratiche cliniche", "A. Caggiati");
                    print_relation("ven/Campana.pdf", "Scleromousse degli assi safenici", "F. Campana");
                    print_relation("ven/Camporese.pdf", "Il trombo residuo: mito o realtà?", " G. Camporese");
                    print_relation("ven/Cestari.pdf", "Cosa evitare nel pdta del linfedema", " M. Cestari");
                    print_relation("ven/Cimminiello.pdf", "Indicazioni all’ASA dopo la TAO", " C. Cimminiello");
                    print_relation("ven/Corcos.pdf", "Le recidive della regione (Safeno)Poplitea: come diagnosticarle e quale terapia EBM", "L. Corcos");
                    print_relation("ven/Coscia.pdf", "Fluid Dynamical modeling: Clinical-Practical Aspects", "V. Coscia");
                    print_relation("ven/Danese.pdf", "Procedure flebologiche ambulatoriali o in day-surgery: aspetti medico legali", "M. Danese");
                    print_relation("ven/DelGuercio.pdf", "Flebopatie e lea: luci ed ombre", "M. Del Guercio");
                    print_relation("ven/DiPaola.pdf", "Il remboursement delle flebopatie nelle varie realtà italiane", "G. Di Paola");
                    print_relation("ven/Ferrini.pdf", "Procedure endovascolari: i risultati della letteratura internazionale", "M. Ferrini");
                    print_relation("ven/Fonti.pdf", "Tecniche endovascolari in DS: sempre possibili?", "M. Fonti");
                    print_relation("ven/Franco.pdf", "La scelta della crossectomia: quando e come", "E. Franco");
                    print_relation("ven/Galeotti.pdf", "La diagnostica per immagini", " R. Galeotti");
                    print_relation("ven/Ghirarduzzi.pdf", "Le complicanze in corso di NAO e TAO", "A. Ghirarduzzi");
                    print_relation("ven/Izzo.pdf", "Modificazioni cliniche e tessutali degli stadi avanzati dell’ivc", "M. Izzo");
                    print_relation("ven/Lugli.pdf", "Ruolo della chirurgia open ed endovascolare", "M. Lugli");
                    print_relation("ven/Mollo.pdf", "Trombosi venosa degli arti superiori e intraddominali", "P. E. Mollo");
                    print_relation("ven / Monaco.pdf", "L'infiammazione al cuore della MVC: aspetti clinici e ruolo della strategia terapeutica", "S. Moncao");
                    print_relation("ven / Pacelli.pdf", "Trattamento scleroterapico delle recidive di cross", "W. Pacelli");
                    print_relation("ven / Pola.pdf", "Genetica e Malattia Venosa Cronica", "R. Pola");
                    print_relation("ven / Polichetti.pdf", "Criteri di scelta nei percorsi del paziente flebopatico", "R. Polichetti");
                    print_relation("ven / Ricci.pdf", "Terapia medica, fisica riabilitativa e chirurgica: come e quando integrarle", "M. Ricci");
                    print_relation("ven / Rocca.pdf", "Ruolo della fibrinolisi nella TVP prossimale", "T. Rocca");
                    print_relation("ven / Serantoni.pdf", "Inquadramento fisiopatologico e aspetti clinici dell’ulcera venosa", "S. Serantoni");
                    print_relation("ven / Sozio.pdf", "Standard per l’accreditamento dei centri flebologici", "G. Sozio");
                    print_relation("ven / Traina.pdf", "Sindrome di Paget - Schroetter", " L. Traina");
                    print_relation("ven / Zamboni.pdf", "La chirurgia della Sdr Post - Trombotica. Il contributo italiano ad una sfida", "P. Zamboni");
                    echo "</ul>";

                    echo "<h4>Corso di aggiornamento: la diagnostica clinica non invasiva nelle flebopatie</h4>";
                    echo "<ul>";
                    print_relation("sab / Ermini.pdf", "La Cartografia", "S. Ermini");
                    print_relation("sab / Izzo.pdf", "La diagnostica tessutale in corso di i. v. c. ", "M. Izzo");
                    print_relation("sab / Medini.pdf", "Prove semeiologiche in corso di esame non invasivo per lo studio del sistema venoso degli arti inferiori", "C. Medini");

                    break;

                case '380':

                    print_relation("Aliberti.pdf", "Emorragie del tubo digerente: Imaging e Radiologia Interventistica", "C. Aliberti");
                    print_relation("Gagliano.pdf", "Patologia neoplastica dell’intestino tenue", "M. Gagliano, S. Sala");
                    print_relation("Malaventura.pdf", "Patologia del tubo digerente in età pediatrica: inquadramento clinico", "C. Malaventura");
                    print_relation("Miele.pdf", "Traumi intestinali e mesenterici", " V. Miele");
                    print_relation("Monteduro.pdf", "Le Ostruzioni meccaniche del tubo Digerente", "F. Monteduro");
                    print_relation("Rizzati.pdf", "Patologia infiammatoria acuta", "R. Rizzati, S. Leprotti");
                    print_relation("Tilli.pdf", "Patologia ischemica ileo - colica", "M. Tilli e M. T. Cannizzaro");

                    break;

                case '378':

                    print_relation("Baldi.pdf", "Lo screening con ricerca del DNA fetale su sangue materno", "M. Baldi");
                    print_relation("Costantino.pdf", "I Folati: quello che le gravide sanno e quello che dovrebbero sapere", "D. Costantino");
                    print_relation("Costantino_1.pdf", "Il danno perinale da parto e post isterectomia", "D. Costantino");
                    print_relation("Costantino_2.pdf", "Il test combinato, ancora attuale ? ", "D. Costantino");
                    print_relation("Guaraldi.pdf", "Classificazione e diagnosi delle disfunzioni perinali", "C. Guaraldi");
                    print_relation("Guaraldi_2.pdf", "L'integrazione come supporto nella patologia del pavimento pelvico", "C. Guaraldi");
                    print_relation("Tess.pdf", "Valutazione uro - ginecologica ed esame obiettivo: la valutazione funzione del pavimento pelvico", "M. Tess");
                    print_relation("Toselli.pdf", "Pavimento pelvico: prevenzione, cura e riabilitazione", "S. Toselli");

                    break;

                case '376':

                    print_relation("Antonelli_Libanore.pdf", "Il contributo della Farmacologia clinica", "T. Antonelli, M. Libanore");
                    print_relation("Catena.pdf", "WSES Guidelines e Farmacoeconomia in chirurgia addominale", "F. Catena");
                    print_relation("Galeotti.pdf", "Imaging delle infezioni intraddominali: approccio interventistico", "R. Galeotti");
                    print_relation("Giuri.pdf", "Terapia empirica e raccomandazioni WSES nelle realtà locali: aspetti organizzativi, chirurgici e infettivologici", "G. Giuri");
                    print_relation("Govoni.pdf", "Imola: un’esperienza di gestione integrata", "Govoni");
                    print_relation("Malagodi.pdf", "Antibiotico profilassi perioperatoria", "M. Malagodi");
                    print_relation("Occhionorelli.pdf", "Gestione Multidisciplinare della Complessità delle Infezioni Endoaddominali", "S. Occhionorelli");
                    print_relation("Righini.pdf", "Sepsi addominale: approccio intensivistico", "E. Righini, M. Malagodi");
                    print_relation("Rossi.pdf", "Patogeni emergenti: impatto epidemiologico", " M. R. Rossi");
                    print_relation("Sarti.pdf", "ERCP and KPC and others CPE", "M. Sarti");
                    print_relation("Viale.pdf", "Infezioni in chirurgia: nuove acquisizioni per nuove indicazioni", "P. Viale");
                    print_relation("Volta.pdf", "Sepsi addominale: approccio intensivistico", " C. A. Volta");
                    break;

                case '374':

                    print_relation("Zamboni_Rossi.pdf", "Malnutrizione e Sarcopenia", " M. Zamboni, A. Rossi");

                    break;

                case '363':

                    print_relation("Bettoli.pdf", "La corretta esecuzione di indagini di laboratorio: in Dermatologia e Allergologia", "V. Bettoli");
                    print_relation("Camerotto.pdf", "Ermete, la conoscenza nel momento esatto della prescrizione", "A. Camerotto");
                    print_relation("Fiumana.pdf", "La corretta esecuzione delle indagini di laboratorio nel bambino", "E. Fiumana");
                    print_relation("Gallerani.pdf", "Attivazione in Az. Ospedaliero Universitaria", "G. Gallerani");
                    print_relation("Guerra.pdf", "Problematiche delle interfacce con i diversi data entry", "G. Guerra");
                    print_relation("Montanari.pdf", "Base per l’appropriatezza", "E. Montanari");
                    print_relation("Pelizzola.pdf", "La corretta ripetizione degli esami di Laboratorio", "D. Pelizzola");
                    print_relation("Toniutti.pdf", "La corretta ripetizione degli esami di Laboratorio: problematiche tecnico informatiche", "A. Toniutti");
                    print_relation("Trenti.pdf", "Esperienza del Dipartimento di Medicina di Laboratorio di Modena nel governo dell'appropriatezza diagnostica degli esami di laboratorio", "     T. Trenti");
                    print_relation("Vagnoni.pdf", "I risvolti economici", "E. Vagnoni");
                    print_relation("Zamboni.pdf", "Appropriatezza degli esami di laboratorio", "A. Zamboni");

                    break;

                case '362':

                    print_relation("Costantino.pdf", "Lattoferrina come agente terapeutico delle infenzioni urogenitali", "D. Costantino");
                    print_relation("Dante.pdf", "L'ecosistema vaginale: dalla pubertà alla menopausa", "G. Dante");
                    print_relation("Fanti.pdf", "Terapie convenzionali delle vulvovaginiti", "S. Fanti");
                    print_relation("Guaraldi.pdf", "Fermenti lattici tindalizzati: nuove opportunità terapeutiche nel trattamento delle vaginiti", "C. Guaraldi");
                    print_relation("Murina.pdf", "Contraccezione ormonale, ecosistema vaginale ed esposizione alle infezioni del basso tratto uro - genitale femminile", "F. Murina");
                    print_relation("Sileo.pdf", "Infezioni delle basse vie urinarie in gravidanza: problematiche materne ed embrio - fetali", "F. Sileo");
                    print_relation("Tumietto.pdf", "Urinocoltura, antibiogramma, antibioticoterapia, antibiotico resistenza: riflessioni", "F. Tumietto");

                    break;

                case '359':

                    print_relation("Bonaccorsi.pdf", "La Cimicifuga Racemosa nel trattamento della sindrome climaterica", "G. Bonaccorsi");
                    print_relation("Costantino.pdf", "Nutraceutica e poliabortività", "D. Costantino");
                    print_relation("Fila.pdf", "Sindrome genito - urinaria in postmenopausa: approccio terapeutico non ormonale. ", "E. Fila");
                    print_relation("Guaraldi.pdf", "Fitoterapici e nutraceutici: definizione e claims di salute", "C. Guaraldi");
                    print_relation("Marci.pdf", "Alimentazione, nutraceutica ed infertilità femminile", "R. Marci e C. Borghi");

                    break;

                case '357':

                    print_relation("Bassi.pdf", "Meningite da pneumococco resistente alla penicillina", "P. Bassi");
                    print_relation("Biagetti.pdf", "Sepsi da KPC a partenza dalle vie biliari: case report e spunti gestionali", "C. Biagetti");
                    print_relation("Calzetti.pdf", "Empiema della colecisti da enterobatteri ESBL positivi", "C. Calzetti");
                    print_relation("Cancellieri.pdf", "Caso clinico II: Infezione di tasca di Pace     – maker da MRSA con MIC > 1 per vancomicina", "C. Cancellieri");
                    print_relation("Codeluppi.pdf", "Sepsi Da VRE in trapianto", "M. Codeluppi");
                    print_relation("Cultrera.pdf", "Casi difficili in patologia infettiva su un caso di infenzione di device cardiaco", "R. Cultrera");
                    print_relation("Giuri.pdf", "Infezione di tasca di Pace – maker da MRSA con MIC > 1 per vancomicina", "G. Giuri");
                    print_relation("Libanore.pdf", "La resistenza agli antibiotici : un problema globale", "M. Libanore");
                    print_relation("Pantaleoni.pdf", "Caso clinico III: Meningite da Streptococco pneumoniae PR", "M. Pantaleoni");
                    print_relation("Sambri.pdf", "Appropriatezza e tempestività nella diagnostica microbiologica", "V. Sambri");
                    print_relation("Sarti.pdf", "Appropriatezza e tempestività nella diagnostica microbiologica", "M. Sarti");
                    print_relation("Sighinolfi.pdf", "Sepsi addominale da KPC in paziente con AIDS", "L. Sighinolfi");
                    print_relation("Viale.pdf", "Linee guida: valori e limiti", "P. Viale");

                    break;

                case '355':

                    print_relation("Amenta.pdf", "Colina alfoscerato nella terapia del deterioramento cognitivo dell'anziano. ", "F. Amenta");
                    print_relation("Bonetti.pdf", "La terapia multifattoriale nel deterioramento cognitivo dell'anziano: quali evidenze ? ", "F. Bonetti");
                    print_relation("Cervellati.pdf", "Stress ossidativo e deterioramento cognitivo dell'anziano. ", "C. Cervellati");
                    print_relation("De_Carolis.pdf", "L'acetilcarnitina e trattamento della demenza senile", "S. De Carolis");
                    print_relation("De_Vreese.pdf", "Gingko Biloba: tra mito e realtà. ", "L. De Vreese");
                    print_relation("Lucchetti.pdf", "Alimenti(AFSM) e neuroinfiammazione: la palmitoiletanolamide. ", "L. Lucchetti");
                    print_relation("Menozzi.pdf", "Omotaurina(tramiprosato) nella malattia di Alzheimer. ", "L. Menozzi");
                    print_relation("Perotta.pdf", "Dieta e Medical Food nel declino cognitivo lieve. ", "D. Perotta");
                    print_relation("Trabucchi.pdf", "Il deterioramento cognitivo e la pluralità delle sue determinanti. ", "M. Trabucchi");
                    print_relation("Zuliani.pdf", "Alzheimer's disease drug - development pipeline: few candidates, frequent failures. ", "G. Zuliani");
                    print_relation("Zurlo.pdf", "Vitamine Liposolubili e Declino Cognitivo: quale ruolo per Vitamina E e Vitamina D ? ", "A. Zurlo");

                    break;

                case '352':

                    print_relation("Caselli.ppt", "Gut microflora have to be modernly considered a “micromial organ” placed within a host organ. ", "M. Caselli");
                    print_relation("Mazzella.pptx", "Epidemiologia della malattie di fegato", "G. Mazzella");
                    print_relation("Stockbrugger.ppt", "Irritable Bowel Syndrome. A functional or an organic condition?", "R. Stockbrugger");
                    print_relation("Zagari.ppt", "Epidemiologia della malattia da reflusso gastro - esofageo: una prospettiva globale", "R. Zagari");

                    break;

                case '350':

                    print_relation("Caselli.pdf", "Diagnosis non invasive tests", "M. Caselli");
                    print_relation("Figura.pdf", "Un nuovo metodo diagnostico per la rilevazione di helicobacter pylori", "N. Figura");
                    print_relation("Figura2.pdf", "Nuove composizioni per il trattamento dell’infezione da helicobacter pylori", "N. Figura");
                    print_relation("LaCorte.pdf", "Consensus H pylori management", "R. LaCorte");
                    print_relation("Zagari.pdf", "Diagnosi", "R. M. Zagari");
                    print_relation("Zagari1.pdf", "Preliminary speech", "R. M. Zagari");
                    print_relation("Zagari2.pdf", "Questioni aperte", "R. M. Zagari");
                    print_relation("Zagari3.pdf", "Problema delle resistenze batteriche", "R. M. Zagari");

                    break;

                case '347':

                    print_relation("Cavazza.pdf", "Il significato della valutazione individuale nelle Aziende Sanitarie", "M. Cavazza");
                    print_relation("Laus.pdf", "La valutazione individuale del Medico Ospedaliero: l’esperienza del Policlinico S. Orsola - Malpighi", "M. Laus");
                    print_relation("Pizzoferrato.pdf", "Il quadro legislativo e contrattuale", "A. Pizzoferrato");
                    print_relation("Rinaldi.pdf", "Gli incarichi dirigenziali: dalla amministrazione del Personale alla gestione strategica delle Risorse Umane", "G. Rinaldi");
                    print_relation("Saltari.pdf", "Sistema di valutazione del personale dirigente ASL Ferrara", "P. Saltari");
                    print_relation("Sarchielli.pdf", "Il sistema premiante: effetti personali e collettivi della valorizzazione differenziale in base al merito professionale", "G. Sarchielli");
                    print_relation("Zoppellari.pdf", "La valutazione individuale dei medici ospedalieri", "R. Zoppellari");

                    break;

                case '329':

                    print_relation("Costantino2.pdf", "Test combinato: è ancora attuale ? ", "D. Costantino");
                    print_relation("Costantino.pdf", "La nuova dimensione della contraccezione", "D. Costantino");
                    print_relation("Giugliano.pdf", "Approccio contraccettivo nell’iperandrogenismo", "E. Giugliano");
                    print_relation("Graziano.pdf", "Integratori in gravidanza: Lusso o Necessità ? ", "A. Graziano");
                    print_relation("Guaraldi.pdf", "Quando l'allattamento diventa contraccezione: allattamento e contraccezione efficace", "C. Guaraldi");
                    print_relation("LoMonte.pdf", "Screening per GBS: utilità e controversie", "G. Lo Monte");
                    print_relation("Moreno.pdf", "Ruolo dell’ostetrica nella gestione della gravidanza fisiologica", "A. Moreno");
                    print_relation("Pagliarusco.pdf", "Il danno perineale da parto: definizione e prevenzione", "G. Pagliarusco");
                    print_relation("Segato.pdf", "Il trattamento delle infezioni da candida in ostetricia e ginecologia nella pratica clinica", "M. Segato");
                    print_relation("Tess.pdf", "Rieducazione e riabilitazione del pavimento pelvico: dalla valutazione del danno al primo approccio riabilitativo", "M. Tess");
                    print_relation("Zanella.pdf", "Contraccezione d'emergenza e responsabilità del medico", "C. Zanella");

                    break;

                case '328':

                    print_relation("Bandiera.pdf", "Ruolo dell’ostetrica nella gestione della gravidanza fisiologica", "E. Bandiera");
                    print_relation("Bianchini.pdf", "Il danno perineale da parto: definizione e prevenzione", "G. Bianchini");
                    print_relation("Costantino.pdf", "Test combinato: è ancora attuale ? ", "D. Costantino");
                    print_relation("Costantino2.pdf", "La nuova dimensione della contraccezione", "D. Costantino");
                    print_relation("Giugliano.pdf", "Approccio contraccettivo nell’iperandrogenismo", "E. Giugliano");
                    print_relation("Graziano.pdf", "Integratori in gravidanza: Lusso o Necessità ? ", "A. Graziano");
                    print_relation("Guaraldi.pdf", "Quando l'allattamento diventa contraccezione: allattamento e contraccezione efficace", "C. Guaraldi");
                    print_relation("Mossuto.pdf", "Eccessivo incrememnto ponderale in gravidanza: rischi materno - fetali", "E. Mossuto");
                    print_relation("Toselli.pdf", "Pavimento Pelvico: Prevenzione Cura e Riabilitazione", "S. Toselli");
                    print_relation("Vesce.pdf", "Diagnosi prenatale: stato dell'arte", "F. Vesce");
                    print_relation("Zanella.pdf", "Contraccezione d'emergenza e responsabilità del medico", "C. Zanella");

                    break;

                case '327':

                    print_relation("Carafelli.pdf", "Un caffè dalla Regione: politiche a sostegno delle attività a bassa soglia", "A. Carafelli");
                    print_relation("Cristofori.pdf", "L’esperienza di Portomaggiore: Ri - troviamoci al cafè", "R. Cristofori, I. Pedriali, P. Veronesi");
                    print_relation("Tola.pdf", "Il progetto locale: l’accordo di programma e le attività correlate", "M. R. Tola");
                    print_relation("Tulipani.pdf", "L’esperienza di Cento: la fatica di ripartire", "C. Tulipani");
                    print_relation("Veronesi.pdf", "L’evoluzione di una storia", "P. Veronesi");

                    break;

                case '324':

                    print_relation("Ambrosio.pdf", "PHPT", "M. R. Ambrosi");
                    print_relation("Balsamo.pdf", "Deficit di 21 idrossilasi", "A. Balsamo, A. Gambineri");
                    print_relation("Bondanelli.pdf", "Novità Farmacologiche in Endocrinologia", "M. Bondanelli");
                    print_relation("Buci.pdf", "Nuove formulazioni di levo - tiroxina", "L. Buci");
                    print_relation("Corona.pdf", "Diagnosi e terapia del deficit di GH in età pediatrica", "G. Corona e D. Ribichini");
                    print_relation("Faustini.pdf", "Tolvaptan", "M. FaustiniFustini");
                    print_relation("Madeo.pdf", "Denosumab", "B. Madeo");
                    print_relation("Minoia.pdf", "Pasireotide", "M. Minoia");
                    print_relation("Nizzoli.pdf", "Quali farmaci dopo la metformina ? ", "M. Nizzoli");
                    print_relation("Pellicano.pdf", "La scelta del farmaco: valutazione costo beneficio", "F. Pellicano");
                    print_relation("Tomasi.pdf", "Target glicemici personalizzati ? ", "F. Tomasi");
                    print_relation("Trimarchi.pdf", "Tiroide e gravidanza(nutrizione iodica)", "F. Trimarchi");
                    print_relation("Zatelli.pdf", "Nodulo tiroideo in paziente gravida", "M. C. Zatelli");
                    print_relation("Zavaroni.pdf", "La riduzione del rischio cardiovascolare principale obiettivo della terapia", "I. Zavaroni");

                    break;

                case '323':

                    print_relation("Feo.pdf", "Chirurgia per i Polipi del Colon", "C. Feo");
                    print_relation("Ravenna.pdf", "Malattia da reflusso gastroesofageo", "F. Ravenna");

                    break;

                case '315':

                    print_relation("Costantino.pdf", "Nuovi approcci terapeutici per il controllo delle alterazioni del metabolismo glucidico", "D. Costantino");
                    print_relation("D_Ambrosio.pdf", "Counseling psicologico per la gestione del peso e dell'obesità", "F. D'Ambrosio");
                    print_relation("D_Ambrosio2.pdf", "L'approccio psicologico nella paziente diabetica", "F. D'Ambrosio");
                    print_relation("Guaraldi.pdf", "Diabete gestazionale ed infezioni del tratto genito - urinario: nuove opportunità terapeutiche", "C. Guaraldi");
                    print_relation("Lanzoni.pdf", "L'insulino - resistenza nelle diverse fasi della vita della donna", "C. Lanzoni");
                    print_relation("Marci.pdf", "Modalità e timing del parto", "R. Marci");
                    print_relation("Nedea.pdf", "Alimentazione, gravidanza e migrazione", "D. Nedea");
                    print_relation("Ultori.pdf", "Il ruolo dell’ecografia nella sorveglianza fetale", "E. Ultori");

                    break;

                case '298':

                    print_relation("Ambrosio.pdf", "Inquadramento diagnostico dei tumori neuroendocrini del pancreas", "M. R. Ambrosio ");
                    print_relation("Ansaldo.pdf", "Stato dell’arte nel trattamento chirurgico del carcinoma differenziato della tiroide", "G. Ansaldo");
                    print_relation("Bondanelli.pdf", "Inquadramento Clinico dell’Incidentaloma Surrenalico", "M. Bondanelli");
                    print_relation("Cavazzini.pdf", "Relazione", "L. Cavazzini");
                    print_relation("Feo.pdf", "Surrenectomia Laparoscopica", "C. Feo");
                    print_relation("Franceschetti.pdf", "Deficit di vitamina d e iperaparatiroidismo secondario", "P. Franceschetti");
                    print_relation("Gianotti.pdf", "Applicazioni cliniche dei calciomimetici", "L. Gianotti");
                    print_relation("Guerra.pdf", "Dosaggio intraoperatorio del PTH", "G. Guerra");
                    print_relation("Trasforini.pdf", "Nodulo tiroideo ed ecografia", "G. Trasforini");
                    print_relation("Verdecchia.pdf", "AJCC / WHO / UICC 2010", "G. Verdecchia");
                    print_relation("Zaccaroni.pdf", "Pianificazione dell’intervento chirurgico: indicazioni, localizzazione e tecniche", "A. Zaccaroni");
                    print_relation("Zatelli.pdf", "Il percorso diagnostico del nodulo tiroideo: il ruolo dell’analisi molecolare", "M. C. Zatelli");

                    break;

                case '297':

                    print_relation("Bozzolani.pdf", "Malattia Tromboembolica - Luci e ombre", "G. Bozzolani");
                    print_relation("Cavalli.pdf", "Il Problema del Ricovero - La Bed Capacity", "M. Cavalli");
                    print_relation("Cesari.pdf", "Il reparto “polmone”, in quali mani ? L’eperienza della Azienda USL di Bologna: la Medicina F", "C. Cesari");
                    print_relation("Dall_Olmi.pdf", "Bed management strategie e buone pratiche", "E. Dall'Olmi");
                    print_relation("De_Pietri.pdf", "Score prognostici nell’emorragia digestiva superiore non varicosa: percorso assistenziale in una rete ospedaliera provinciale", "S. DePietri");
                    print_relation("Farinatti.pdf", "Score polmoniti: casi clinici", "M. Farinatti");
                    print_relation("Felletti.pdf", "La stratificazione del rischio nelle emorragie digestive del tratto superiore", "A. Felletti Spadazzi");
                    print_relation("Guidetti_Giuffre.pdf", "La Sincope", "A. Guidetti,E. Giuffrè");
                    print_relation("Iervese.pdf", "Il boarding e i suoi rischi", "T. Iervese");
                    print_relation("Lenzi.pdf", "I nuovi anticoagulanti orali(NAO). Cosa cambia ? ", "T. Lenzi");
                    print_relation("Magnacavallo.pdf", "Molti scores necessari per stratificare il rischio e definire il trattamento ottimale", "A. Magnacavallo");
                    print_relation("Pinelli.pdf", "Utilità degli score: luci ed ombre. Le Polmoniti. ", "G. Pinelli");
                    print_relation("Prati.pdf", "L'OBI e la medicina d'urgenza: come quantificare il carico di lavoro infermieristico", "K. Prati");
                    print_relation("Rastelli.pdf", "Il ricovero in appoggio in reparti \"incompetenti\"", "G. Rastelli");
                    print_relation("Scappin_Mule.pdf", "Gli score nella valutazione del dolore toracico", "S. Scappin, P. Mulè");
                    print_relation("Sighinolfi.pdf", "Transient Ischemic Attack", "D. Sighinolfi");
                    print_relation("Squarzoni.pdf", "Il Ricovero in \"letti aggiuntivi\"", "G. Squarzoni");
                    print_relation("Tampieri.pdf", "Score nella Fibrillazione atriale", "A. Tampieri");

                    break;

                case '296':

                    print_relation("Coratti.ppt", "La TME robotica", "A. Coratti, M. diMarino");
                    print_relation("De_Togni.ppt", "Epidemiologia e screening del tumore del retto ", "A. De Togni");
                    print_relation("Feo.pptx", "Escissione Totale del Mesoretto: Laparotomia vs Laparoscopia", "C. Feo");
                    print_relation("Frassoldati.ppt", "Basi razionali della terapia perioperatoria", "A. Frassoldati");
                    print_relation("Pezzoli.ppt", "I colostent: un ponte verso la chirurgia ? ", "A. Pezzoli");

                    break;

                case '291':

                    print_relation("Cagnin.pdf", "Prospettive future nella terapia della demenza di Alzheimer", "A. Cagnin");
                    print_relation("De_Ronchi.ppt", "Frontotemporal Dementia and Related Disorders: Organic Psychoses ? ", "D. De Ronchi, A. Rita");
                    print_relation("Fabbo.ppt", "Il trattamento non farmacologico dei BPSD: quali evidenze ? ", "A. Fabbo");
                    print_relation("Govoni.pdf", "Diagnosi della demenza di Alzheimer", "S. Govoni");
                    print_relation("Mecocci.ppt", "La Demenza con corpi di Lewy: diagnosi e trattamento", "P. Mecocci");
                    print_relation("Nichelli.ppt", "La demenza nel morbo di Parkinson", "P. Nichelli");
                    print_relation("Rozzini.ppt", "Depressione e demenza oppure demenza e depressione ? ", "R. Rozzini");
                    print_relation("Zanetti.ppt", "Stato d’arte della terapia della demenza di Alzheimer", "O. Zanetti");

                    break;

                case '288':

                    print_relation("Atti.pdf", "L’ecografia dell’anca neonatale", "G. Atti");
                    print_relation("Atti_2.pdf", "Diagnosi della Displasia Evolutiva dell’Anca", "G. Atti");
                    print_relation("Ciliberti.pdf", "Casi Clinici", "M. Ciliberti, V. Frisullo");
                    print_relation("Di_Pancrazio.pdf", "Dall’embriogenesi all’imaging", "L. DiPancrazio");
                    print_relation("Lombardi.pdf", "Back to school: dall’Embriologia all’Imaging dell’Apparato Digerente", "A. A. Lombardi");

                    break;

                case '282':

                    print_relation("locandina.pdf", "Locandina del convegno");
                    print_relation("folder_esterno.pdf", "Folder esterno ");
                    print_relation("folder_interno.pdf", "Folder interno");
                    print_relation("Ascanelli.ppt", "Sindrome da ostruita defecazione", "S. Ascanelli");
                    print_relation("De_Togni.pdf", "Risultati del programma di screening dei tumori del colon retto a Ferrara", "A. De Togni");
                    print_relation("Feo.ppt", "Il cancro del colon: aspetti chirurgici", "C. Feo");
                    print_relation("Gasbarrini.ppt", "Terapia dell'esofagite da reflusso", "A. Gasbarrini");
                    print_relation("Matarese.ppt", "Aspetti endoscopici", "V. G. Matarese");
                    print_relation("Pansini.ppt", "Lesioni precancerose & tumori dell'esofago", "G. Pansini");
                    print_relation("Pazzi.ppt", "Gli inibitori della secrezione gastrica: sempre utili, sempre innocui ? ", "P. Pazzi");
                    print_relation("Zagari.ppt", "Eradicazione H. pylori, quando e come", "R. M. Zagari");

                    break;

                case '278':

                    print_relation("Arveda.pptx", "La cefalea e il dentista", "N. Arveda");
                    print_relation("Ferronato.pptx", "Trattamento comportamentale: esperienza su volontari del gruppo di autoaiuto di Ferrara", "C. Ferronato");
                    print_relation("Fiumana.ppt", "Impatto della cefalea sul sistema sanitario nazionale", "E. Fiumana ");
                    print_relation("Galli.pptx", "Le terapie non farmacologiche", "F. Galli");
                    print_relation("Gaudio.ppt", "La disabilità e gli aspetti medico - legali", "R. M. Gaudio");
                    print_relation("Merighi.ppt", "La cefalea in età pediatrica", "L. Merighi");
                    print_relation("Tassorelli.ppt", "Fisiopatologia della cefalea emicranica nelle varie età della vita. ", "C. Tassorelli");
                    print_relation("Terrazzino.pptx", "La Farmacogenomica", "S. Terrazzino");

                    break;

                case '266':

                    print_relation("Bruni.ppt", "Focus su Drospirenone", "V. Bruni");
                    print_relation("Costantino.ppt", "Il futuro della contraccezione ormonale: quale sarà il ruolo occupato dall’etonogestrel ? ", "D. Costantino");
                    print_relation("Ferrari.ppt", "Clormadinone, perché prescriverlo. ", "S. Ferrari");
                    print_relation("Furicchia.ppt", "Estroprogestinici e Rischio Anestesiologico", "G. Furicchia");
                    print_relation("Guaraldi.pptx", "Scelte contraccettive in Italia vs Europa ed America", "C. Guaraldi");
                    print_relation("Marci.ppt", "La contraccezione ormonale nella terapia dell’endometriosi", "R. Marci");
                    print_relation("Morgante.pdf", "La contraccezione nell’adolescenza", "G.
                Morgante");
                    print_relation("Nedea.pptx", "Lo iud è ancora richiesto ? Perchè e quante donne lo preferiscono ? ", "D. Nedea");
                    print_relation("Vaccaro.ppt", "COC e TROMBOSI", "V. Vaccaro");
                    print_relation("Valerio.ppt", "Levonorgestrel, desogestrel, gestodene e nomegestrolo a confronto", "A. Valerio");

                    break;

                case '258':

                    print_relation("Ciorba.ppt", "Alterazioni fisiopatologiche indotte dal tumore della laringe", "A. Ciorba");
                    print_relation("Malagutti_Aimoni_Ciorba.ppt", "Le alterazioni fisio - patologiche legate al tumore del cavo orale", "N. Malagutti, C. Aimoni, A. Ciorba");
                    print_relation("Merlo_Raffaelli_Pastore.ppt", "Alterazioni funzionali iatrogene e strategie riabilitative chirurgiche", "R. Merlo, R. Raffaelli, A. Pastore");
                    print_relation("Pastore.ppt", "La riabilitazione funzionale nella chirurgia oncologica oro - faringo - laringea", "A. Pastore");
                    print_relation("Poletti.ppt", "Il consenso informato ", "D. Poletti");
                    print_relation("Stomeo.ppt", "Riabilitazione post - chirurgica fonoarticolatoria", "F. Stomeo");

                    break;

                case '255':

                    print_relation("Caletti.ppt", "L’accreditamento dei servizi in Endoscopia Digestiva dalla Teoria alla Pratica. Quali ricadute sui Servizi ? ", "G. Ciorba");
                    print_relation("Di_Giulio.ppt", "Appropriatezza e Costi", "E. DiGiulio");
                    print_relation("Fabbri.ppt", "Il Percorso del paziente con tumore del colon retto", "C. Fabbri");
                    print_relation("Fusaroli.ppt", "Role of EUS in colon lesions", "P. Fusaroli");
                    print_relation("Gaiani.ppt", "L’appropriatezza delle prestazioni infermieristiche in Endoscopia Digestiva", "R. Gaiani");
                    print_relation("Gullini.ppt", "Presentazionde del convegno", "S. Gullini");
                    print_relation("Matarese.ppt", "Controlli e follow - up: quando si e quando no", "V. G. Matarese");
                    print_relation("Merighi.ppt", "La richiesta urgente: quando, dove, con chi", "A. Merighi");
                    print_relation("Petrini.ppt", "Appropriatezza della sedazione: tra linee guida e legislazione. Quando, come e chi la deve fare", "S. Petrini");
                    print_relation("Ruina.ppt", "L'appropriatezza in endoscopia digestiva", "M. Ruina");
                    print_relation("Sensi.ppt", "La malattia di parkinson in fase avanzata: opzioni terapeutiche", "M. Sensi");
                    print_relation("Stroppa.ppt", "Appropriatezza e rischio clinico in endoscopia digestiva", "I. Stroppa");
                    print_relation("Tafi.ppt", "Bioetica e nutrizione artificiale", "A. Tafi");
                    print_relation("Trevisani.ppt", "Appropriatezza delle Richieste: Esperienza di Ferrara", "L. Trevisani");
                    print_relation("Triossi.ppt", "Appropriatezza dell’operatività in corso di colonscopia: polipectomia, mucosectomia, dissezione sottomucosa ", "O. Triossi");
                    print_relation("Zoppellari.pptx", "Appropriatezza della PEG nel paziente neurologico complesso: stato vegetativo permanente", "R. Zoppellari");
                    print_relation("Zurlo.ppt", "La PEG nel paziente anziano demente: il punto di vista del Geriatra", "A. Zurlo");

                    break;

                case '254':

                    print_relation("Costantino.ppt", "Trattamento della dismenorrea e del dolore post - episiotomico con ibuprofene sale di arginina. ", "D. Costantino");
                    print_relation("D'Ambrosio.ppt", "Maternità e dipendenze patologiche: il ruolo dello psicologo", "F. D'Ambrosio");
                    print_relation("De_Francesco.ppt", "La tristezza che assale la mamma dopo il parto, cause, sintomi e rimedi. ", "L. De Francesco");
                    print_relation("Furicchia.ppt", "Il management della gravida tossicodipendente: il ruolo dell’anestesista - rianimatore", "G. Furicchia");
                    print_relation("Giugliano.ppt", "L - Metionina, Ibiscus sabdariffea e Boswellia serrata nel trattamento delle infezioni urinarie in gravidanza", "E. Giugliano");
                    print_relation("Guaraldi.ppt", "Alcool e gravidanza: sindrome feto alcolica. ", "C. Guaraldi");
                    print_relation("Guerra.ppt", "Antiossidanti e neurotrofici in ginecologia: nuove prospettive terapeutiche", "L. Guerra");
                    print_relation("Larocca.ppt", "La vita prima di nascere", "F. Larocca");
                    print_relation("Mantegazza_Murina.pptx", "Dolore pelvico cronico: nuovi approcci terapeutici", "V. Mantegazza, F. Murina");
                    print_relation("Marci.ppt", "La sindrome premestruale: eziopatogenesi, diagnosi e terapia", "R. Marci");
                    print_relation("Migliaccio.pptx", "Eziopatogenesi della vulvodinia: nuovi approcci terapeutici", "R. Migliaccio");
                    print_relation("Morgante.pptx", "Tabagismo ed Alcool: il loro ruolo nella infertilità maschile e femminile. ", "G. Morgante");
                    print_relation("Ricchieri.ppt", "Le proantocianidine nella profilassi delle infezioni urinarie", "F. Ricchieri");

                    break;

                case '229':

                    print_relation("Bonaccorsi.pdf", "La sindrome metabolica in postmenopausa", "G. Bonaccorsi");
                    print_relation("Bruni.pdf", "Contraccezione per via orale e riduzione dell’intervallo libero da ormoni: quali benefici", "V. Bruni");
                    print_relation("Cagnacci.pdf", "Estradiolo e Dienogest: quali altri vantaggi oltre la contracceziione", "A. Cagnacci");
                    print_relation("Carlomagno.pdf", "Inositol, Melatonin and oocyte quality", "G. Carlomagno");
                    print_relation("Costantino.pdf", "L'adelmidrol nelle iperalgesie vulvovaginali nel post - parto e puerperio", "D. Costantino");
                    print_relation("Costantino_2.pdf", "L’inositolo", "D. Costantino");
                    print_relation("Dante_Facchinetti.pdf", "L’inositolo nella terapia della PCOS", "G. Dante, F. Facchinetti");
                    print_relation("Farina.pdf", "Anello vaginale ed effetti metabolici", "A. E. Farina");
                    print_relation("Furicchia.pdf", "Il ruolo dell’anestesista nella terapia del dolore cronico", "G. Furicchia");
                    print_relation("Giugliano.pdf", "Impiego non contraccettivo della pillola con solo progestinico", "E. Giugliano");
                    print_relation("Guaraldi.pdf", "Micronutrienti in gravidanza", "C. Guaraldi");
                    print_relation("Marelli.pdf", "Inositolo, anti - ossidanti e apparato riproduttivo maschile", "G. Marelli");

                    break;

                case '84':

                    print_relation("Dionigi.ppt", "Il monitoraggio nervoso intraoperatorio", "G. Dionigi");
                    print_relation("Feggi.ppt", "Medicina nucleare e biopsia del linfonodo sentinella", "L. M. Feggi");
                    print_relation("Feo.ppt", "Colon: Il punto di vista del chirurgo", "C. Feo");
                    print_relation("Lanza.ppt", "Il punto di vista dell’anatomo - patologo", "G. Lanza");
                    print_relation("Rossi.ppt", "Ecografia preoperatoria ed indicazioni all’exeresi del linfonodo sentinella ", "R. Rossi");
                    print_relation("Verdecchia.ppt", "Controversie nella biopsia del linfonodo sentinella nel melanoma", "G. M. Verdecchia");
                    print_relation("Zanzi.ppt", "L'esperienza ferrarese", "M. V. Zanzi");
                    print_relation("Zatelli.ppt", "Analisi genetica del nodulo tiroideo: implicazioni per l’esecuzione del linfonodo sentinella", "M. C. Zatelli");

                    break;

                case '79':

                    print_relation("Bertolazzi.pdf", "Lesioni da caustici. Gestione pre - endoscopica nel DEA", "P. Bertolazzi");
                    print_relation("Bozzolani.pdf", "Trattamento intensivo pre - endoscopico nel paziente con emorragia digestiva", "G. Bozzolani");
                    print_relation("Carlini.pdf", "Percorsi Integrati in Endoscopia Digestiva: perché questa scelta ? ", "E. Carlini");
                    print_relation("Cifalà.pdf", "Il timing dell’endoscopia e descrizione degli Algoritmi", "V. Cifalà");
                    print_relation("Galeotti_Salviato.pdf", "Le Emorragie Digestive Varicose e Non - Varicose - Ruolo della Radiologia Interventistica", "R. Galeotti, E. Salviato");
                    print_relation("Giacometti.pdf", "Il Corpo Estraneo: Gestione pre - endoscopica nel DEA", "M. Giacometti");
                    print_relation("Giampreti_Locatelli.pdf", "I Caustici – Il punto di vista del CAV", "A. Giampreti, C. Locatelli");
                    print_relation("Locatelli_Giampreti.pdf", "La decontaminazione mediante EGD", "C. Locatelli, A. Giampreti");
                    print_relation("Matarese.pdf", "I caustici: descrizione degli algoritmi", "V. G. Matarese");
                    print_relation("Merighi.pdf", "L’esperienza AIGO - SIED - SIGE - SIMEU Emilia - Romagna in tema di emergenze - urgenze in Endoscopia Digestiva", "A. Merighi");
                    print_relation("Michelini.pdf", "L'endoscopia d'urgenza in età pediatrica", "M. E. Michelini");
                    print_relation("Osti.pdf", "Il ruolo dell’anestesista rianimatore nella gestione del paziente con ingestione di caustici", "D. Osti");
                    print_relation("Pezzoli.pdf", "La colonscopia in urgenza: quando e come", "A. Pezzoli");
                    print_relation("Ricciardelli.pdf", "Interfaccia AUSL - AOU", "A. Ricciardelli");
                    print_relation("Rossi.pdf", "Caustici: Ruolo dell’E. D. ", "A. Rossi");
                    print_relation("Trevisani1.pdf", "Presentazione del gruppo di lavoro", "L. Trevisani");
                    print_relation("Trevisani2.pdf", "Le emorragie varicose e non varicose: descrizione degli algoritmi", "L. Trevisani");
                    print_relation("Vasquez.pdf", "Ruolo del Chirurgo nei percorsi d’urgenza", "G. Vasquez");
                    print_relation("Zanotti.pdf", "L’interfaccia Azienda USL - Azienda Ospedaliera - Universitaria Ferrara", "C. Zanotti");

                    break;
                default:
                    header('Location: https://www.mcrferrara.org/relazioni.php');
            }

            ?>

        </ul>
        <h4>Note</h4>
        Le relazioni sono in ordine alfabetico secondo il cognome del relatore.<br/>
        Per scaricare una relazione cliccare sul titolo della stessa.

    </div>

<?php
$gen->chiudi_pagina();

