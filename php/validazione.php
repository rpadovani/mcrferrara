<?php
require_once(__DIR__ . '/../data/events.php');

$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

if (!isset($_POST['convegno']) || !is_numeric($_POST['convegno']) || $_POST['convegno'] <= 0) {
    header("Location:../iscrizioni.php?error=1");
}

$events = new events();
$no_event = false;
try {
    $event = $events->get_event_by_id($_POST['convegno']);

    if (!$event['iscrizioni']) {
        $no_event = true;
    }
} catch (Exception $e) {
    $no_event = true;
}

if ($no_event) {
    header("Location:../iscrizioni.php");
}

if (!isset($_POST['terms'])) {
    header("Location:../iscrizioni.php?error=2");
}

$title = $event['titolo'];

if (isset($_POST['nome']))
    $nome = $_POST['nome'];
else
    header("Location:../iscrizioni.php?error=3");

if (isset($_POST['cognome']))
    $cognome = $_POST['cognome'];
else
    header("Location:../iscrizioni.php?error=4");

if (isset($_POST['luogo_nascita']))
    $luogo_nascita = $_POST['luogo_nascita'];

if (isset($_POST['data_nascita']))
    $data_nascita = $_POST['data_nascita'];

if (isset($_POST['indirizzo']))
    $indirizzo = $_POST['indirizzo'];
else
    header("Location:../iscrizioni.php?error=5");

if (isset($_POST['città']))
    $citta = $_POST['città'];
else
    header("Location:../iscrizioni.php?error=6");

if (isset($_POST['provincia']))
    $provincia = $_POST['provincia'];

if (isset($_POST['cap']))
    $cap = $_POST['cap'];

if (isset($_POST['email']))
    $email = $_POST['email'];
else
    header("Location:../iscrizioni.php?error=7");

if (isset($_POST['telefono']))
    $telefono = $_POST['telefono'];

if (isset($_POST['ente']))
    $ente = $_POST['ente'];
else
    header("Location:../iscrizioni.php?error=8");

if (isset($_POST['ruolo']) && $_POST['ruolo'] !== '') {
    $role = $_POST['ruolo'];
} else {
    $role = $_POST['specialty'];
}

//Email
$message = "
Convegno: " . $title . "
Cognome: " . $cognome . "
Nome: " . $nome . "
Luogo di nascita: " . $luogo_nascita . "
Data di nascita: " . $data_nascita . "

Indirizzo: " . $indirizzo . "
Città: " . $citta . "
Provincia: " . $provincia . "
CAP: " . $cap . "

E-mail: " . $email . "
Telefono: " . $telefono . "

Ente: " . $ente . "
Ruolo: " . $role;

if (isset($_POST['invoicing_name']) && $_POST['invoicing_name'] !== "" && $_POST['invoicing_name'] !== null) {
    $message .= "
    
Fatturazione

Nome e cognome / Organizzazione: " . $_POST['invoicing_name'] . "
Indirizzo: " . $_POST['invoicing_address'] . "
Città: " . $_POST['invoicing_town'] . "
C.F. / P.IVA: " . $_POST['invoicing_vat'] . "
Codice univoco: " . $_POST['invoicing_code'];
}

if (isset($_POST['fee'])) {

    $message .= "
    
Tipologia partecipante: " . $_POST['fee'];

}

if (isset($_POST['satelliteCourse'])) {
    $message .= "
    
Satellite course: ";

    if ($_POST['satelliteCourse'] === 'stage') {
        $message .= 'Strategically Acquired Gradient Echo (STAGE) imaging for brain tissue quantitative mapping and vessel enhancement';
    } else if ($_POST['satelliteCourse'] === 'doppler') {
        $message .= "Up date on Doppler Ultrasonography of the neck vessels – what’s new?";
    } else if ($_POST['satelliteCourse'] === 'endovenous') {
        $message .= "Endovenous surgery for sparing the great saphenous vein.";
    } else if ($_POST['endovascular']) {
        $message .= "New trends and devices in endovascular surgery";
    }
}

$message .= "

Con l'invio della presente autorizzo MCR Ferrara a inserire i miei dati nell'elenco dei suoi clienti per l'invio di materiale riguardante l'evento congressuale.
In ogni momento, a norma dell'art. 13 legge 675/96 e del Regolamento UE 2016/679, potrò avere accesso ai miei dati, chiederne la modifica o la cancellazione oppure oppormi al loro utilizzo scrivendo a: MCR Ferrara Via Cittadella 31, 44121 Ferrara.
                        
";

if (isset($_POST['terms_ads'])) {

    $message = $message . "

Con l'invio della presente autorizzo MCR Ferrara a inserire i miei dati nell'elenco dei suoi clienti per l'invio di materiale informativo congressuale di natura commerciale.
In ogni momento, a norma dell'art. 13 legge 675/96 e del Regolamento UE 2016/679, potrò avere accesso ai miei dati, chiederne la modifica o la cancellazione oppure oppormi al loro utilizzo scrivendo a: MCR Ferrara Via Cittadella 31, 44121 Ferrara.
                       
";

}

$message .= "
Autorizazzione al trattemento dei dati con finalità commerciale: 
" . isset($_POST['terms_ads']) === true;

$headers = "From: " . $email . "\r\n" .
    "Reply-To: " . $email . "\r\n" .
    'X-Mailer: PHP/' . phpversion() . "\r\n" .
    'Content-Type: text/plain; charset=UTF-8';

$destinatario = "info@mcrferrara.org";
$oggetto = "Iscrizione online all'evento " . $title;
mail($destinatario, $oggetto, $message, $headers);

header("Location:../conferma_iscrizione.php");
