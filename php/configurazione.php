<?php
/*
Project: Mcr Ferrara - Sito ufficiale  
Author: Riccardo Padovani <ricki.padovani@gmail.com>
Copyright: 2011 Riccardo Padovani
License: GPL-2+
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

On Debian GNU/Linux systems, the full text of the GNU General Public License
can be found in the file /usr/share/common-licenses/GPL-2.
*/

/* Convenzioni
HTML 4.1 & CSS 2
Tabulazioni: 4 spazi
Nomi separati da underscore
*/

/*Il file contiene i dati di configurazione principali espressi come costanti
Il file va rinominato come configurazione.php*/

//Dati connessione al database
define('SERVER', 'indirizzo_ip_del_server');
define('NOME', 'nome_utente');
define('PASSWORD', 'password');
define('DATABASE', 'database_tabella_convegni');

//Email dell'amministratore di sistema
define('EMAIL_AMMINISTRATORE', 'example@example.com');

//Email no-reply mittente dei messaggi d'errore
define('EMAIL_NO_REPLY', 'no-reply@example.com');
?>
