<?php
$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

require_once (__DIR__.'/../data/events.php');

function echoActiveClassIfRequestMatches($requestUri)
{
    $current_file_name = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);

    if (gettype($requestUri) == "string" ) {
        $requestUri = [$requestUri];
    }

    if (in_array($current_file_name, $requestUri)) {
        return 'active';
    }
}

function echoDisabledIfNoEventForYear($year)
{
    if (!isAnyEventForYear($year)) {
        return 'disabled';
    }
}

function isAnyEventForYear($year)
{
    try {
        $events = new events();
        $all_events = $events->events_by_year($year);
        return count($all_events) > 0;
    } catch (Exception $e) {
        return false;
    }
}

class generatore
{
    function __construct($titolo)
    {
        include __DIR__ . '/../templates/begin_header.html';
        echo "<title>$titolo</title>";
    }

    function chiudi_head()
    {
        echo "</head>";
    }

    function crea_body()
    {
        include __DIR__ . '/../templates/navbar.php';
    }

    function inserisci_contenuto()
    {
        echo <<<HTML
        <div id="contenuto">
HTML;
    }

    function chiudi_pagina()
    {
        include __DIR__ . '/../templates/footer.html';
    }
}

