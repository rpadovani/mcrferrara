<?php
require_once('php/generatore.php');
require_once('data/events.php');
require_once('data/utils.php');

set_language($_SERVER['HTTP_ACCEPT_LANGUAGE'], $_GET['locale']);

$events = new events();

if (!isset($_GET['id'])) {
    header('Location: https://www.mcrferrara.org');
}

$event = null;

try {
    $event = $events->get_event_by_id($_GET['id']);
} catch (Exception $e) {
    header('Location: https://www.mcrferrara.org');
}

if ($event == null) {
    header('Location: https://www.mcrferrara.org');
}

$is_event_in_future = new DateTime() < new DateTime($event['start_date']);
$start_date = strftime('%d %B %Y', strtotime($event['start_date']));
$end_date = strftime('%d %B %Y', strtotime($event['end_date']));

$data_text = $is_event_in_future ? _('The event will take place') : _('The event took place');
$data_text .= ' ';
$data_text .= $event['end_date'] ? _('from') . ' ' . $start_date . ' ' . _('to') . ' ' . $end_date : _('on') . ' ' . $start_date;

$gen = new generatore($event['titolo']);

$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <h2><?= _("Event introduction") ?></h2>

        <div class="event-title text-center">
            <?= $event['titolo'] ?>
        </div>

        <p><?= $event['descrizione'] ?></p>


        <?php if ($event['iscrizioni'] && $is_event_in_future) { ?>
            <h2><?= _("Registration form") ?></h2>
            <a href='iscrizioni.php?convegno=<?= $event['id_convegno'] ?>'
               title='<?= _("Register to the event online") ?>' class="invisible-link">
                <div class="row" style="margin-top: 20px">
                    <div class="col-2">
                        <img src='img/iscrizioni.gif' alt='<?= _("Online registration") ?>' class="mx-auto d-block"
                             height="50px"/>
                    </div>

                    <div class="col-8">
                        <?= _("You can register to the event online") ?>
                    </div>
                </div>
            </a>
        <?php } ?>

        <h2><?= _("Date and place") ?></h2>

        <div class="row">
            <div class="col">
                <p><?= $event['luogo'] ?></p>

                <p><?= $data_text ?></p>
            </div>

            <!--        <div class="col">-->
            <!--            <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=11.679368019104004%2C44.79241929068141%2C11.714150905609133%2C44.806396659837226&amp;layer=mapnik&amp;marker=44.79940839850965%2C11.696748733520508" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=44.7994&amp;mlon=11.6967#map=16/44.7994/11.6968" target="_blank">Apri in una nuova scheda</a></small>-->
            <!--        </div>-->
        </div>

        <h2><?= _("More information") ?></h2>

        <div class="row">
            <div class="col">
                <p><?= _('If you need more details on the event, please <a href="chi_siamo.php">contact us</a>.') ?></p>
            </div>
        </div>

        <?php
        if ($event['società']) {
            switch ($event['società']) {
                case 'smc':
                    {
                        ?>

                        <a href='smc.php' title='Organizzato dalla Smc' class="invisible-link" target="_blank">
                            <div class="row" style="margin-top: 20px">
                                <div class="col-2">
                                    <img src='img/segreterie/smc.jpg'
                                         alt='Convegno Smc'
                                         class='mx-auto d-block'
                                         height="50px"
                                    />
                                </div>

                                <div class="col-8">
                                    Evento organizzato dalla Società Medico Chirurgica
                                </div>
                            </div>
                        </a>

                        <?php
                        break;
                    }
            }
        }

        if ($event['download']) { ?>
            <a href="<?= $event['download'] ?>" title='Scarica il programma del convegno'
               class="invisible-link" target="_blank">
                <div class="row" style="margin-top: 20px">
                    <div class="col-2">
                        <img src='img/pdf.png' class="mx-auto d-block" alt='Programma' height="50px"/>
                    </div>

                    <div class="col-8">
                        È presente un programma da scaricare
                    </div>
                </div>
            </a>

        <?php }

        if ($event['relazioni']) { ?>
            <a href='dettagli_relazione.php?id=<?= $event['id_convegno'] ?>'
               title='Scarica le relazioni del convegno' class="invisible-link">
                <div class="row" style="margin-top: 20px">
                    <div class="col-2">
                        <img src='img/relazioni.jpg' alt='Relazioni' class="mx-auto d-block" height="50px"/>
                    </div>

                    <div class="col-8">
                        È possibile consultare le relazioni dell'evento
                    </div>
                </div>
            </a>

        <?php }

        if ($event['scheda_iscrizione'] && $is_event_in_future) { ?>
            <div class="row" style="margin-top: 20px">
                <div class="col">
                    <a href='download/iscrizioni/<?= $event['scheda_iscrizione'] ?>'
                       title='Scarica la scheda di iscrizione'>
                        <img src='img/download_iscrizioni.jpg' alt='Scheda di iscrizione'/>
                    </a>

                </div>
            </div>
        <?php }

        if ($event['società'] && $event['società'] == 'smc') { ?>
            <h2>Iscrizioni</h2>
            <div class="row" style="margin-top: 20px">
                <div class="col">
                    Le iscrizioni agli incontri della SMC sono diverse a seconda della professione:
                    <ul>
                        <li><b>Infermieri e tecnici</b> dipendenti dell'AOU FE devono iscriversi tramite il
                            proprio coordinatore, per quelli non dipendenti si sa solo la mattina del
                            convegno se ci sono posti disponibili per i crediti
                        <li><b>Medici:</b> i crediti sono riservati agli iscritti alla SMC (€48 annui) o a
                            chi si iscrive alla giornata (€25,00)
                        </li>
                    </ul>

                    In ogni caso <b>le iscrizioni avvengono la mattina stessa</b> e la partecipazione, con
                    attestato, è libera
                </div>
            </div>
        <?php }

        if ($event['accreditamento']) { ?>
            <div class="row" style="margin-top: 20px">
                <div class="col">
                    L'evento è <b>accreditato</b> per <?= $event['accreditamento'] ?>
                </div>
            </div>
        <?php } ?>
    </div>

<?php

$gen->chiudi_pagina();

