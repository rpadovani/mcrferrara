<?php
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'user');
define('DB_PASSWORD', 'pwd');
define('DB_NAME', 'events');

define('ADMIN_EMAIL', 'example@example.com');
define('EMAIL_NO_REPLY', 'no-reply@example.com');
