<?php
require_once('php/generatore.php');

$gen = new generatore("SEDI");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <h2>SEDI
            <small class="text-muted">Segreteria scientifica gestita da Mcr Ferrara</small>
        </h2>
        <p>
            <img src="img/segreterie/sedi.jpg" alt="Logo SEDI" class="rounded mx-auto d-block"/><br/>
            Il SEDI, sindacato endoscopisti digestivi italiani, è un sindacato medico con la finalità di tutelare ad
            ogni
            livello il ruolo dirigente e l'autonomia professionale dei medici endoscopisti digestivi di qualsiasi
            estrazione
            anche nel più ampio contesto delle discipline a cui afferiscono (Gastroenterologia, Chirurgia, Medicina
            interna,
            Pediatria). Il Sedi è socio fondatore della Fesmed, Federazione Sindacale Medici Dirigenti, che raggruppa ,
            oltre agli endoscopisti del Sedi, i chirurghi dell'ACOI, i ginecologi-ostetrici dell'AOGOI, i cardiologi
            dell'ANMCO, i medici di direzione Sanitaria del'ANMDO, i medici degli enti previdenziali della FEMEPA e il
            sindacato generalista SUMI. La Fesmed è sindacato a tutti gli effetti riconosciuto dal Ministero della
            Sanità,
            firmatario del Contratto Nazionale della Dirigenza Medica e convocato ad ogni riunione ufficiale sia a
            livello
            Nazionale che Regionale che Aziendale.
        </p>

        <h2>Contatti</h2>

        <dl class="row">
            <dt class="col-sm-3">Sede</dt>
            <dd class="col-sm-9">Via Comelico 3, 20135 Milano</dd>

            <dt class="col-sm-3">Sito internet</dt>
            <dd class="col-sm-9"><a href="http://www.sindacatosedi.it" target="_blank">www.sindacatosedi.it</a></dd>

            <dt class="col-sm-3">Indirizzo email</dt>
            <dd class="col-sm-9"><a href="mailto:info@sindacatosedi.it">info@sindacatosedi.it</a></dd>
        </dl>
    </div>

<?php
$gen->chiudi_pagina();
