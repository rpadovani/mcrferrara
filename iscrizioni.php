<?php
require_once('php/generatore.php');
require_once('data/utils.php');
require_once('data/events.php');
require_once('templates/switch_statement_events.php');

$events = new events();

set_language($_SERVER['HTTP_ACCEPT_LANGUAGE'], $_GET['locale']);

$gen = new generatore(_("Online registration"));
$no_online_subscriptions = false;
$no_database_connection = false;

try {
    $next_online_subscriptions = $events->get_next_online_subscriptions();

    if (sizeof($next_online_subscriptions) <= 0) {
        $no_online_subscriptions = true;
    }
} catch (Exception $e) {
    $no_database_connection = true;
}
?>

    <script type="text/javascript">
        function getUrlParameter(sParam) {
            let sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }

        function setMinAge() {
            let max = new Date();
            max.setFullYear(max.getFullYear() - 18);

            const dateElement = document.getElementById('data_nascita');
            dateElement.setAttribute('max', max.toISOString().split("T")[0]);
        }

        function eventSelected() {
            const lista = document.getElementById("convegno");
            const event_from_url = getUrlParameter('convegno');
            const event_id = parseInt(lista.options[lista.selectedIndex].value) || parseInt(event_from_url);

            if (!parseInt(lista.options[lista.selectedIndex].value) && event_from_url) {
                lista.value = event_from_url;
            }

            document.getElementById("chosen").style.display = event_id ? 'inline' : 'none';

            let toHide;
            let toShow;

            if (event_id === 474) {
                toHide = document.getElementsByClassName('hidden-for-474');
                toShow = document.getElementsByClassName('show-for-474');
            } else {
                toHide = document.getElementsByClassName('show-for-474');
                toShow = document.getElementsByClassName('hidden-for-474');
            }

            for (let i = 0; i < toHide.length; i++) {
                toHide.item(i).style.display = 'none';
            }

            for (let i = 0; i < toShow.length; i++) {
                toShow.item(i).style.display = 'block';
            }

            const selectRuolo = document.getElementById("ruolo");
            selectRuolo.options.length = 1;

            switch (event_id) {

            <?php
                if ($no_database_connection == false) {
                    foreach ($next_online_subscriptions as $row => $event) {
                        echo print_roles_switch_statement($event['id_convegno']);
                    }
                }
                ?>

                default:
                    selectRuolo.options[selectRuolo.options.length] = new Option('Medico', 'Medico');
                    selectRuolo.options[selectRuolo.options.length] = new Option('Infermiere', 'Infermiere');
            }
        }

        window.onload = function () {
            eventSelected();
            setMinAge();
        }
    </script>

<?php
$gen->chiudi_head();
$gen->crea_body();


if ($no_online_subscriptions) {
    ?>

    <div class="alert alert-dark" role="alert">
        <?= _("At the moment there isn't any event you can register for, please check again in next weeks") ?>
    </div>

    <?php
}

if ($no_database_connection) {
    ?>

    <div class="alert alert-danger" role="alert">
        Abbiamo riscontrato un problema, i nostri tecnici sono stati informati e sono al lavoro per risolvere.
        Per favore riprovi fra qualche ora.
    </div>

    <?php
}
?>

    <div class="container">
        <h2><?= _("Online registration to events") ?></h2>

        <form method="post" action="php/validazione.php" class="needs-validation" novalidate accept-charset="UTF-8">
            <div class="form-group row">
                <label for="convegno" class="col-sm-2 col-form-label"><?= _("Choose an event") ?></label>
                <div class="col-sm-10">
                    <select id="convegno" name="convegno" class="form-control" onchange="eventSelected()" required>
                        <option value>[<?= _("Choose an event") ?>]</option>

                        <?php
                        foreach ($next_online_subscriptions as $row => $event) {
                            ?>

                            <option value="<?= $event['id_convegno'] ?>"><?= $event['titolo'] ?></option>

                            <?php
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div id="chosen" style="display:none;">

                <h4><?= _("Personal data") ?></h4>
                <div class="form-group row">
                    <label for="nome" class="col-sm-2 col-form-label"><?= _("Name") ?></label>
                    <div class="col-sm-10">
                        <input id="nome" name="nome" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your name.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cognome" class="col-sm-2 col-form-label"><?= _("Surname") ?></label>
                    <div class="col-sm-10">
                        <input id="cognome" name="cognome" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your surname.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="luogo_nascita" class="col-sm-2 col-form-label"><?= _("Place of birth") ?></label>
                    <div class="col-sm-10">
                        <input id="luogo_nascita" name="luogo_nascita" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your place of birth.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="data_nascita" class="col-sm-2 col-form-label"><?= _("Date of birth") ?></label>
                    <div class="col-sm-10">
                        <input id="data_nascita" name="data_nascita" class="form-control" type="date" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your date of birth. To register to events you need be 18 or older.") ?>
                        </div>
                    </div>
                </div>

                <h4><?= _("Your domicile") ?></h4>
                <div class="form-group row">
                    <label for="indirizzo" class="col-sm-2 col-form-label"><?= _("Address") ?></label>
                    <div class="col-sm-10">
                        <input id="indirizzo" name="indirizzo" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your current address.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="città" class="col-sm-2 col-form-label"><?= _("City") ?></label>
                    <div class="col-sm-10">
                        <input id="città" name="città" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your city.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="provincia" class="col-sm-2 col-form-label"><?= _("State") ?></label>
                    <div class="col-sm-10">
                        <input id="provincia" name="provincia" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your state.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cap" class="col-sm-2 col-form-label"><?= _("ZIP code") ?></label>
                    <div class="col-sm-10">
                        <input id="cap" name="cap" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert your ZIP code.") ?>
                        </div>
                    </div>
                </div>

                <h4><?= _("Your contacts") ?></h4>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label"><?= _("Email address") ?></label>
                    <div class="col-sm-10">
                        <input id="email" name="email" class="form-control" type="email" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert a valid email address.") ?>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="telefono" class="col-sm-2 col-form-label"><?= _("Phone number") ?></label>
                    <div class="col-sm-10">
                        <input id="telefono" name="telefono" class="form-control" type="tel" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert a valid phone number.") ?>
                        </div>
                    </div>
                </div>

                <h4><?= _("Organization") ?></h4>
                <div class="form-group row">
                    <label for="ente" class="col-sm-2 col-form-label"><?= _("Organization / company") ?></label>
                    <div class="col-sm-10">
                        <input id="ente" name="ente" class="form-control" type="text" required/>
                        <div class="invalid-feedback">
                            <?= _("Please, insert the name of the organization / company you work for.") ?>
                        </div>
                    </div>
                </div>

                <div class="hidden-for-474">
                    <div class="form-group row">
                        <label for="ruolo" class="col-sm-2 col-form-label">Ruolo</label>
                        <div class="col-sm-10">
                            <select id="ruolo" name="ruolo" class='form-control'>
                                <option value>[Seleziona ruolo]</option>
                            </select>
                            <div class="invalid-feedback">
                                Per favore, selezioni il suo ruolo.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="show-for-474">
                    <h4><?= _("Invoicing data") ?></h4>
                    <div class="form-group row">
                        <label for="invoicing_name"
                               class="col-sm-2 col-form-label"><?= _("Surname and Name / Organization") ?></label>
                        <div class="col-sm-10">
                            <input id="invoicing_name" name="invoicing_name" class="form-control" type="text"/>
                            <div class="invalid-feedback">
                                <?= _("Please, insert your surname and name, or your organization.") ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="invoicing_address" class="col-sm-2 col-form-label"><?= _("Address") ?></label>
                        <div class="col-sm-10">
                            <input id="invoicing_address" name="invoicing_address" class="form-control" type="text"/>
                            <div class="invalid-feedback">
                                <?= _("Please, insert an address for invoicing.") ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="invoicing_town"
                               class="col-sm-2 col-form-label"><?= _("Town / ZIP Code / State") ?></label>
                        <div class="col-sm-10">
                            <input id="invoicing_town" name="invoicing_town" class="form-control" type="text"/>
                            <div class="invalid-feedback">
                                <?= _("Please, insert a town for invoicing.") ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="invoicing_vat" class="col-sm-2 col-form-label"><?= _("CF or VAT number") ?></label>
                        <div class="col-sm-10">
                            <input id="invoicing_vat" name="invoicing_vat" class="form-control" type="text"/>
                            <div class="invalid-feedback">
                                <?= _("Please, insert a VAT number or an italian fiscal code card number.") ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="invoicing_code" class="col-sm-2 col-form-label">Codice univoco</label>
                        <div class="col-sm-10">
                            <input id="invoicing_code" name="invoicing_code" class="form-control" type="text"/>
                            <div class="invalid-feedback">
                                Per favore, inserisca il suo codice univoco.
                            </div>
                        </div>
                    </div>
                </div>



                <div class="show-for-474">

                    <h4>Quota di iscrizione</h4>

                    <table class="table">
                        <thead class="thead-mcr">
                        <tr>
                            <th scope="col">Select</th>
                            <th scope="col"></th>
                            <th scope="col">Prima del 15 aprile</th>
                            <th scope="col">Dopo il 15 aprile</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="radio" name="fee"
                                           id="fee1"
                                           value="Biologi, Medici, Ostetriche, Tecnici di laboratorio e infermieri" aria-label="Costo partecipanti">
                                </div>
                            </th>
                            <td>Biologi, Medici, Ostetriche, Tecnici di laboratorio e infermieri</td>
                            <td>€150.00 (IVA inclusa)</td>
                            <td>€200.00 (IVA inclusa)</td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="radio" name="fee"
                                           id="fee3"
                                           value="Studenti e specializzandi" aria-label="studenti">
                                </div>
                            </th>
                            <td>Studenti e specializzandi</td>
                            <td>Gratuito</td>
                            <td>Gratuito</td>
                        </tr>
                        </tbody>
                    </table>

                    <b>Avviso agli studenti</b><br/>
                    <p>
                        A causa dell'elevato numero di iscrizioni pervenute, si rendono disponibili ancora 15 posti gratuiti. Si consiglia comunque agli studenti di iscriversi in quanto la segreteria si riserva di contattarvi in caso di eventuali rinunce.
                    </p>

<!--                        <p>-->
<!--                            It includes the entrance to the scientific sessions, the educational material, the coffee-->
<!--                            breaks-->
<!--                            and lunches, ECM.-->
<!--                        </p>-->


                    <h4>Termini di pagamento</h4>

                    <p>
                        Bonifico bancario a MCR IBAN <code>IT 41 T 06115 13000 000 000 005655</code> - Cassa di
                        Risparmio di Cento<br/>
                        N.B. <b>Non saranno fatti rimborsi in caso di cancellazione o mancata partecipazione</b>
                    </p>
                </div>

                <h4><?= _("Privacy policy") ?></h4>

                <p class="hidden-for-474"><b>L’iscrizione è gratuita.</b></p>

		<p>In ottemperanza alle disposizioni per la prevenzione della diffusione del COViD-19 i posti sono limitati e gli assembramenti vietati. Pertanto potranno accedere all’Aula solamente i pre-iscritti alla giornata.</p>

                <p>
                    <b>Consenso al trattamento dei dati personali per l'evento:</b> con l'invio della presente
                    autorizzo
                    MCR Ferrara a inserire i miei dati nell'elenco dei suoi clienti per l'invio di materiale
                    riguardante
                    l'evento congressuale.
                    In ogni momento, a norma dell'art. 13 legge 675/96 e del Regolamento UE 2016/679, potrò avere
                    accesso ai miei dati, chiederne la modifica o la cancellazione oppure oppormi al loro utilizzo
                    scrivendo a: MCR Ferrara Via Cittadella 31, 44121 Ferrara.
                    <a href="privacy.php" target="_blank">Ulteriori informazioni.</a>
                </p>

<!--                <div class="form-group show-for-474">-->
<!--                    <h5>Personal Data Protection Code</h5>-->
<!---->
<!--                    <p>-->
<!--                        The present document describes terms and conditions to handle users&#39; personal data. Users&#39;-->
<!--                        data are needed to register to the events organized and managed by MCR.<br/>-->
<!--                        The Code is written in compliance with the Legislative Decree 196/03 - art. 13 - Personal Data-->
<!--                        Protection Code.<br/>-->
<!--                        The Holder of the personal data used for executing the on-line registration is MCR All data are-->
<!--                        collected and recorded in MCR headquarter and processed by staff in charge of the processing-->
<!--                        either by-->
<!--                        external staff occasionally involved in data maintenance and recovery.-->
<!--                    </p>-->
<!---->
<!--                    <h5>The parties&#39; rights</h5>-->
<!---->
<!--                    <p>-->
<!--                        A data subject shall have the right to obtain confirmation as to whether or not his personal-->
<!--                        data-->
<!--                        exist and know their content and source. <br/>-->
<!--                        A data subject shall have the right to obtain updating, straightening or, where interested-->
<!--                        therein, integration-->
<!--                        of the data, erasure, conversion in anonymous form or blocking of data that have been processed-->
<!--                        unlawfully. <br/>-->
<!--                        The subject shall also have the right to object in whole or in part, to any data processing on-->
<!--                        legitimate grounds. <br/>-->
<!--                        Possible applications shall be addressed to MCR, as data treatment Holder.-->
<!--                    </p>-->
<!---->
<!--                    <h5>Data treated</h5>-->
<!---->
<!--                    <p>-->
<!--                        When a user registers online he/she is asked to fill in some personal data (name, surname, email-->
<!--                        etc.) as well as, the personal data of possible accompanying person. <br/>-->
<!--                        The personal data supplied are used by the MCR to supply the services. <br/>-->
<!--                        The personal data are not set about or communicated to third subjects, except if the-->
<!--                        communication is obligated by law or it is necessary for the fulfillment of the applications.-->
<!--                        <br/>-->
<!--                        The users shall decide to provide or not their personal data, however sometimes no service could-->
<!--                        be provided lacking users&#39; data.<br/>-->
<!--                        Besides, MCR may ask you to give the number of your credit card or other information concerning-->
<!--                        payment conditions. The aforesaid personal data, in case of hotel booking, could be notified to-->
<!--                        third entities (eg. hotels) as reservation guarantee. <br/>-->
<!--                        In addition, it could be necessary for registered users to provide also sensitive data (eg.-->
<!--                        health needs etc.). <br/>-->
<!--                        In the event, the party should, in compliance with the law, express his/her written consent on-->
<!--                        the downloaded specific form signing and faxing it back (by email if previously scanned). <br/>-->
<!--                        Lacking the aforementioned written consent, MCR will not be able to guarantee the quality-->
<!--                        service offered. <br/>-->
<!--                        The personal data provided by users for further purposes (eg. news on future event managed by-->
<!--                        MCR, business activities, surveys) will be only used for the aforesaid purposes and, if-->
<!--                        necessary,-->
<!--                        could be notified to third entities (eg. mailing etc). <br/>-->
<!--                        Besides the above mentioned already, such purposes include:-->
<!--                    </p>-->
<!--                    <ul>-->
<!--                        <li>business activities and future events of MCR;</li>-->
<!--                        <li>analysis and research activities in order to manage, protect and improve our services;</li>-->
<!--                    </ul>-->
<!---->
<!--                    <h5>Rules applying to data protection and handling</h5>-->
<!---->
<!--                    <p>-->
<!--                        Personal data are processed by automated tools and kept in a form which permits identification-->
<!--                        of the data subject for no longer than is necessary for the purposes for which the data were-->
<!--                        collected or subsequently processed. <br/>-->
<!--                        Safety measures are undertaken by MCR to prevent data losses, unlawful or/and incorrect uses or-->
<!--                        unauthorized accesses.<br/>-->
<!--                        Processed data will be constantly updated and completed, and, for this purpose, users are kindly-->
<!--                        invited to notify to MCR possible updates. <br/>-->
<!--                        All data are collected and recorded could be accessed only by employees and external staff in-->
<!--                        charge of the processing needing these kinds of information to be able to work as well as to-->
<!--                        manage, develop and improve our services-->
<!--                    </p>-->
<!--                </div>-->

                <div class="form-check">
                    <input class="form-check-input" name="terms" type="checkbox" value="accepted" id="terms"
                           required>
                    <label class="form-check-label" for="terms">
                        <?= _("I accept the above terms") ?>
                    </label>
                    <div class="invalid-feedback">
                        <?= _("You need to authorize MCR Ferrara to treat your data to continue") ?>
                    </div>
                </div>

                <div class="form-group row hidden-for-474">
                    <p>
                        <b>Consenso al trattamento dei dati personali per comunicazioni commerciali:</b> con l'invio
                        della presente autorizzo MCR Ferrara a inserire i miei dati nell'elenco dei suoi clienti per
                        l'invio di materiale informativo congressuale. <br/>
                        In ogni momento, a norma dell'art. 13 legge 675/96 e del Regolamento UE 2016/679, potrò avere
                        accesso ai miei dati, chiederne la modifica o la cancellazione oppure oppormi al loro utilizzo
                        scrivendo a: MCR Ferrara Via Cittadella 31, 44121 Ferrara.
                        <a href="privacy.php" target="_blank">Ulteriori informazioni.</a>
                    </p>

                    <div class="form-check">
                        <input class="form-check-input" name="terms_ads" type="checkbox" value="accepted"
                               id="terms_ads">
                        <label class="form-check-label" for="terms_ads">
                            Accetto di ricevere comunicazioni con finalità commerciali da Mcr Ferrara.
                        </label>
                    </div>
                </div>

                <button class="btn btn-primary btn-lg btn-success btn-block" type="submit">
                    <?= _("Register to the event!") ?>
                </button>
            </div>
        </form>
    </div>

    <script>
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                const forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                const validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

<?php
$gen->chiudi_pagina();
