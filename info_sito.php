<?php
require_once('php/generatore.php');

$gen = new generatore("Informazioni sul sito");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">

        <h2>Per gli utenti</h2>
        <p>
            Per contattarci, se vuole segnalare errori, farci una critica o darci un suggerimento, ci <a
                    href="mailto:riccardo@rpadovani.com">scriva.</a><br/>
            Se ha difficoltà a visualizzare il sito, le consigliamo di aggiornare il suo browser all'ultima versione, o
            di considerare l'utilizzo di <a href="http://www.mozilla.org/it/firefox/new/">Firefox.</a><br/>
            Se vuole sapere le ultime novità, segua la nostra pagina su <a
                    href="http://www.facebook.com/pages/Ferrara-Italy/Mcrferrara/158830014158119" target="_blank">Facebook</a>.

        <h2>Per i webmaster e gli sviluppatori</h2>
        <p>
            I contenuti del sito sono di proprietà di Mcr Ferrara.
            Il codice sorgente del sito è disponibile sotto licenza MIT su <a
                    href="https://gitlab.com/rpadovani/mcrferrara" target="_blank">Gitlab</a>
        </p>

        <h2>Copyright</h2>
        <p>
            I contenuti pubblicati su mcrferrara.org, dove non diversamente ed esplicitamente indicato, sono protetti
            dalla normativa vigente in materia di tutela del diritto d'autore, legge n. 633/1941 e successive modifiche
            ed integrazioni.<br/>
            È vietata la riproduzione integrale su Internet e su qualsiasi altro supporto cartaceo e/o digitale senza la
            preventiva autorizzazione scritta di Mcr Ferrara, qualsiasi sia la finalità di utilizzo.<br/>
            È altresì permessa e incoraggiata la riproduzione parziale sotto forma di abstract, a condizione che:</p>
        <ul>
            <li>il contenuto riprodotto non superi il 15% dei caratteri del contenuto originale;</li>
            <li>sia presente un chiaro e ben visibile link verso la pagina Web originale del contenuto citato;</li>
            <li>sia chiaramente indicato il nome di Mcr Ferrara</li>
        </ul>
        <p>
            Gli elementi grafici e il codice HTML/XHTML sono da considerarsi ricondivisibili secondo licenza MIT
        </p>
    </div>
<?php
$gen->chiudi_pagina();
