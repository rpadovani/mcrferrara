<?php
require_once('php/generatore.php');

$gen = new generatore("La nostra storia");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <h2>La nostra storia
            <small class="text-muted">A vostra disposizione dal 2003</small>
        </h2>

        <p class="lead">MCR nasce dalla lunga esperienza in campo congressuale della sua titolare, Maria Chiara
            Rippa.</p>
        <p>Laureatasi in Lingue e Letterature Straniere Moderne presso la Facoltà di Lettere e Filosofie dell'Università
            di Bologna, giornalista pubblicista dal 1989 al 1995, ha iniziato a lavorare in campo congressuale fin dal
            1986,
            prima come collaboratrice esterna, poi come libera professionista. Responsabile dell'Ufficio Congressi della
            Società che ha gestito le manifestazioni celebrative in occasione del VI Centenario dell'Università di
            Ferrara
            dal 1989 al 1991, nel 1992 diventa titolare di Pangea Service, società di organizzazione congressuale ben
            nota
            in ambito ferrarese, sia per la qualità dei servizi forniti che per il numero di manifestazioni organizzate.
            Ora, a chiusura della suddetta società, ha deciso di continuare ad offrire l'esperienza acquisita con
            l'organizzazione di centinaia di eventi e di metterla a frutto in una nuova agenzia. MCR si avvale della
            collaborazione di vari professionisti dei diversi settori, secondo le esigenze specifiche di ogni Cliente,
            fornendo i servizi richiesti con la professionalità, la puntualità, la serenità e la cordialità che
            derivano dalla lunga esperienza della nostra titolare.
        </p>

        <!--Javascript SDK per Facebook-->
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <h2>Contatti</h2>

        <dl class="row">
            <dt class="col-sm-3">Sede</dt>
            <dd class="col-sm-9">Via Cittadella 31, 44121 Ferrara</dd>

            <dt class="col-sm-3">Indirizzo email</dt>
            <dd class="col-sm-9"><a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a></dd>
        </dl>

        <h2>Social network</h2>
        Mcr Ferrara è presente anche su <a href="http://www.facebook.com/mcrferrara">Facebook</a>. Seguici per essere
        sempre informato sulle ultime attività!<br/><br/>
        <div class="badge_fb">
            <div class="fb-like-box" data-href="https://www.facebook.com/pages/Mcrferrara/158830014158119"
                 data-width="400" data-show-faces="true" data-stream="false" data-header="false"></div>
        </div>
    </div>
<?php
$gen->chiudi_pagina();

