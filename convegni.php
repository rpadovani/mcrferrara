<?php
require_once('php/generatore.php');
require_once('data/events.php');

const ELEMENTS_PER_ROW = 3;

$title = 'Eventi';

if (isset($_GET['anno'])) {
    $selected_year = (int)$_GET['anno'];
    $title .= " " . $selected_year;
}

$gen = new generatore($title);
$gen->chiudi_head();
$gen->crea_body();

$events = new events();
try {
    $next_event = $events->next_events(1);
} catch (Exception $e) {
    echo $e;
}

setlocale(LC_TIME, 'it_IT.utf8');
$next_event_date = strftime('%d %B %Y', strtotime($next_event[0]['start_date']));
?>

    <div class="container">
        <div class="card">
            <div class="card-header text-white bg-mcr">
                Il prossimo evento sarà il <?= $next_event_date ?>
            </div>
            <div class="card-body">
                <h5 class="card-title"><?= $next_event[0]['titolo'] ?></h5>
                <p class="card-text"><?= $next_event[0]['luogo'] ?>.</p>
                <a href="dettagli_convegno.php?id=<?= $next_event[0]['id_convegno'] ?>"
                   class="btn text-white bg-mcr float-right">Ulteriori informazioni &rarr;</a>
            </div>
        </div>

        <?php
        if (!isset($_GET['anno'])) {
            echo "<h2>Archivio</h2>";

            try {
                $all_years = $events->counts_events_by_year();
            } catch (Exception $e) {
            }

            foreach ($all_years as $row => $year) {

                if ($row % ELEMENTS_PER_ROW == 0) {
                    echo "<div class=\"card-deck\">";
                }

                ?>
                <div class="card">
                    <div class="card-header bg-mcr text-white">
                        <?= $year['year'] ?>
                    </div>
                    <div class="card-body">

                        <p class="card-text">Sono presenti <b><?= $year['count'] ?></b> eventi per
                            l'anno <?= $year['year'] ?></p>
                        <p class="card-text"><a href="convegni.php?anno=<?= $year['year'] ?>"
                                                class="btn btn-light float-right">Vedi tutti &rarr;</a></p>
                    </div>
                </div>
                <?php

                if ($row % ELEMENTS_PER_ROW == ELEMENTS_PER_ROW - 1) {
                    echo "</div><br/>";
                }
            }

            if (count($all_years) % ELEMENTS_PER_ROW != ELEMENTS_PER_ROW - 1) {
                echo "</div>";
            }
        } else {
            try {
                $all_events = $events->events_by_year($selected_year);
            } catch (Exception $e) {
            }
            ?>
            <h2><?= $selected_year ?>
                <small class="text-muted"><?= count($all_events) ?> eventi</small>
            </h2>
            <?php

            foreach ($all_events as $row => $event) {

                $event_day = strftime('%d', strtotime($event['start_date']));

                $is_event_in_future = new DateTime() < new DateTime($event['start_date']);

                if ($event['end_date']) {
                    $event_day .= ' - ' . strftime('%d', strtotime($event['end_date']));
                }
                ?>
                <div class="card" style="margin-top: 5px">
                    <div class="card-body">
                        <div class="row">
                            <div class="col col-md-2 col-4 text-center">
                                <div class="event-day"><?= $event_day ?></div>
                                <div class="event-month"><?= strftime('%B', strtotime($event['start_date'])) ?></div>
                                <div class="event-year"><?= strftime('%Y', strtotime($event['start_date'])) ?></div>
                            </div>
                            <div class="col col-md-8 col-8">
                                <div class="event-title">
                                    <a href="dettagli_convegno.php?id=<?= $event['id_convegno'] ?>">
                                        <?= $event['titolo'] ?>
                                    </a>
                                </div>
                                <div class="event-location"><?= $event['luogo'] ?></div>
                            </div>
                            <div class="col col-md-2 event-icons">
                                <?php
                                if ($event['società']) {
                                    switch ($event['società']) {
                                        case 'smc':
                                            {
                                                ?>
                                                <div class="row">
                                                    <div class="col">
                                                        <a href='smc.php' title='Organizzato dalla Smc'
                                                           class="float-right">
                                                            <img src='img/segreterie/smc.jpg'
                                                                 alt='Convegno Smc'
                                                                 class='immagine_icona_singolo_convegno'/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php
                                                break;
                                            }
                                    }
                                }

                                if ($event['download']) { ?>
                                    <div class="row">
                                        <div class="col">
                                            <a href="<?= $event['download'] ?>"
                                               title='Scarica il programma del convegno'
                                               class="float-right">
                                                <img src='img/pdf.png' alt='Programma'/>
                                            </a>
                                        </div>
                                    </div>
                                <?php }

                                if ($event['relazioni']) { ?>
                                    <div class="row">
                                        <div class="col">
                                            <a href='dettagli_relazione.php?id=<?= $event['id_convegno'] ?>'
                                               title='Scarica le relazioni del convegno' class="float-right">
                                                <img src='img/relazioni.jpg' alt='Relazioni'/>
                                            </a>
                                        </div>
                                    </div>
                                <?php }

                                if ($event['iscrizioni'] && $is_event_in_future) { ?>
                                    <div class="row">
                                        <div class="col">
                                            <a href='iscrizioni.php?convegno=<?= $event['id_convegno'] ?>'
                                               title='Iscriviti online al convegno' class="float-right">
                                                <img src='img/iscrizioni.gif' alt='Iscrizioni online'/>
                                            </a>

                                        </div>
                                    </div>
                                <?php }

                                if ($event['scheda_iscrizione'] && $is_event_in_future) { ?>
                                    <div class="row">
                                        <div class="col">
                                            <a href='download/iscrizioni/<?= $event['scheda_iscrizione'] ?>'
                                               title='Scarica la scheda di iscrizione' class="float-right">
                                                <img src='img/download_iscrizioni.jpg' alt='Iscrizioni online'/>
                                            </a>

                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <ul class="nav nav-pills-mcr nav-fill">
                <li class="nav-item">
                    <a class="nav-link <?= echoDisabledIfNoEventForYear($selected_year - 1) ?>" href="convegni.php?anno=<?= $selected_year - 1 ?>">
                        &larr; <?= $selected_year - 1 ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#"><?= $selected_year ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= echoDisabledIfNoEventForYear($selected_year + 1) ?>" href="convegni.php?anno=<?= $selected_year + 1 ?>">
                        <?= $selected_year + 1 ?> &rarr;
                    </a>
                </li>
            </ul>

            <?php
        }
        ?>

    </div>

<?php

$gen->chiudi_pagina();

