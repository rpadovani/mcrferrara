<?php
require_once('php/generatore.php');

$gen = new generatore("Alleanza Flebolinfologica");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <h2>Alleanza Flebolinfologica
            <small class="text-muted">Segreteria scientifica gestita da Mcr Ferrara</small>
        </h2>
        <p>
            <img src="img/segreterie/alleanza_flebolinfologica.png" alt="Logo Alleanza Flebolinfologica" class="rounded mx-auto d-block"/><br/>
<!--            La Società Medico Chirurgica (S.M.C.) di Ferrara, è un organismo scientifico che, da più di 60 anni, ha come-->
<!--            scopo-->
<!--            principale l'aggiornamento scientifico dei medici della nostra provincia, con una stretta collaborazione tra-->
<!--            le-->
<!--            varie categorie mediche : universitaria, ospedaliera, medici di famiglia. Vengono organizzati circa 10-->
<!--            incontri-->
<!--            l'anno, con la collaborazione delle UU.OO. dell'Arcispedale S.Anna, degli Ospedali del territorio e con-->
<!--            l'apporto di-->
<!--            esperti nazionali ed internazionali.-->
        </p>

<!--        <h2>Contatti</h2>-->
<!---->
<!--        <dl class="row">-->
<!--            <dt class="col-sm-3">Sede</dt>-->
<!--            <dd class="col-sm-9">Via Cittadella 31, 44121 Ferrara</dd>-->
<!---->
<!--            <dt class="col-sm-3">Sito internet</dt>-->
<!--            <dd class="col-sm-9"><a href="http://www.ospfe.it" target="_blank">www.ospfe.it</a></dd>-->
<!---->
<!--            <dt class="col-sm-3">Indirizzo email</dt>-->
<!--            <dd class="col-sm-9"><a href="mailto:smc@mcrferrara.org">smc@mcrferrara.org</a></dd>-->
<!--        </dl>-->
<!---->
<!--        <h2>Organigramma</h2>-->
<!---->
<!--        <dl class="row">-->
<!--            <dt class="col-sm-3">Presidente</dt>-->
<!--            <dd class="col-sm-9">Prof. Roberto Manfredini</dd>-->
<!--        </dl>-->
    </div>

<?php
$gen->chiudi_pagina();
