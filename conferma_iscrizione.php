<?php
require_once('php/generatore.php');

$gen = new generatore(_("Successful online registration"));
$gen->chiudi_head();
$gen->crea_body();

?>

    <div class="container">
        <h2><?= _("Online registration to the event has been successful!") ?></h2>
        <p><?= _("Your data has been sent to MCR Ferrara") ?>. <br/>
            <?= _("For any question please dont't hesitate to contact us at") ?> <a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a><br/>
        </p>

        <!--Javascript SDK per Facebook-->
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <h2>Social network</h2>
        Mcr Ferrara è presente anche su <a href="http://www.facebook.com/mcrferrara">Facebook</a>. Seguici per essere
        sempre informato sulle ultime attività!<br/><br/>
        <div class="badge_fb">
            <div class="fb-like-box" data-href="https://www.facebook.com/pages/Mcrferrara/158830014158119"
                 data-width="400" data-show-faces="true" data-stream="false" data-header="false"></div>
        </div>
    </div>

<?php
$gen->chiudi_pagina();
