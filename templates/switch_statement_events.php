<?php
require_once('data/events.php');

const DEFAULT_ROLES_HTML = "
selectRuolo.options[selectRuolo.options.length] = new Option('Medico', 'Medico');
selectRuolo.options[selectRuolo.options.length] = new Option('Infermiere', 'Infermiere');
";

function print_roles_switch_statement($event_id) {
    $events = new events();

    $htmlSwitch = "case " . $event_id . ":";

    try {
        $roles = $events->get_roles_for_event($event_id);

        if (sizeof($roles) <= 0) {
            // Fallback to default roles if there are no roles defined
            $htmlSwitch .= DEFAULT_ROLES_HTML;
        } else {
            foreach ($roles as $role_row => $role) {
                $htmlSwitch .= "selectRuolo.options[selectRuolo.options.length] = new Option('" . $role['role']. "', '". $role['role'] . "');";
            }
        }
    } catch (Exception $e) {
        // Fallback to default roles when there is an exception
        $htmlSwitch .= DEFAULT_ROLES_HTML;
    }

    return $htmlSwitch . "break;";
}