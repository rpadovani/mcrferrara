<body class="bg-image">
<!--
<main>
    <div id="farewell"></div>
    <div id="farewell-contain">
        <p class="farewell-text">
            Buongiorno a tutti!<br/>
            Visto il periodo contingente MCR si prende una pausa<br/>
            Grazie a tutti quelli che abbiamo incontrato in questi quasi 25 anni!<br/>
            Grazie a chi ha condiviso i momenti di gioia…. e i momenti di ansia<br/>
            Grazie a chi ha camminato con noi<br/>
            Grazie a chi ci ha sorretto…e si è fatto sorreggere<br/>
            Grazie a tutti i clienti, spesso diventati amici, con cui abbiamo creato eventi unici<br/>
            Grazie a tutti i fornitori, che ci hanno permesso di creare eventi unici<br/>
            Grazie a tutti i collaboratori, siete ….unici<br/>
            Grazie a chi ci ha sorriso, a chi ci ha “sgridato”, a chi ci ha parlato, a chi ci ha scritto.<br/>
            Siete stati tanti, e siete tutti nel nostro cuore<br/>
            Grazie<br/>        </p>

        <p class="cachet"><img src="img/cheers.svg" alt="Un brindisi" /></p>
           <p class="farewell-text">Brindiamo a tempi migliori,</p>
        <div id="signature">Maria Chiara Rippa</div>
    </div>
</main>-->

<header class="navbar navbar-expand-lg navbar-dark" style="background-color: #014d03;">
    <a class="navbar-brand" href="#">
        <img src="img/logo_menu.jpg" height="33px" width="54px" alt="Logo Mcr Ferrara"
             class="d-inline-block align-top"/>

        Mcr Ferrara
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= echoActiveClassIfRequestMatches('index') ?>">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item <?= echoActiveClassIfRequestMatches('chi_siamo') ?>">
                <a class="nav-link" href="chi_siamo.php">Chi siamo</a>
            </li>
            <li class="nav-item <?= echoActiveClassIfRequestMatches('servizi') ?>">
                <a class="nav-link" href="servizi.php">Servizi</a>
            </li>
            <li class="nav-item dropdown <?= echoActiveClassIfRequestMatches(['convegni', 'relazioni', 'iscrizioni', 'dettagli_convegno']) ?>">
                <a class="nav-link dropdown-toggle" href="#" id="eventsDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Eventi
                </a>
                <div class="dropdown-menu" aria-labelledby="eventsDropdown">
                    <?php
                    if (isAnyEventForYear(date('Y') + 1)) {
                        ?>
                        <a class="dropdown-item" href="convegni.php?anno=<?= date('Y') + 1 ?>"><?= date('Y') + 1 ?></a>
                        <?php
                    }
                    ?>
                    <a class="dropdown-item" href="convegni.php?anno=<?= date('Y') ?>"><?= date('Y') ?></a>
                    <a class="dropdown-item <?= echoActiveClassIfRequestMatches('relazioni') ?>" href="relazioni.php">Relazioni</a>
                    <a class="dropdown-item <?= echoActiveClassIfRequestMatches('iscrizioni') ?>" href="iscrizioni.php">Iscrizioni
                        online</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="convegni.php">Anni passati</a>
                </div>
            </li>
            <li class="nav-item dropdown <?= echoActiveClassIfRequestMatches(['alleanza_flebolinfologica', 'sedi', 'smc', 'societa']) ?>">
                <a class="nav-link dropdown-toggle" href="#" id="segreterieDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Società scientifiche
                </a>
                <div class="dropdown-menu" aria-labelledby="segreterieDropdown">
                    <a class="dropdown-item <?= echoActiveClassIfRequestMatches('alleanza_flebolinfologica') ?>"
                       href="alleanza_flebolinfologica.php">Alleanza Flebolinfologica</a>
                    <a class="dropdown-item <?= echoActiveClassIfRequestMatches('sedi') ?>" href="sedi.php">SEDI</a>
                    <a class="dropdown-item <?= echoActiveClassIfRequestMatches('smc') ?>" href="smc.php">S.M.C.</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item <?= echoActiveClassIfRequestMatches('societa') ?>" href="societa.php">Storiche</a>
                </div>
            </li>
        </ul>
    </div>
</header>
