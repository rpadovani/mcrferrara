<?php
require_once('php/generatore.php');

$gen = new generatore("Privacy");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <h2>Informativa sulla Privacy</h2>
        <p>
            Informativa ai sensi dell'art.13 del D.Lgs. 196/03 (Codice in materia di protezione dei dati personali) e al
            Regolamento UE 2016/679 (Regolamento Generale di Protezione dei Dati)
            <br/>
            I Suoi dati personali saranno trattati nel rispetto della disciplina di legge per l'evasione degli ordini,
            l'erogazione dei servizi per i quali è prevista la registrazione e per gli altri scopi indicati nella
            presente informativa; il trattamento sarà improntato ai principi di correttezza, liceità e trasparenza e di
            tutela della Sua riservatezza. Le modalità del trattamento prevedono l'utilizzo di strumenti manuali,
            informatici e telematici (ivi compresi telefax, telefono, anche senza assistenza di operatore, posta
            elettronica, SMS ed altri sistemi informatici e/o automatizzati di comunicazione) e sono comunque tali da
            assicurare la sicurezza e riservatezza dei dati stessi. Il titolare del trattamento è: MCR Via Cittadella 31,
            Ferrara 44121. Il responsabile del trattamento ai sensi dell'art.29 D.Lgs.196/03 potrà essere contattato al
            seguente indirizzo: <a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a>
            <br/>
            Ai sensi dell'art.7 D.Lgs.196/03: <br/>
        </p>
        <ul>
            <li>L'interessato ha diritto di ottenere la conferma dell'esistenza o meno di dati personali che lo
                riguardano, anche se non ancora registrati, e la loro comunicazione in forma intelligibile.
            </li>
            <li>L'interessato ha diritto di ottenere l'indicazione:
                <ul>
                    <li>dell'origine dei dati personali;</li>
                    <li>delle finalità e modalità del trattamento;</li>
                    <li>della logica applicata in caso di trattamento effettuato con l'ausilio di strumenti elettronici;
                    </li>
                    <li>degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai
                        sensi
                        dell'articolo 5, comma 2;
                    </li>
                    <li>dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o
                        che
                        possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato,
                        di
                        responsabili o incaricati.
                    </li>
                </ul>
            </li>
            <li>L'interessato ha diritto di ottenere:
                <ul>
                    <li>l'aggiornamento, la rettificazione ovvero, quando vi ha interesse, l'integrazione dei dati;</li>
                    <li>la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione
                        di
                        legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i
                        quali
                        i dati sono stati raccolti o successivamente trattati;
                    </li>
                    <li>l'attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza,
                        anche
                        per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o
                        diffusi,
                        eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi
                        manifestamente sproporzionato rispetto al diritto tutelato.
                    </li>
                </ul>
            </li>
            <li>L'interessato ha diritto di opporsi, in tutto o in parte:
                <ul>
                    <li>per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti
                        allo
                        scopo della raccolta;
                    </li>
                    <li>al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o
                        di
                        vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.
                    </li>
                </ul>
            </li>
        </ul>
        <p>
            Lei potrà esercitare i Suoi diritti sopra indicati nonché chiedere qualsiasi informazione in merito alla
            nostra policy di tutela dei dati personali inviando una comunicazione all'indirizzo sopra indicato o
            inviando un messaggio a: <a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a>
            Il mancato conferimento dei dati personali segnalati come obbligatori renderà impossibile l'esecuzione
            dell'eventuale ordine o servizio mentre il mancato conferimento di quelli non contrassegnati come
            obbligatori non ha conseguenze ai fini dell'esecuzione dell'ordine o servizio. Previo Suo consenso, MCR
            Ferrara potrà trattare i Suoi dati personali per l'erogazione dei servizi per i quali è prevista la
            registrazione nonché per finalità commerciali quali la segnalazione di offerte, promozioni o iniziative
            commerciali di qualsiasi genere avviate da essa o da altri operatori.<br/>
        </p>
    </div>

<?php
$gen->chiudi_pagina();

