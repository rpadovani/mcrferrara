<?php
require_once('php/generatore.php');

$gen = new generatore("Servizi");
$gen->chiudi_head();
$gen->crea_body("servizi");
?>
<div class="container">
    <h2>I servizi offerti da Mcr Ferrara</h2>
    <p>
        Mcr Ferrara offre servizi sempre all'avanguardia per soddisfare i bisogni di ogni singolo cliente; <br/>
        si parte dalla <b>preparazione del congresso</b>, passando all'organizzazione per il miglior
        <b>svolgimento</b> dello stesso, concludendo con <b>servizi finali</b> per
        rendere indimenticabile l'evento. <br/>
        A seconda delle esigenze di ogni cliente offriamo <b>ulteriori servizi</b>, grazie all'aiuto di
        società terze di indubbia professionalità.<br/>
        Ci <a href="mailto:info@mcrferrara.org">contatti</a> per una panoramica completa e per avere un preventivo su misura.
    </p>

    <nav class="nav nav-pills nav-fill nav-pills-mcr">
        <a class="nav-item nav-link active" id="before-tab" data-toggle="tab" href="#before" role="tab" aria-controls="home" aria-selected="true">
            Preparazione
        </a>
        <a class="nav-item nav-link" id="current-tab" data-toggle="tab" href="#current" role="tab" aria-controls="home" aria-selected="true">
            Svolgimento
        </a>
        <a class="nav-item nav-link" id="after-tab" data-toggle="tab" href="#after" role="tab" aria-controls="home" aria-selected="true">
            Servizi finali
        </a>
        <a class="nav-item nav-link" id="other-tab" data-toggle="tab" href="#other" role="tab" aria-controls="home" aria-selected="true">
            Ulteriori servizi
        </a>
    </nav>

    <br/>

    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="before" role="tabpanel" aria-labelledby="before-tab">
            <ul class="list-unstyled">
                <li>&rArr; Contatti con i relatori da Voi indicati con conseguenti istruzioni delle priorità da seguire;</li>
                <li>&rArr; Coordinamento sponsor e raccolta documentazione per Ministero della Sanità;</li>
                <li>&rArr; Raccolta degli interventi dei relatori prima dell'inizio del congresso;</li>
                <li>&rArr; Gestione prenotazioni alberghiere;</li>
                <li>&rArr; Organizzazione programma sociale;</li>
                <li>&rArr; Preparazione materiale da inviare;</li>
                <li>&rArr; Preparazione materiale personalizzato per i congressisti e loro accompagnatori: kit congressuali, badges, omaggi, ecc.;</li>
                <li>&rArr; Assistenza stampa;</li>
                <li>&rArr; Proseguimento rapporti con sponsors intrapresi dal Comitato;</li>
                <li>&rArr; Preparazione contratto per sponsors;</li>
                <li>&rArr; Assistenza all'allestimento stands;</li>
            </ul>
        </div>
        <div class="tab-pane fade" id="current" role="tabpanel" aria-labelledby="current-tab">
            <ul class="list-unstyled">
                <li>&rArr; Allestimento sale congressuali presso la sede del convegno;</li>
                <li>&rArr; Coordinamento di tutte le fasi del convegno;</li>
                <li>&rArr; Servizio di segreteria generale presso la sede del convegno;</li>
                <li>&rArr; Distribuzione del materiale congressuale;</li>
                <li>&rArr; Distribuzione materiale illustrativo sulla città di Ferrara;</li>
            </ul>
        </div>
        <div class="tab-pane fade" id="after" role="tabpanel" aria-labelledby="after-tab">
            <ul class="list-unstyled">
                <li>&rArr; Chiusura del convegno;</li>
                <li>&rArr; Redazione bilancio consuntivo;</li>
            </ul>
        </div>
        <div class="tab-pane fade" id="other" role="tabpanel" aria-labelledby="other-tab">
            <ul class="list-unstyled">
                <li><b>Allestimenti:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Addobbi floreali</li>
                        <li>&rArr; Cartelloni</li>
                        <li>&rArr; Creazione centri slides</li>
                        <li>&rArr; Zone poster attrezzate</li>
                        <li>&rArr; Individuazione e affitto sedi congressuali</li>
                        <li>&rArr; Striscioni stradali</li>
                    </ul>
                </li>
                <li><b>Attrezzature:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Impianti di amplificazione, registrazione, proiezione audio e video</li>
                        <li>&rArr; Impianti di traduzione simultanea con ricevitori via radio o a raggi infrarossi</li>
                        <li>&rArr; Impianti di votazione elettronica</li>
                        <li>&rArr; Impianti di rilevamento elettronico delle presenze </li>
                    </ul>
                </li>
                <li><b>Stampati:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Locandine, manifesti, inviti</li>
                        <li>&rArr; Programmi preliminari e definitivi</li>
                        <li>&rArr; Schede di iscrizione e schede alberghiere</li>
                        <li>&rArr; Volumi di abstracts e di atti del congresso </li>
                        <li>&rArr; Attestati di frequenza</li>
                        <li>&rArr; Badges</li>
                    </ul>
                </li>
                <li><b>Personale:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Utilizzo di hostess qualificate multilingue</li>
                        <li>&rArr; Utilizzo di interpreti qualificati, iscritti all'<a href="http://www.assointerpreti.it">Assointerpreti</a></li>
                    </ul>
                </li>
                <li><b>Ospitalità:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Gestione delle prenotazioni alberghiere presso la città in cui si svolge il convegno</li>
                    </ul>
                </li>
                <li><b>Catering:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Cene di gala</li>
                        <li>&rArr; Cene di lavoro</li>
                        <li>&rArr; Cocktails inaugurali</li>
                        <li>&rArr; Coffe-breaks</li>
                        <li>&rArr; Colazioni di lavoro</li>
                    </ul>
                </li>
                <li><b>Trasporti:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Autovetture con autista, aria condizionata e telefono</li>
                        <li>&rArr; Minibus</li>
                        <li>&rArr; Pullman</li>
                    </ul>
                </li>
                <li><b>Attività sociali:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Visite guidate alla città e ai suoi musei</li>
                        <li>&rArr; Visite alle mostre</li>
                        <li>&rArr; Visite alle città limitrofe</li>
                        <li>&rArr; Organizzazione di serate musicali</li>
                    </ul>
                </li>
                <li><b>Cadeaux:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Omaggi per i Relatori</li>
                        <li>&rArr; Omaggi per le Signore</li>
                        <li>&rArr; Omaggi per i congressisti</li>
                    </ul>
                </li>
                <li><b>Servizio stampa:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Contatti con i giornalisti delle testate locali e nazionali</li>
                        <li>&rArr; Redazione e invio di comunicati stampa sul significato del congresso</li>
                        <li>&rArr; Rapporti con i mass-media</li>
                        <li>&rArr; Presenza sul nostro sito web</li>
                        <li>&rArr; Creazione di un sito web dedicato</li>
                    </ul>
                </li>
                <li><b>Sponsor:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Ricerca sponsor su indicazione del committente</li>
                    </ul>
                </li>
                <li><b>Servizio fotografico:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Fotoservizio del convegno</li>
                    </ul>
                </li>
                <li><b>Gestione servizi amministrativi e fiscali:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Assistenza societaria</li>
                        <li>&rArr; Assistenza fiscale</li>
                        <li>&rArr; Elaborazione contabile dati</li>
                    </ul>
                </li>
                <li><b>Gestione contabile del convegno:</b>
                    <ul class="list-unstyled">
                        <li>&rArr; Raccolta delle quote di iscrizione al convegno</li>
                        <li>&rArr; Ricevimento delle sponsorizzazioni</li>
                        <li>&rArr; Gestionde del budget congressuale</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php
$gen->chiudi_pagina();
?>
