<?php
require_once('php/generatore.php');

$gen = new generatore("Mcr Ferrara");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <?php
        require_once('data/events.php');

        $events = new events();

        try {
            $next_online_subscriptions = $events->get_next_online_subscriptions();
            setlocale(LC_TIME, 'it_IT.utf8');

            if (sizeof($next_online_subscriptions) > 0) {
                ?>

                <div class="card">
                    <div class="card-header text-white bg-mcr">
                        Iscrizioni online
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Sono presenti <b><?= sizeof($next_online_subscriptions) ?></b> eventi a
                            cui iscriversi online</h5>
                        <p class="card-text">
                        <ul>
                            <?php
                            foreach ($next_online_subscriptions as $row => $event) {
                                $print_date = strftime('%d %B %Y', strtotime($event['start_date']));
                                ?>

                                <li>
                                    <b><?= $print_date ?></b>:
                                    <a href="dettagli_convegno.php?id=<?= $event['id_convegno'] ?>">
                                        <?= $event['titolo'] ?>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>

                        </p>
                        <a href="iscrizioni.php" class="btn text-white bg-mcr float-right">Iscriviti ora &rarr;</a>
                    </div>
                </div>


                <?php
            }
        } catch (Exception $e) {
            // Do nothing
        }
        ?>

        <div class="row d-none d-lg-block" style="margin-top: 10px">
            <div class="col">
                <div id="homePageContainer" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="img/slider/1.jpg" alt=""/>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/slider/2.jpg" alt=""/>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/slider/3.jpg" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 10px">
            <div class="col">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-header text-white bg-mcr">
                            Contatti
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div>
                                    <a href="mailto:info@mcrferrara.org">info@mcrferrara.org</a>
                                </div>
                                <div class="home_page_detail">Scrivici!</div>
                            </li>
                            <li class="list-group-item">
                                <div>Via della Cittadella 31, 44121 Ferrara</div>
                                <div class="home_page_detail">Visitaci!</div>
                            </li>
                            <li class="list-group-item">
                                <a href="chi_siamo.php" class="btn btn-light float-right">Ulteriori informazioni
                                    &#8594;</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-header text-white bg-mcr">
                            Prossimi eventi
                        </div>
                        <ul class="list-group list-group-flush">
                            <?php
                            require_once('data/events.php');

                            $events = new events();

                            try {
                                $next_events = $events->next_events(4);
                                setlocale(LC_TIME, 'it_IT.utf8');

                                foreach ($next_events as $row => $event) {
                                    $event_day = strftime('%d', strtotime($event['start_date']));

                                    if ($event['end_date']) {
                                        $event_day .= ' - ' . strftime('%d', strtotime($event['end_date']));
                                    }

                                    $event_day .= ' ' . strftime('%B %Y', strtotime($event['start_date']));
                                    ?>
                                    <li class="list-group-item">
                                        <div class="index-card-title text-truncate">
                                            <a href="dettagli_convegno.php?id=<?= $event['id_convegno'] ?>">
                                                <?= $event['titolo'] ?>
                                            </a>
                                        </div>
                                        <div class="home_page_detail"><?= $event_day ?></div>
                                    </li>
                                    <?php
                                }
                            } catch (Exception $e) {
                                ?>

                                <div class="alert alert-warning" role="alert">
                                    Sfortunatamente non siamo riusciti a connetterci al database, per favore riprovi più
                                    tardi.
                                </div>

                                <?php
                            }
                            ?>

                            <li class="list-group-item">
                                <a href="convegni.php?anno=<?= date('Y') ?>" class="btn btn-light float-right">Vedi
                                    tutti
                                    gli eventi &#8594;</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-header text-white bg-mcr">
                            Ultime relazioni disponibili
                        </div>
                        <ul class="list-group list-group-flush">
                            <?php

                            try {
                                $last_relations = $events->last_relations(4);

                                foreach ($last_relations as $row => $event) {
                                    $print_date = strftime('%d %B %Y', strtotime($event['start_date']));
                                    ?>
                                    <li class="list-group-item">
                                        <div class="index-card-title text-truncate">
                                            <a href="dettagli_relazione.php?id=<?= $event['id_convegno'] ?>">
                                                <?= $event['titolo'] ?>
                                            </a>
                                        </div>
                                        <div class="home_page_detail"><?= $print_date ?></div>
                                    </li>
                                    <?php
                                }
                            } catch (Exception $e) {
                                ?>
                                <div class="alert alert-warning" role="alert">
                                    Sfortunatamente non siamo riusciti a connetterci al database, per favore riprovi più
                                    tardi.
                                </div>
                                <?php
                            }
                            ?>

                            <li class="list-group-item">
                                <a href="relazioni.php" class="btn btn-light float-right">Vedi tutte le relazioni
                                    &#8594;</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$gen->chiudi_pagina();

