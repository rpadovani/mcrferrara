<?php
require_once('php/generatore.php');

$gen = new generatore("Mappa del sito");
$gen->chiudi_head();
$gen->crea_body();
?>

    <div class="container">
        <h2>La società</h2>
        <ul class="mappa_sito">
            <li><a href="index.php">Home page</a></li>
            <li><a href="chi_siamo.php">Chi siamo</a></li>
            <li><a href="servizi.php">I servizi offerti</a></li>
        </ul>

        <h2>Gli eventi</h2>
        <ul class="mappa_sito">
            <li><a href="iscrizioni.php">Iscrizioni online ai convegni</a></li>
            <li><a href="relazioni.php">Elenco delle relazioni disponibili dei vari eventi</a></li>
            <?php
            $last_year = date("Y");
            $current_year = 2003;

            while ($current_year <= $last_year) {
                echo "<li><a href='convegni.php?anno=$current_year'>Elenco eventi dell'anno $current_year </a></li>";
                $current_year++;
            }
            ?>
        </ul>

        <h2>Le segreterie scientifiche</h2>
        <ul class="mappa_sito">
            <li><a href="alleanza_flebolinfologica.php">Alleanza Flebolinfologica</a></li>
            <li><a href="sedi.php">Sedi</a></li>
            <li><a href="smc.php">Smc</a></li>
        </ul>
    </div>

<?php
$gen->chiudi_pagina();

