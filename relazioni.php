<?php
require_once('php/generatore.php');
require_once('data/events.php');

$gen = new generatore("Relazioni dei convegni");
$gen->chiudi_head();
$gen->crea_body();

$events = new events();
$next_event = $events->next_events(1);
setlocale(LC_TIME, 'it_IT.utf8');
$next_event_date = $print_date = strftime('%d %B %Y', strtotime($next_event[0]['start_date']));
?>

    <div class="container">

        <div class="card">
            <div class="card-header text-white bg-mcr">
                Il prossimo evento sarà il <?= $next_event_date ?>
            </div>
            <div class="card-body">
                <h5 class="card-title"><?= $next_event[0]['titolo'] ?></h5>
                <p class="card-text"><?= $next_event[0]['luogo'] ?>.</p>
                <a href="dettagli_convegno.php?id=<?= $next_event[0]['id_convegno'] ?>"
                   class="btn text-white bg-mcr float-right">Ulteriori informazioni &rarr;</a>
            </div>
        </div>

        <?php

        $all_events = $events->get_all_relations();
        ?>
        <h2>
            <small class="text-muted"><?= count($all_events) ?> eventi hanno delle relazioni disponibili</small>
        </h2>
        <?php

        foreach ($all_events as $row => $event) {

            $event_day = strftime('%d', strtotime($event['start_date']));

            if ($event['end_date']) {
                $event_day .= ' - ' . strftime('%d', strtotime($event['end_date']));
            }
            ?>
            <div class="card" style="margin-top: 5px">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-md-2 col-4 text-center">
                            <div class="event-day"><?= $event_day ?></div>
                            <div class="event-month"><?= strftime('%B', strtotime($event['start_date'])) ?></div>
                            <div class="event-year"><?= strftime('%Y', strtotime($event['start_date'])) ?></div>
                        </div>
                        <div class="col col-md-8 col-8">
                            <div class="event-title">
                                <a href="dettagli_relazione.php?id=<?= $event['id_convegno'] ?>">
                                    <?= $event['titolo'] ?>
                                </a>
                            </div>
                            <div class="event-location"><?= $event['luogo'] ?></div>
                        </div>
                        <div class="col col-md-2 event-icons">
                            <?php
                            if ($event['società']) {
                                switch ($event['società']) {
                                    case 'smc':
                                        {
                                            ?>
                                            <div class="row">
                                                <div class="col">
                                                    <a href='smc.php' title='Organizzato dalla Smc' class="float-right">
                                                        <img src='img/segreterie/smc.jpg'
                                                             alt='Convegno Smc'
                                                             class='immagine_icona_singolo_convegno'/>
                                                    </a>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        }
                                }
                            }

                            if ($event['download']) { ?>
                                <div class="row">
                                    <div class="col">
                                        <a href="<?= $event['download'] ?>" title='Scarica il programma del convegno'
                                           class="float-right">
                                            <img src='img/pdf.png' alt='Programma'/>
                                        </a>
                                    </div>
                                </div>
                            <?php }

                            if ($event['relazioni']) { ?>
                                <div class="row">
                                    <div class="col">
                                        <a href='dettagli_relazione.php?id=<?= $event['id_convegno'] ?>'
                                           title='Scarica le relazioni del convegno' class="float-right">
                                            <img src='img/relazioni.jpg' alt='Relazioni'/>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

    </div>

<?php

$gen->chiudi_pagina();


